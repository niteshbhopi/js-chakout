<!DOCTYPE html>
<html>

<head>
    
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>JS Checkout - FAB Example</title>

    
    <!-- Bootstrap for UI design of SafexPay Checkout -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" /> -->

    <!-- JS Checkout Library -->
    <script src="./js-checkout.js"></script>
    <!-- <script src="https://www.safexpay.com/testfab/fab-js-checkout/js-checkout.js"></script> -->

    <script>
        /** Initialization of JS Checkout Library **/
        let jsCheckoutOptions = {
//             merchantId: "201710270001", /// For normal JS Checkout.
//             merchantKey: 'oqUl4D0LqA4plZw4reAX/K3UKJoQdet0k/N6X6K4Y5k=',

//             merchantId: "202010140001", // For Tabby and Credit Card payment
//             merchantKey: 'UzYf5qDVnTLYsf1/2/JTmjy2qFHW4GU4mcOQ684MaCM=',
            
// 			merchantId: "202010140001", // For Tabby and Credit Card payment
//             merchantKey: 'UzYf5qDVnTLYsf1/2/JTmjy2qFHW4GU4mcOQ684MaCM=',
            
//             merchantId: "202010120002",
//             merchantKey: 'LSSd+r8CE82t9DA21Br+i7euZKXKabHm1UKFzd6boTc=',

//             merchantId: "202010120004", // for EMI with ARE country code
//             merchantKey: 'mhH0AB1ObVXSUE11rGPsK711V34Mv6ltZ54Y3/DgJg0=',

//             merchantId: "202010120003",
//             merchantKey: 'M8oz8mBY2rOoEAsl3W7mcwOnMYRuDmH0HDC7o8UziVE=',


//local
            merchantId: "201905070001",
            merchantKey: 'yyNMMuA1fKraeVsYj/K+IaXSghXO1lYT+9ShHLqwsVs=',


            // responseUrl: "https://www.safexpay.com/testfab/Response.php",
           // responseUrl: "http://localhost/js/Response.php",
//             responseUrl: "http://localhost:8080/fab-jsp/Response.jsp",
            //responseUrl: "https://bankfab.safexpay.com/fab-jsp/Response.jsp",
            
            responseUrl: "https://localhost:8085/fab-jsp/Response.jsp",
           
            countryCode: 'UAE', // ARE for Credit Card EMI test. UAE for tabby and normal Credit card payemnt 
            mode: 'DEV' // Itigration Mode can be DEV or PROD. Default is DEV.

        }
        		 spObj = new JsCheckout(); // Crating object of JS Checkout Library.

        spObj.Init(jsCheckoutOptions); // Initializing the required options.

        function Buy() {
            /** Order Details for Payment Initialization **/
            let orderDetails = {
                amount: 100,
                currencyCode: 'AED',
                currencySymbol: 'AED',

                orderNo: Math.floor((Math.random() * 100000) + 1), // Applied Math.random to get unique order No. for development only. This should be provided by the developer who is integrating this library.

                userId: 'sani@antino.io', // Applied Math.random to get unique order No. for development only. This should be provided by the developer who is integrating this library.
                // userId: Math.floor((Math.random() * 100000) + 1), // Applied Math.random to get unique order No. for development only. This should be provided by the developer who is integrating this library.

                customerDetails: { // This object is optional/Mandatory for card/Emi payments
                    name: 'Sani Yadav',
                    email: 'successful.payment@tabby.ai',
                    phone: '500000001'
                },
                billingDetails: { // This object is optional/Mandatory for Emi payments
                    bill_address: "123",
                    bill_city: "Gurgaon",
                    bill_state: "Hariyana",
                    bill_country: "India",
                    bill_zip: "110038"
                }

            }

            spObj.Pay(orderDetails, CallbackForResponse); // Pay function will accept two arguments and both are mandatory, First will be order details and second will be callback function. Callback function will be used to handle the response on the merchant side.
        }


        function CallbackForResponse(response) {
            /***
             *  Response can be Object or null. For the success and handled error it will be an object other wise it will be null. 
             *  Object will look like: 
             *  {
             *      status: <Boolean>,
             *      message:<String>,
             *      data:<Pipe separated string>
             *  } 
             * 
             *  Success Response Example: 
             *  {
             *      status: true,
             *      message:"success",
             *      data:"paygate|201710270001|43131|50.00|IND|INR|2020-10-15|14:11:14.0|1016660390359760896|PG_1016660390359760896|Successful|0|Successful"
             *  } 
             * 
             * 
             *  Filed Response Example: 
             *  {
             *      status: true,
             *      message:"failed",
             *      data:"paygate|201710270001|30721|50.00|IND|INR|2020-10-15|14:17:39.0|1016662004965474304|0|Failed|00046|Received fail response from the bank."
             *  } 
             * 
             *  
             * ****/
            console.log("ResponseInCallback:::", response);
        }
    </script>

</head>

<body>
    <div class="p-5">
        <button onclick="Buy()">Buy</button>
    </div>

</body>

</html>