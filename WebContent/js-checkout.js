
var JsCheckout = function (spOptions) {

    let AgId = 'fab';
    let MeId = null;
    let SuccessUrl = null;
    let FailUrl = null;
    // let SuccessUrl = "https://www.safexpay.com/testfab/Response.php";
    // let FailUrl = "https://www.safexpay.com/testfab/Response.php";
    // let SuccessUrl = "http://localhost/js/Response.php";
    // let FailUrl = "http://localhost/js/Response.php";
    let CountryCode = null;
    let Key = null;
    let InternalKey = 'HiktfH0Mhdla4zDg0/4ASwFQh2OS+nf9MVL0ik3DsmE='; // Internal Key for encryption
    let Base64IV = '0123456789abcdef';
    let PaymentData = null;
    let MerchantSchemesData = null;
    let MerchantBrandData = null;
    let CustomerOrder = null;
    let CustomerDetails = null;
    let CustomerCardDetails = {};
    let CurrentMode = null; // current selected payment mode. It will be Card, Net Banking, UPI, Wallet etc.
    let AvailablePaymentOptions = []; /// All payment modes against Merchant ID.
    let SavedCards = []; // Available Saved cards on server.
    let SelectedPaymentMode = null; // Selected Payment Mode by User.
    let SelectedPaymentOption = null; // Selected Payment Option by User.
    let IntegrationMode = 'DEV'; // DEV or PROD
    let IntegrationType = 'Aggrigate'; // Aggrigate or Merchant
    let BaseUrl = 'http://localhost:8080/agcore'; // Only for development //currently using
    // let BaseUrl = 'https://safexpaysit.bankfab.com/agcore'; // Only for development 
    //let BaseUrl = 'https://bankfab.safexpay.com/agcore'; // Only for development // for AWS 
    let PaymentUrl = BaseUrl + '/payment'; // Only for development 
    let PaymentDevUrl = 'https://localhost:8080/agcore/payment';//local
   // let PaymentDevUrl = 'https://bankfab.safexpay.com/agcore/payment'; // Only for development 
    let PaymentProdUrl = 'https://test.avantgardepayments.com/agcore/payment'; // Only for Production
    let PaymentResponseType = null;
    let ResponseCallback = null;
    let ResponseValue = null;
    let IsEMIEnabled = false; /// Status to check emi option selected by the customer or not.
    let EmiPlans = null;
    let SelectedEmiPlan = null;
    let CardTypeDetails = null;
    let IsSavedCardPayment = false;
    // let QueryUrl = 'https://test.avantgardepayments.com/agcore/api/query'; // for merchant details. 
    let QueryUrl = BaseUrl + '/api/query'; // for merchant details. 
    let CustomerBillingAddress = null;
    // let assetsUrl = 'https://bankfab.safexpay.com/fab-jsp';
    // let assetsUrl = 'https://www.safexpay.com/testfab/fab-js-checkout';
    //let assetsUrl = 'http://localhost/js-checkout-fab';
    let assetsUrl = 'http://localhost:8085/fab-jsp';
    let TabbyAccessKey = 'pk_test_eb5ec24c-0497-4008-9254-420a1472ace5'; // Test key
    let TabbyAccessData = null;
    let RelaunchTabby = false;
    let TabbyAgRef = null;
    let SessionTimeout = null;


    let SvGIconSet = {
        plusIcon: "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ0OCA0NDgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPjxwYXRoIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZD0ibTQwOCAxODRoLTEzNmMtNC40MTc5NjkgMC04LTMuNTgyMDMxLTgtOHYtMTM2YzAtMjIuMDg5ODQ0LTE3LjkxMDE1Ni00MC00MC00MHMtNDAgMTcuOTEwMTU2LTQwIDQwdjEzNmMwIDQuNDE3OTY5LTMuNTgyMDMxIDgtOCA4aC0xMzZjLTIyLjA4OTg0NCAwLTQwIDE3LjkxMDE1Ni00MCA0MHMxNy45MTAxNTYgNDAgNDAgNDBoMTM2YzQuNDE3OTY5IDAgOCAzLjU4MjAzMSA4IDh2MTM2YzAgMjIuMDg5ODQ0IDE3LjkxMDE1NiA0MCA0MCA0MHM0MC0xNy45MTAxNTYgNDAtNDB2LTEzNmMwLTQuNDE3OTY5IDMuNTgyMDMxLTggOC04aDEzNmMyMi4wODk4NDQgMCA0MC0xNy45MTAxNTYgNDAtNDBzLTE3LjkxMDE1Ni00MC00MC00MHptMCAwIiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+PC9nPjwvc3ZnPg==",
        minusIcon: 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' xmlns:svgjs=\'http://svgjs.com/svgjs\' version=\'1.1\' width=\'512\' height=\'512\' x=\'0\' y=\'0\' viewBox=\'0 0 124 124\' style=\'enable-background:new 0 0 512 512\' xml:space=\'preserve\' class=\'\'%3E%3Cg%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3Cpath d=\'M112,50H12C5.4,50,0,55.4,0,62c0,6.6,5.4,12,12,12h100c6.6,0,12-5.4,12-12C124,55.4,118.6,50,112,50z\' fill=\'%23828282\' data-original=\'%23000000\' style=\'\' class=\'\'/%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3C/g%3E%3C/svg%3E',
        infoIcon: 'data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' xmlns:svgjs=\'http://svgjs.com/svgjs\' version=\'1.1\' width=\'512\' height=\'512\' x=\'0\' y=\'0\' viewBox=\'0 0 512 512\' style=\'enable-background:new 0 0 512 512\' xml:space=\'preserve\' class=\'\'%3E%3Cg%3E%3Cpath xmlns=\'http://www.w3.org/2000/svg\' d=\'M256,0C114.613,0,0,114.617,0,256c0,141.391,114.613,256,256,256s256-114.609,256-256C512,114.617,397.387,0,256,0z M256,128c17.674,0,32,14.328,32,32c0,17.68-14.326,32-32,32s-32-14.32-32-32C224,142.328,238.326,128,256,128z M304,384h-96 c-8.836,0-16-7.156-16-16c0-8.836,7.164-16,16-16h16v-96h-16c-8.836,0-16-7.156-16-16c0-8.836,7.164-16,16-16h64 c8.836,0,16,7.164,16,16v112h16c8.836,0,16,7.164,16,16C320,376.844,312.836,384,304,384z\' fill=\'%230058a3\' data-original=\'%23000000\' style=\'\' class=\'\'/%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3C/g%3E%3C/svg%3E',
    }



    /************************************ Private Functions **************************************************** */

    /**** Initialize the data according to Merchant ID */
    Init = function () {
        console.log("Init is called................................");
        //console.log("Inside Init Function::", MeId);
        // Load Crypto JS Third Party Library

        LoadCryptoScript("https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js", function () {
            //initialization code
            LoadTabbyIntegrationScript(); // Add Tabby integration JS.
            // GetPayModeAndScheme(); // Get Payment Modes and its enabled schemes
            // GetBrandingDetails(); // Get Merchat Branding Details
            GetMerchantKey(); // Get Merchant Key by Me ID
            // GetSavedCardDetails(); // Get Saved cards of the users by Merchant Id.
            // GetEMIDetailsByCard('5457 2100', '1000'); // Get Saved cards of the users by Merchant Id.
            // GetEMIDetailsByCard('4012 0010', '1000'); // Get Saved cards of the users by Merchant Id.
        });
        LoadJsCheckoutStyleFile(assetsUrl + '/assets/css/js-checkout.css'); // Load the js-checkout Style file from server.
    }

    /******** Get Merchant Key by Merchant ID and Card No ***** */
    GetMerchantKey = function () {

        let url = `${BaseUrl}/api/merchantSignUp/merchantKey`;
         console.log("MeKeyAPI::::::::::::::", url);
        let params = {
            ag_id: AgId,
            me_id: MeId
        };
        console.log("MeKeyAPI::::::::::::::", params);
        //    let response = CallAPI('POST', url, {}, function (response) {
        let response = CallAPI('POST', url, params, function (response) {
             console.log("MKey Details::", response);
            try {
                let meKeyData = JSON.parse(response);
                console.log("meKeyData..............",meKeyData);
                if (meKeyData.status == 'Success') {
                    Key = meKeyData.merchant_key;
                    GetPayModeAndScheme(); // Get Payment Modes and its enabled schemes
                    GetBrandingDetails(); // Get Merchat Branding Details
                }
                // HandleEMIBreakDowns(emiData)
            } catch (e) {
                console.log("Error:", e);
                // HandleEMIBreakDowns([])
            }
            //SavedCards = JSON.parse(cardData); // Decrypted scheme data 
        });

        // /************ Only for dev ************/
        //response = ["1000.0|3.0|335.0|5.0|1005.0|3.0|101|12.0|null", "1000.0|6.0|169.6|18.0|1018.0|6.0|101|0.0|null", "1000.0|9.0|115.32|38.0|1038.0|9.0|101|0.0|null"];

        // return response

    }

    /******** Load Crypto Js  ***** */
    LoadJsCheckoutStyleFile = function (url) {
        var style = document.createElement("link")
        style.rel = "stylesheet";

        // if (style.readyState) {  //IE
        //     style.onreadystatechange = function () {
        //         if (style.readyState == "loaded" ||
        //             style.readyState == "complete") {
        //             style.onreadystatechange = null;
        //             // callback();
        //         }
        //     };
        // } else {  //Others
        //     style.onload = function () {
        //         // callback();
        //     };
        // }

        style.href = url;
        document.getElementsByTagName("head")[0].appendChild(style);
    }

    /******** Load Crypto Js  ***** */
    LoadCryptoScript = function (url, callback) {
        var script = document.createElement("script")
        script.type = "text/javascript";

        if (script.readyState) {  //IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" ||
                    script.readyState == "complete") {
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {  //Others
            script.onload = function () {
                callback();
            };
        }

        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    }
    /******** Load Tabby Js  ***** */
    LoadTabbyIntegrationScript = function () {
        var script = document.createElement("script")
        script.type = "text/javascript";

        if (script.readyState) {  //IE
            script.onreadystatechange = function () {
                if (script.readyState == "loaded" ||
                    script.readyState == "complete") {
                    script.onreadystatechange = null;
                    // InitTabby();
                }
            };
        } else {  //Others
            script.onload = function () {
                // InitTabby();
            };
        }

        script.src = 'https://checkout.tabby.ai/integration.js';
        document.getElementsByTagName("head")[0].appendChild(script);
    }

    /******** Get Payment Modes and Schemes by Merchant ID ***** */
    GetPayModeAndScheme = function () {
        // Encrypt Merchant Id with internal Key and pass to API.
        let encryptedMID = EncrypttWithInternalKey(MeId);
        console.log("MerchantIdInSchemeFunction::", MeId, encryptedMID);
        let payload = {
            me_id: encryptedMID
        }
        // let payload = {
        //     me_id: "wtYe7WPbZqNbe+xJTSJerQ=="
        // }
        // let payload = {
        //     "me_id" : "jOzRL+1UuazUVUndq+1l/Q=="
        //   }
        let response = CallAPI('POST', QueryUrl + '/payModeAndSchemeAPI', payload, function (response) {
            //.log("Scheme Data::", response);
            // Decrypt the response and set the Scheme variable.
            // let schemeData = DecryptWithInternalKey(JSON.parse(response).schemes);
            try {

                console.log("API is called ...................................");
                let resp = JSON.parse(response);
                console.log("API is called response ---->",resp);
                if (resp.error_details.error_code == 1) {
                    if (resp.schemes && resp.schemes != '') {
                        let schemeData = Decrypt(resp.schemes);

                        console.log("schemeData is .......=",schemeData);
                        // console.log("DecryptedSchemeData", schemeData);
                        // MerchantSchemesData = JSON.parse(schemeData); // Decrypted scheme data 
                        // SetAvaileblePaymentModes(); // to change the format of scheme response.

                        AvailablePaymentOptions = JSON.parse(schemeData); // Decrypted scheme data
                    }
                    else {
                        console.log("No payment mode is enabled for this merchant.");
                    }
                }
                else {
                    console.log("Merchant Error:" + resp.error_details.error_message);
                }
            }
            catch (e) {
                console.log("Error:", e);
            }
        });
    }


    /******** Get Payment Modes and Schemes by Merchant ID ***** */
    GetBrandingDetails = function () {
        //we've to encrypt the merchant Id and then we can pass the data to call the Merchant Brand API.
        let encryptedMID = EncrypttWithInternalKey(MeId);
        // console.log("MerchantIdInSchemeFunction::", MeId, encryptedMID);
        let payload = {
            me_id: encryptedMID
        }
        // let payload = {
        //     me_id: "wtYe7WPbZqNbe+xJTSJerQ=="
        // }
        let response = CallAPI('POST', QueryUrl + '/merchantBrandingDetails', payload, function (response) {
            // console.log("Branding Data::", JSON.parse(response).merchantBrandingDetails);
            // Decrypt the response and set the Scheme variable.
            // let brandData = DecryptWithInternalKey(JSON.parse(response).merchantBrandingDetails);

            let brandData = Decrypt(JSON.parse(response).merchantBrandingDetails);
            // console.log("DecryptedBrandingData", brandData);

            // let brandData = {
            //     "logo": "dsada",
            //     "integration_type": "Aggregator Hosted",
            //     "merchantThemeDetails": {
            //         "heading_bgcolor": "#008000",
            //         "bgcolor": "#f6f6f6",
            //         "menu_color": "#d55413",
            //         "footer_color": "#000000"
            //     },
            //     "error_details": {
            //         "error_code": "1",
            //         "error_message": "Success"
            //     }
            // }
            MerchantBrandData = JSON.parse(brandData); // Decrypted scheme data 
            // if (MerchantBrandData.merchantThemeDetails !== undefined) {
            //     if (MerchantBrandData.merchantThemeDetails.heading_bgcolor) {
            //         document.documentElement.style.setProperty('--header-color', MerchantBrandData.merchantThemeDetails.heading_bgcolor); // Header Background Color 
            //     }
            //     if (MerchantBrandData.merchantThemeDetails.bgcolor) {
            //         document.documentElement.style.setProperty('--background-color', MerchantBrandData.merchantThemeDetails.bgcolor); // Header Background Color 
            //     }
            //     // if (MerchantBrandData.merchantThemeDetails.menu_color) {
            //     //     document.documentElement.style.setProperty('--menu-color', MerchantBrandData.merchantThemeDetails.menu_color); // Header Background Color 
            //     // }
            //     if (MerchantBrandData.merchantThemeDetails.footer_color) {
            //         document.documentElement.style.setProperty('--footer-color', MerchantBrandData.merchantThemeDetails.footer_color); // Header Background Color 
            //     }
            // }

        });
    }


    /******** Get Saved Card Details by Merchant ID ***** */
    GetSavedCardDetails = function () {
        //we've to encrypt the merchant Id and then we can pass the data to call the Merchant Brand API.
        let encryptedMID = EncrypttWithInternalKey(MeId);
        //console.log("MerchantIdInSchemeFunction::", MeId, encryptedMID);
        let payload = {
            me_id: encryptedMID
        }
        // let payload = {
        //     me_id: "wtYe7WPbZqNbe+xJTSJerQ=="
        // }


        let response = CallAPI('POST', QueryUrl + '/getCardsByMerchant', payload, function (response) {
            // let response = CallAPI('POST', 'https://test.avantgardepayments.com/agcore/api/query/getCardsByMerchant', payload, function (response) {
            //console.log("Card Data::", JSON.parse(response).cardsDetails);
            // Decrypt the response and set the Scheme variable.
            // let cardData = DecryptWithInternalKey(JSON.parse(response).cardsDetails);
            try {
                let resp = JSON.parse(response);
                if (resp.error_Details.error_code == 1) {

                    if (resp.cardsDetails && resp.cardsDetails != '') {

                        let cardData = Decrypt(resp.cardsDetails);
                        // console.log("DecryptedSavedCardData", cardData);

                        SavedCards = JSON.parse(cardData); // Decrypted scheme data 
                    }
                    else {
                        console.log("No merchant details available.");
                    }
                }
                else {
                    console.log("Merchant Error:" + resp.error_Details.error_message);
                }
            }
            catch (e) {
                console.log("Error:", e);
            }
        });

        // /************ Only for dev ************/
        // response = { "cardsDetails": "hlaDgdzscUq497Aq+VBLJm6Uq3qEwFFM1FUluaB7BBzu9uhSNdhlV6cmvwAOxx2oNPut4/qorEO+iQU3WklAMa3p+UVOWkNiLoEHmpB/UuAxABvadwTPwu4k4onjqqgXr6+ej6nTv0bbqTLzL9Xfx+n3lDfAuNqporsjK5Y5UFZRowlKK6O9nCsbIcFq8Mvk", "error_Details": { "error_code": "1", "error_message": "Success" } };
        // let cardData = Decrypt(response.cardsDetails);
        // console.log("DecryptedSavedCardData", cardData);

        // SavedCards = JSON.parse(cardData); // Decrypted scheme data 

    }

    /******** Get EMI Details by Merchant ID and Card No ***** */
    GetEMIDetailsByCard = function (cardNo) {

        // let url = 'https://bankfab.safexpay.com/agcore/checkEmiOnCard/54572100/201710270001/100';
        // let url = `https://bankfab.safexpay.com/agcore/checkEmiOnCard/${cardNo}/${MeId}/${CustomerOrder.amount}`;
        //    let url = `${BaseUrl}/checkEmiOnCard/${cardNo}/${MeId}/${CustomerOrder.amount}`; //working with plain-text
        // let url = `${BaseUrl}/checkEPPonCard?cardNumber=${encodeURIComponent(Encrypt(cardNo))}&amount=${CustomerOrder.amount}&merchantId=${MeId}`;
        let url = `${BaseUrl}/checkEPPonCard?cardNumber=${EncrypttWithInternalKey(cardNo)}&amount=${CustomerOrder.amount}&merchantId=${MeId}`;
        // console.log("EMI::::::::::::::", url);
        // let params = {
        //     cardNumber: Encrypt(cardNo),
        //     amount: CustomerOrder.amount,
        //     merchantId: MeId
        // };

        //    let response = CallAPI('POST', url, {}, function (response) {
        let response = CallAPI('POST', url, {}, function (response) {
            // console.log("EMI Details::", response);
            try {
                let emiData = JSON.parse(response);
                HandleEMIBreakDowns(emiData)
            } catch (e) {
                console.log("Error:", e);
                HandleEMIBreakDowns([])
            }
            //SavedCards = JSON.parse(cardData); // Decrypted scheme data 
        });

        // /************ Only for dev ************/
        //response = ["1000.0|3.0|335.0|5.0|1005.0|3.0|101|12.0|null", "1000.0|6.0|169.6|18.0|1018.0|6.0|101|0.0|null", "1000.0|9.0|115.32|38.0|1038.0|9.0|101|0.0|null"];

        // return response

    }


    /******** Get EMI Details by Merchant ID and Card No ***** */
    GetCardTypeByCardNo = function (cardNo, elementId) {

        // let withoutSpace = cardNo.replace(/\W/gi, '');
        // let url = 'https://bankfab.safexpay.com/agcore/checkEmiOnCard/54572100/201710270001/100';
        let url = `${QueryUrl}/checkCardTpe/${cardNo}`;
        // console.log("CardNO ::::::::::::::", url);


        let response = CallAPI('POST', url, {}, function (response) {
            // console.log("CardTypeDetails::", response);
            try {
                if (response != '') {
                    let cardTypeData = response.split('|');
                    if (cardTypeData.length > 0) {
                        let data = {
                            cardNo: cardTypeData[0],
                            cardType: cardTypeData[1],
                            cardSchemeName: cardTypeData[2],
                            cardSchemeLogo: cardTypeData[3],
                            cardIssuerBank: cardTypeData[4],
                            cardOperatingMode: cardTypeData[5],
                            cardProductType: cardTypeData[6],
                        }
                        CardTypeDetails = data;
                        if (document.getElementById(elementId)) {
                            let str = '<img src="' + data.cardSchemeLogo + '"/>';
                            document.getElementById(elementId).innerHTML = str;
                        }
                        for (let i = 0; i < AvailablePaymentOptions.length; i++) { // for setting the payment mode i.e CC or DC.

                            if (AvailablePaymentOptions[i].payModeId == CardTypeDetails.cardType) {
                                SelectedPaymentMode = AvailablePaymentOptions[i];
                                break;
                            }

                        }
                    }
                    else {
                        CardTypeDetails = null;
                    }
                    // HandleEMIBreakDowns(emiData)
                }
            } catch (e) {
                console.log("Error:", e);
                // HandleEMIBreakDowns([])
            }
            //SavedCards = JSON.parse(cardData); // Decrypted scheme data 
        });

        // /************ Only for dev ************/
        //response = ["1000.0|3.0|335.0|5.0|1005.0|3.0|101|12.0|null", "1000.0|6.0|169.6|18.0|1018.0|6.0|101|0.0|null", "1000.0|9.0|115.32|38.0|1038.0|9.0|101|0.0|null"];

        // return response

    }

    InitTabby = function (ag_ref) {
        document.getElementById('sp-footer-btn-tabby').innerHTML = '<span class="_sp-lds-dual-ring-sm"></span>';
        document.getElementById('sp-footer-btn-tabby').disabled = true;
        // return;
        console.log("Inside the tabby flow..................................");
        // BUILD A PAYMENT OBJECT
        var payment = {
            amount: CustomerOrder.amount,
            buyer: {
                // dob: '1987-10-20',
                // email: 'successful.payment@tabby.ai',
                // name: 'John Doe',
                // phone: '+971500000001',
                email: CustomerDetails.email,
                name: CustomerDetails.name,
                phone: CustomerDetails.phone,
            },
            currency: CustomerOrder.currencyCode,
            // description: 'Tabby Store Order #3',
            order: {
                reference_id: ag_ref,
                // shipping_amount: '50',
                // tax_amount: '500',
            }
        };

        // THIS IS USED TO CONTROL RECREATION OF TABBY POPUP
        RelaunchTabby = false;


        let selectedTabbyProduct = null;
        //let payOption = document.getElementById('tabbyTermRadios1').value;
        if (document.getElementById('tabbyTermRadios2').checked) {
            selectedTabbyProduct = Tabby.INSTALLMENTS;
        }
        else {
            selectedTabbyProduct = Tabby.PAYLATER;
        }
        //console.log("PayOptions:::", selectedTabbyProduct);


        // console.log("PayOptions:::", payOption)

        // IN THIS DEMO WE HAVE TWO BUTTONS, DEPENDS ON CHECKOUT IMPLEMENTATION
        // var payLaterButton = document.querySelector('#payLaterButton');
        // var installmentsButton = document.querySelector('#installmentsButton');

        // LETS SETUP A CONFIG FOR TABBY SDK
        var config = {
            nodeId: 'tabby-checkout',
            apiKey: TabbyAccessData.api_key,
            //   sessionId: 'EXISTING_SESSION_ID', // OPTIONAL
            lang: 'en', // ar, ar_AE, ar_BH, ar_DZ, ar_EG, ar_IN, ar_IQ, ar_JO, ar_KW, ar_LB, ar_LY, ar_MA, ar_OM, ar_QA, ar_SA, ar_SD, ar_SS, ar_SY, ar_TN, ar_YE are treated as Arabic language, the rest is mapped to English
            merchantCode: TabbyAccessData.merchant_code, // OPTIONAL, USED WHEN A MERCHANT HAS MORE THAN ONE STORE, PLEASE CONTACT YOUR INTEGRATION MANAGER
            payment,
            onChange: data => {
                // TABBY API RESPONSES ARE FULLY PROPAGATED THROUGH SDK
                //console.log("Inside Tabby Onchange::", data);
                if (data.status === 'created' || config.sessionId) {

                    // ACTIVATE OR RENDER CONTROLS SO USER CAN CLICK IT
                    // payLaterButton.classList.remove('button__disabled');
                    // payLaterButton.removeAttribute('disabled');
                    // installmentsButton.classList.remove('button__disabled');
                    // installmentsButton.removeAttribute('disabled');

                    // IMPORTANT PIECE, TELLS SDK IF POPUP NEEDS TO BE RENDERED AGAIN
                    // PLEASE KEEP IT THIS WAY
                    // if (RelaunchTabby) {
                    //     Tabby.launch({ product: selectedTabbyProduct });
                    // }
                    if (RelaunchTabby) {
                        // console.log("In RelaunchTabby");
                        Tabby.create();
                        document.getElementById('tabby-checkout').style.display = '';
                    } else {
                        // console.log("In LaunchTabby");
                        Tabby.launch({ product: selectedTabbyProduct });
                        // console.log("Tabby,", Tabby);
                        document.getElementById('tabby-checkout').style.display = '';
                    }
                    // document.getElementById('sp-payment-div').style.display = 'none';
                    // document.getElementById('sp-payment-porccessing').style.display = 'block'; 
                }

                if (data.payment?.status === 'authorized') {
                    setTimeout(() => {
                        // WE'VE AUTHORIZED GIVEN PAYMENT, IT'S OK TO PASS IT TO BACKEND NOW, PLEASE VERIFY IT WITH SERVER TO SERVER CALL AND SECRET KEY
                        // ...YOUR CODE (REDIRECT TO SUCCESSFUL ORDER PAGE)

                        // console.log("Handle server redirection here.");

                        /*document.querySelector('#tabby-checkout').style.display = 'none';
                        
                        let resObj = null;
                        let d = 'fab|' + MeId + '|' + CustomerOrder.orderNo + '|' + data.payment.amount + '|' + CountryCode + '|' + CustomerOrder.currencyCode + '|' + new Date(data.payment.created_at).toDateString() + '|' + new Date(data.payment.created_at).toTimeString() + '|' + ag_ref + '|PG_' + ag_ref + '|Successful|0|Successful';
                        let res = d.split('|');
                        // if (res[10] == 'Successful') {
                        // Show Success Screen
                        // console.log("Inside the Success Response");
                        RenderSuccessContent(res);

                        resObj = {
                            status: true,
                            data: d,
                            message: 'success'
                        }

                        // }
                        // if (res[10] == 'Failed') {
                        //     // Show Failed Screen
                        //     // console.log("Inside the Failed Response");
                        //document.getElementById('sp-txn-failed-text').innerText = 'Transaction failed.';
                        //     RenderFailedContent(res);
                        //     resObj = {
                        //         status: false,
                        //         data: d,
                        //         message: 'failed'
                        //     }

                        // }
                        ResponseCallback(resObj); // send success object in callback
                        */

                        SendTabbyResponse(ag_ref, data, 'success');
                    }, 3000);

                    // SendTabbyResponse(ag_ref, data, 'success');
                }
                if (data.payment?.status === 'error') {
                    /*let resObj = null;
                    let d = 'fab|' + MeId + '|' + CustomerOrder.orderNo + '|' + data.payment.amount + '|' + CountryCode + '|' + CustomerOrder.currencyCode + '|' + new Date().toDateString() + '|' + new Date().toTimeString() + '|' + ag_ref + '|PG_' + ag_ref + '|Failed|0|Failed from tabby';
                    let res = d.split('|');
                    // if (res[10] == 'Successful') {
                    // Show Success Screen
                    // console.log("Inside the Success Response");
                    RenderFailedContent(res);
                    resObj = {
                        status: false,
                        data: d,
                        message: 'failed'
                    }
                    ResponseCallback(resObj);*/
                    SendTabbyResponse(ag_ref, data, 'fail');
                }

                // console.log('SESSION:', data.status);
                // console.log('AVAILABLE PRODUCTS:', data);
            },
            onClose: () => {
                // IMPORTANT PIECE, IF USER CLOSES TABBY POPUP, WE NEED TO REDRAW IT, CHANCES ARE SHE CHOOSES ANOTHER PRODUCT
                RelaunchTabby = true;
                document.querySelector('#tabby-checkout').style.display = 'none';
                document.getElementById('sp-footer-btn-tabby').innerText = 'Pay now';
                document.getElementById('sp-footer-btn-tabby').disabled = false;
                // GoToPayLaterByTabby();
                if (SessionTimeout != null) {
                    clearTimeout(SessionTimeout);
                }
            },
        };

        // THIS SNIPPET DEMOSTRATES AN ONCLICK EVENT FOR 'PLACE ORDER' BUTTON
        // IN CASE OF A SINGLE BUTTON, YOU CAN SAVE selectedTabbyProduct UPFRONT WHEN USERS CHOOSES ONE OF THE METHODS
        // payLaterButton.onclick = () => {
        //     document.querySelector('#tabby-checkout').style.display = '';
        //     selectedTabbyProduct = Tabby.PAYLATER;

        // LAUNCH POPUP OR RECREATE AND LAUNCH
        // if (relaunchTabby) {
        //     Tabby.create();
        // } else {
        //     Tabby.launch({ product: selectedTabbyProduct });
        // }
        // };

        // installmentsButton.onclick = () => {
        //     document.querySelector('#tabby-checkout').style.display = '';
        //     selectedTabbyProduct = Tabby.INSTALLMENTS;

        //     if (relaunchTabby) {
        //         Tabby.create();
        //     } else {
        //         Tabby.launch({ product: selectedTabbyProduct });
        //     }
        // };

        // INITIALIZE SDK AND CREATE POPUP FOR QUICKER DISPLAY WHEN IT'S NEEDED
        Tabby.init(config);
        Tabby.create();
        // console.log("Tabby Initialized::", Tabby);

        SpHandleTimeout()// Handle session timeout.
    }

    /********** Send Data **************/
    SendTabbyResponse = function (ag_ref, tabbyRes, type) {
        let data = tabbyRes;
        // let params = `ag_ref=${encodeURIComponent(Encrypt(ag_ref))}&pg_ref=${encodeURIComponent(Encrypt(tabbyRes.payment.id))}&amount=${encodeURIComponent(Encrypt(tabbyRes.payment.amount))}&status=${encodeURIComponent(Encrypt(tabbyRes.payment?.status))}&me_id=${MeId}`;
        let params = {
            ag_ref: Encrypt(ag_ref),
            pg_ref: Encrypt(tabbyRes.payment.id),
            amount: Encrypt(tabbyRes.payment.amount),
            status: Encrypt(tabbyRes.payment?.status),
            me_id: MeId
        };
        // let url = `${BaseUrl}/tabbyJSRequest?${params}`;
        // let url = `${BaseUrl}/tabbyJSPostResponse`;
        let url = `${BaseUrl}/tabbyJSResponse`;
        // console.log("SendResponse:: ::::::::::::::", url, params);


        CallAPI('POST', url, params, function (response) {
            // console.log("CardTypeDetails::", response);
            try {
                if (response != '') {
                    let resp = JSON.parse(response);
                    document.querySelector('#tabby-checkout').style.display = 'none';
                    document.getElementById('sp-payment-div').style.display = 'none';
                    document.getElementById('sp-payment-porccessing').style.display = 'block';
                    if (resp.message == 'Success') {
                        // document.querySelector('#tabby-checkout').style.display = 'none';
                        // document.getElementById('sp-payment-div').style.display = 'none';
                        // document.getElementById('sp-payment-porccessing').style.display = 'block';
                        if (type == 'success') {
                            let resObj = null;
                            let d = AgId + '|' + MeId + '|' + CustomerOrder.orderNo + '|' + data.payment.amount + '|' + CountryCode + '|' + CustomerOrder.currencyCode + '|' + new Date(data.payment.created_at).toDateString() + '|' + new Date(data.payment.created_at).toTimeString() + '|' + ag_ref + '|PG_' + tabbyRes.payment?.id + '|Successful|0|Successful';
                            let res = d.split('|');
                            // if (res[10] == 'Successful') {
                            // Show Success Screen
                            // console.log("Inside the Success Response");
                            RenderSuccessContent(res);

                            resObj = {
                                status: true,
                                data: d,
                                message: 'success'
                            }
                            TabbyAgRef = null;
                            ResponseCallback(resObj); // send success object in callback
                        }
                        if (type == 'fail') {
                            let resObj = null;
                            let d = AgId + '|' + MeId + '|' + CustomerOrder.orderNo + '|' + data.payment.amount + '|' + CountryCode + '|' + CustomerOrder.currencyCode + '|' + new Date().toDateString() + '|' + new Date().toTimeString() + '|' + ag_ref + '|PG_' + tabbyRes.payment?.id + '|Failed|0|Failed from tabby';
                            let res = d.split('|');
                            // if (res[10] == 'Successful') {
                            // Show Success Screen
                            // console.log("Inside the Success Response");
                            RenderFailedContent(res);
                            resObj = {
                                status: false,
                                data: d,
                                message: 'failed'
                            }
                            ResponseCallback(resObj);
                        }

                    }
                    else {  // Handle error from Response API
                        // document.querySelector('#tabby-checkout').style.display = 'none';
                        // document.getElementById('sp-payment-div').style.display = 'none';
                        // document.getElementById('sp-payment-porccessing').style.display = 'block';

                        let resObj = null;
                        let d = AgId + '|' + MeId + '|' + CustomerOrder.orderNo + '|' + data.payment.amount + '|' + CountryCode + '|' + CustomerOrder.currencyCode + '|' + new Date().toDateString() + '|' + new Date().toTimeString() + '|' + ag_ref + '|PG_' + tabbyRes.payment?.id + '|Failed|0|Failed from gateway provider';
                        let res = d.split('|');
                        // if (res[10] == 'Successful') {
                        // Show Success Screen
                        // console.log("Inside the Success Response");
                        RenderFailedContent(res);
                        resObj = {
                            status: false,
                            data: d,
                            message: 'failed'
                        }
                        ResponseCallback(resObj);

                    }
                }
            } catch (e) {
                console.log("Error:", e);
                // HandleEMIBreakDowns([])
            }
            //SavedCards = JSON.parse(cardData); // Decrypted scheme data 
        });

    }

    /****Send Data to JS Checkout server via API */
    CallAPI = function (method, url, postData, callback, normalHeader = '') {
        // console.log("URL:", url)
        const xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
                //   document.getElementById("demo").innerHTML = this.responseText;
                // console.log("CallAPI", this.responseText);
                callback(this.responseText);
            }
            else {
                //Errors can be handled here.
                // console.log("OnChange Response::", this.responseText);

            }

        };
        // XHR - Make Request  
        // xhr.open('POST', 'https://test.avantgardepayments.com/agcore/payment?me_id=' + mId, true);
        xhr.open(method, url, true);
        if (normalHeader != '') {
            xhr.setRequestHeader("Content-type", "application/json");
            // xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
            xhr.send(JSON.stringify(postData));
        }
        else {
            // xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            // xhr.send(postData);
            xhr.setRequestHeader("Content-type", "application/json");
            xhr.send(JSON.stringify(postData));
        }
    }




    /**** Apply encryption alogrithm and return encrypted string. */
    Encrypt = function (str) {

        let parsedBase64Key = CryptoJS.enc.Base64.parse(Key); //Working 
        // let dataParseKey = 'ArM5NBD2d6sEBDXqmv9y6z7WjeQz3SjIJfURwK9jcNU=';
        // let parsedBase64Key = CryptoJS.enc.Base64.parse(dataParseKey); //Working 
        // let parsedBase64Key = CryptoJS.enc.Base64.parse(InternalKey); /// Working for internal key will be only used for the MeID encryption for Branding and Schemes API.
        let iv = CryptoJS.enc.Utf8.parse(Base64IV); //working
        // Encryption process
        var plaintText = str;
        if (typeof plaintText !== 'string') {
            plaintText = str.toString();
        }
        //console.log("plaintText = " + plaintText);
        // this is Base64-encoded encrypted data
        var encryptedData = CryptoJS.AES.encrypt(plaintText, parsedBase64Key, {
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
            iv: iv
        });
        //console.log( "encryptedData = " + encryptedData );
        return encryptedData.toString();
    }

    /**** Apply Decryption alogrithm and return plain string. */
    Decrypt = function (eData) {
        // Decryption process
        var encryptedCipherText = eData; // or encryptedData;
        let parsedBase64Key = CryptoJS.enc.Base64.parse(Key);
        let iv = CryptoJS.enc.Utf8.parse(Base64IV);
        var decryptedData = CryptoJS.AES.decrypt(encryptedCipherText, parsedBase64Key, {
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
            iv: iv
        });
        // console.log( "DecryptedData = " + decryptedData );
        // this is the decrypted data as a string
        var decryptedText = decryptedData.toString(CryptoJS.enc.Utf8);
        // console.log("DecryptedText = " + decryptedText);
        return decryptedText;
    }


    /**** Apply encryption alogrithm and return encrypted string. on Merchant Id only */
    EncrypttWithInternalKey = function (str) {
        // let parsedBase64Key = CryptoJS.enc.Base64.parse(InternalKey); /// Working for internal key will be only used for the MeID encryption for Branding and Schemes API.
        let parsedBase64Key = CryptoJS.enc.Base64.parse(InternalKey); /// Working for internal key will be only used for the MeID encryption for Branding and Schemes API.
        //console.log("parsedBase64Key::", InternalKey, parsedBase64Key.toString());
        // let iv = CryptoJS.enc.Utf8.parse(Base64IV); //working
        let iv = CryptoJS.enc.Utf8.parse(Base64IV); //working
        //console.log("utf8Iv::", iv.toString());
        // Encryption process
        var plaintText = str;
        if (typeof plaintText !== 'string') {
            plaintText = str.toString();
        }
        //console.log("plaintText = " + plaintText);
        // this is Base64-encoded encrypted data
        var encryptedData = CryptoJS.AES.encrypt(plaintText, parsedBase64Key, {
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
            iv: iv
        });
        //console.log( "encryptedData = " + encryptedData );
        return encryptedData.toString();
    }

    /**** Apply Decryption alogrithm and return plain string. */
    DecryptWithInternalKey = function (eData) {
        let dataParseKey = 'ArM5NBD2d6sEBDXqmv9y6z7WjeQz3SjIJfURwK9jcNU='; // this will work only for Scheme data with Merchant ID wtYe7WPbZqNbe+xJTSJerQ==
        // Decryption process
        var encryptedCipherText = eData; // or encryptedData; 
        let parsedBase64Key = CryptoJS.enc.Base64.parse(InternalKey);
        // let parsedBase64Key = CryptoJS.enc.Base64.parse(dataParseKey);
        let iv = CryptoJS.enc.Utf8.parse(Base64IV);
        var decryptedData = CryptoJS.AES.decrypt(encryptedCipherText, parsedBase64Key, {
            mode: CryptoJS.mode.CBC,
            padding: CryptoJS.pad.Pkcs7,
            iv: iv
        });
        // console.log( "DecryptedData = " + decryptedData );
        // this is the decrypted data as a string
        var decryptedText = decryptedData.toString(CryptoJS.enc.Utf8);
        //console.log("DecryptedText = " + decryptedText);
        return decryptedText;
    }

    RenderPaymentModal = function () {
        console.log("RenderPaymentModal is called .....................");
        let meLogoUrl = assetsUrl + '' + assetsUrl + '/assets/img/sm-logo.png';
        if (MerchantBrandData.logo !== undefined) {
            meLogoUrl = MerchantBrandData.logo; // Merchant Logo 
        }
        let str = '<div class="_sp-modal _sp-fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="display:block;"> <div class="_sp-modal-dialog sp-modal-dialog p-4"> <div class="_sp-modal-content sp-modal-main-content"> <div class="_sp-modal-header"> <div class="sp-header"> <button type="button" class="_sp-close" data-dismiss="modal" aria-label="Close" onclick="CloseSpModal()"> <span aria-hidden="true"><span class="sp-close-btn">&times;</span></span> </button> <div class="sp-small-logo _sp-py-2"> <img src="' + meLogoUrl + '" class="sp-logo-img" /> </div> </div> </div> <div class="_sp-modal-body sp-modal-body"> <div class="sp-modal-content"> <div class="_sp-row" id="sp-payment-div" style="display:flex"> <!-- Side Menu --> <div class="_sp-col-md-4 _sp-col-sm-12"> <div class=""> <p class="sp-order-div "> <span class="sp-order-id-text">Order No : </span> <span class="sp-order-id-value" id="sp-order-id-value">0</span> </p> </div> ';

        str += '<div class="sp-payment-methods-list"> ';
        /******* Looping for the Payment modes available ***** */
        let combineCardType = false;
        if (AvailablePaymentOptions.length > 0) {

            console.log("AvailablePaymentOptions are =====> ",AvailablePaymentOptions);// Atul 

            for (let i = 0; i < AvailablePaymentOptions.length; i++) {
                if (AvailablePaymentOptions[i].payModeId == 'DC' || AvailablePaymentOptions[i].payModeId == 'CC') {
                    if (!combineCardType) {
                        str += '<div class="sp-payment-methods-item" id="sp-payment-mode-card" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <!-- <span class="fa fa-credit-card"></span> --> <img src="' + assetsUrl + '/assets/img/icons/card.svg" class="sp-card-img" /> <img src="' + assetsUrl + '/assets/img/icons/card-active.svg" class="sp-card-img-active" /> </div> <div class="sp-payment-mode-name"> Pay by <span>card</span> </div> </div> </div>';
                        combineCardType = true; /// To Avoid the 
                    }

                }

                // else if (AvailablePaymentOptions[i].payModeId == 'CC') {
                //     str += '<div class="sp-payment-methods-item" id="sp-payment-mode-credit-card" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <!-- <span class="fa fa-credit-card"></span> --> <img src="./assets/img/icons/card.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> Pay by <span> instalments <span style="font-size: 0.6rem;">(' + AvailablePaymentOptions[i].paymentMode.toLowerCase() + ')</span></span> </div> </div> </div>';
                // } 

                // else if (AvailablePaymentOptions[i].payModeId == 'CC') {
                //     str += '<div class="sp-payment-methods-item" id="sp-payment-mode-credit-card" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <!-- <span class="fa fa-credit-card"></span> --> <img src="./assets/img/icons/card.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> Pay by <span>credit card</span> </div> </div> </div>';
                // }
                else if (AvailablePaymentOptions[i].payModeId == 'CE') {
                    console.log("payModeId is CE ...............................");
                    str += '<div class="sp-payment-methods-item" id="sp-payment-mode-epp" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/icons/emi.svg" class="sp-card-img" /> <img src="' + assetsUrl + '/assets/img/icons/emi-active.svg" class="sp-card-img-active" /></div> <div class="sp-payment-mode-name"> Pay by <span> instalments <span style="font-size: 0.6rem;">(Credit Card)</span></span> </div> </div> </div>';
                }
                else if (AvailablePaymentOptions[i].payModeId == 'NB') {
                    console.log("payModeId is NB ...............................");

                    str += '<div class="sp-payment-methods-item" id="sp-payment-mode-net-banking" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/icons/bank.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> Pay by <span style="text-transform:lower-case">' + AvailablePaymentOptions[i].paymentMode.toLowerCase() + '</span> </div> </div> </div>';
                }
                else if (AvailablePaymentOptions[i].payModeId == 'PL' || AvailablePaymentOptions[i].payModeId == 'TB') {
                    console.log("payModeId is TB ...............................");

                    str += '<div class="sp-payment-methods-item" id="sp-payment-mode-tabby" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/icons/emi.svg" class="sp-card-img" /> <img src="' + assetsUrl + '/assets/img/icons/emi-active.svg" class="sp-card-img-active" /></div> <div class="sp-payment-mode-name"> Buy now pay later<span></span> </div> </div> </div>';
                    str += '<div class="sp-payment-methods-item" id="sp-payment-mode-wallet" onclick="SelectPaymentMode(\'' + "PW" + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/icons/wallet.svg" class="sp-card-img" /> <img src="' + assetsUrl + '/assets/img/icons/emi-active.svg" class="sp-card-img-active" /></div> <div class="sp-payment-mode-name"> Pay with wallet<span></span> </div> </div> </div>';
                    str += '<div class="sp-payment-methods-item" id="sp-payment-mode-gift" onclick="SelectPaymentMode(\'' + "PG" + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/gift_card.svg" class="sp-card-img" /> <img src="' + assetsUrl + '/assets/img/icons/emi-active.svg" class="sp-card-img-active" /></div> <div class="sp-payment-mode-name"> Pay by gift card<span></span> </div> </div> </div>';
                }

                else if (AvailablePaymentOptions[i].payModeId == 'PW') {   // for pay with wallet //Atul
                    console.log("payModeId is PW ...............................");

                   // str += '<div class="sp-payment-methods-item" id="sp-payment-mode-net-banking" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/icons/bank.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> Pay by <span style="text-transform:lower-case">' + AvailablePaymentOptions[i].paymentMode.toLowerCase() + '</span> </div> </div> </div>';
                }

                else if (AvailablePaymentOptions[i].payModeId == 'PG') {   // for pay by gift card //Atul
                    console.log("payModeId is PG ...............................");

                   // str += '<div class="sp-payment-methods-item" id="sp-payment-mode-net-banking" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/icons/bank.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> Pay by <span style="text-transform:lower-case">' + AvailablePaymentOptions[i].paymentMode.toLowerCase() + '</span> </div> </div> </div>';
                }


                else {
                    let type = AvailablePaymentOptions[i].paymentMode.toLowerCase().replace(' ', '-');
                    str += '<div class="sp-payment-methods-item" id="sp-payment-mode-' + type + '" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].payModeId + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/icons/wallet.svg" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> Pay with <span>' + AvailablePaymentOptions[i].paymentMode.toLowerCase() + ' </span> </div> </div> </div>';
                }
            }
        }
        // str += '<div class="sp-payment-methods-item" id="sp-payment-mode-card" onclick="SelectPaymentMode(\'card\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <!-- <span class="fa fa-credit-card"></span> --> <img src="./assets/img/icons/card.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> <span>Pay by card</span> </div> </div> </div>';

        // str += ' <div class="sp-payment-methods-item" id="sp-payment-mode-epp" onclick="SelectPaymentMode(\'epp\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="./assets/img/icons/wallet.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> <span>Pay by instalments <span style="font-size: 0.6rem;">(Credit Card)</span></span> </div> </div> </div> <div class="sp-payment-methods-item" id="sp-payment-mode-tabby" onclick="SelectPaymentMode(\'tabby\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="./assets/img/icons/wallet.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> <span>Buy now pay later</span> </div> </div> </div> <div class="sp-payment-methods-item" id="sp-payment-mode-wallet" onclick="SelectPaymentMode(\'wallet\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <!-- <span class="fa fa-credit-card"></span> --> <img src="./assets/img/icons/wallet.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> <span>Pay with wallets</span> </div> </div> </div> <div class="sp-payment-methods-item" id="sp-payment-mode-netbanking" onclick="SelectPaymentMode(\'netbanking\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="./assets/img/icons/wallet.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> <span>Net Banking</span> </div> </div> </div> <!-- <div class="sp-payment-methods-item" id="sp-payment-mode-applepay" onclick="SelectPaymentMode(\'applepay\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="./assets/img/icons/wallet.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> <span>Apple Pay</span> </div> </div> </div> <div class="sp-payment-methods-item" id="sp-payment-mode-gpay" onclick="SelectPaymentMode(\'gpay\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="./assets/img/icons/wallet.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> <span>GPay</span> </div> </div> </div> -->';

        str += '</div> ';

        str += '</div><!-- Payment Option Details --> <div class="_sp-col-md-8 _sp-col-sm-12"> <fieldset class="_sp-scheduler-border"> <legend class="_sp-scheduler-border" id="sp-selected-mode-text">Pay by card</legend> <!-- Screen 1- Card Payment Details --> <div id="sp-card-payment" style="display: none;"> </div> <!-- Screen 2- Net Banking--> <div id="sp-net-banking" style="display: none;"> </div> <!-- Screen 3- Wallet Screen--> <div id="sp-wallets" style="display: none;"> </div> <!-- Screen 4- UPI Screen--> <div id="sp-upis" style="display: none;"> </div> <!-- Screen 5- EPP with Card Payment --> <div id="sp-epp" style="display: none;"> </div> <!-- Screen 6- Tabby Contact Details--> <div id="sp-tabby-details" style="display: block;"> </div> <!-- Screen 7- Tabby Payment Option selection --> <div id="sp-tabby-payment-option-selection" style="display: none;"> </div> <!-- Screen 8- Apple Payment Details--> <div id="sp-applepay" style="display: block;"> </div> <!-- Pay Button --> <div class="_sp-row _sp-pt-2" id="sp-pay-now-content"> <div class="_sp-col-6 _sp-col-sm-6" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <span class="sp-amount-heading" id="sp-amount-heading">Amount payable</span> <h1 class="_sp-mt-0 sp-amount-text" id="sp-amount-text">' + CustomerOrder.currencyCode + ' ' + CustomerOrder.amount + '</p> </div> </div> <div class="_sp-col-6 _sp-col-sm-6" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <button class="_sp-btn sp-btn" id="sp-footer-btn" onclick="SPPayNow()" disabled="true">Pay now</button> </div> </div> </div> </fieldset> </div> </div> <!-- Screen 9- Success Payment --> <div id="sp-success-payment" style="display: none;"> <div class="sp-contact-payment-modes"> <div class="sp-response-content"> <div class="sp-transaction-details"> <h6>Payment of Amount</h6> <p class="sp-total-amount-value" id="sp-success-payment-amount">AED 0</p> </div> <div class="sp-response-icon"> <!-- <span class="far fa-check-circle"></span> --> <img src="data:image/svg+xml,%3Csvg id=\'Layer_1\' data-name=\'Layer 1\' xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' viewBox=\'0 0 152 152\'%3E%3Cdefs%3E%3Cstyle%3E.cls-1,.cls-2%7Bfill-rule:evenodd;%7D.cls-1%7Bfill:url(%23linear-gradient);%7D.cls-2%7Bfill:url(%23linear-gradient-2);%7D%3C/style%3E%3ClinearGradient id=\'linear-gradient\' x1=\'-143.25\' y1=\'124.37\' x2=\'-142.25\' y2=\'124.37\' gradientTransform=\'matrix(150.08, 0, 0, -150.08, 21499.53, 18741.12)\' gradientUnits=\'userSpaceOnUse\'%3E%3Cstop offset=\'0\' stop-color=\'%23283c93\'/%3E%3Cstop offset=\'1\' stop-color=\'%23009cdc\'/%3E%3C/linearGradient%3E%3ClinearGradient id=\'linear-gradient-2\' x1=\'-142.17\' y1=\'121.84\' x2=\'-141.17\' y2=\'121.84\' gradientTransform=\'matrix(92.8, 0, 0, -68.73, 13222.74, 8450.98)\' xlink:href=\'%23linear-gradient\'/%3E%3C/defs%3E%3Cg id=\'Group_307\' data-name=\'Group 307\'%3E%3Cpath id=\'Path_10\' data-name=\'Path 10\' class=\'cls-1\' d=\'M76,151C34.69,151,.89,117.3,1,76A75,75,0,1,1,76,151Zm62.69-74.93C139,41.85,110.87,14.26,78,13.28,42,12.22,13.54,41.59,13.23,75.4c-.3,33.62,26.63,63.27,62.52,63.43a62.59,62.59,0,0,0,63-62.21c0-.17,0-.34,0-.51Z\'/%3E%3Cpath id=\'Path_12\' data-name=\'Path 12\' class=\'cls-2\' d=\'M36.35,75.07c2-.15,3.63.85,5.18,2q9.94,7.56,19.92,15.11c2.16,1.63,2.17,1.64,4-.3q21.41-22.75,42.8-45.52c.84-.88,1.66-1.79,2.53-2.63a6.54,6.54,0,0,1,9.25-.32c.13.12.26.25.38.38a7.26,7.26,0,0,1,0,10.06C116.61,58,112.77,62.06,109,66.12Q89.8,86.51,70.63,106.88c-4.13,4.38-6.9,4.64-11.69,1Q45.83,98.05,32.77,88.1a7.12,7.12,0,0,1-3.09-6.91A6.84,6.84,0,0,1,34,75.46,5,5,0,0,1,36.35,75.07Z\'/%3E%3C/g%3E%3C/svg%3E" /> </div> <div class="sp-response-message"> <h6>Transaction Successful</h6> <p>Order ID : <span id="sp-success-payment-transaction-id"></span></p> </div> </div> </div> </div> <!-- Screen 10- Failed Payment --> <div id="sp-failed-payment" style="display: none;"> <div class="sp-contact-payment-modes"> <div class="sp-response-content pt-0"> <div class="sp-transaction-details"> <h6 class="_sp-mt-0">Sorry transaction failed</h6> <p class="sp-total-amount-value" id="sp-failed-payment-amount">AED 0</p> </div> <div class="sp-response-icon _sp-failed"> <!-- <span class="far fa-times-circle"></span> --> <img src="data:image/svg+xml,%3Csvg id=\'Layer_1\' data-name=\'Layer 1\' xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' viewBox=\'0 0 152 152\'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bfill-rule:evenodd;fill:url(%23linear-gradient);%7D.cls-2%7Bfill:url(%23linear-gradient-2);%7D.cls-3%7Bfill:url(%23linear-gradient-3);%7D%3C/style%3E%3ClinearGradient id=\'linear-gradient\' x1=\'28.47\' y1=\'124.02\' x2=\'29.51\' y2=\'124.86\' gradientTransform=\'matrix(150.08, 0, 0, -150.08, -4281.16, 18756.2)\' gradientUnits=\'userSpaceOnUse\'%3E%3Cstop offset=\'0\' stop-color=\'%23ba000e\'/%3E%3Cstop offset=\'1\' stop-color=\'%23ff0014\'/%3E%3C/linearGradient%3E%3ClinearGradient id=\'linear-gradient-2\' x1=\'62.09\' y1=\'126.51\' x2=\'62.09\' y2=\'125.51\' gradientTransform=\'matrix(7.36, 8.17, 49.79, -44.83, -6654.74, 5217.8)\' xlink:href=\'%23linear-gradient\'/%3E%3ClinearGradient id=\'linear-gradient-3\' x1=\'17.96\' y1=\'120.11\' x2=\'22.78\' y2=\'120.87\' gradientTransform=\'matrix(7.78, -7.78, -47.38, -47.38, 5625.37, 5978.37)\' xlink:href=\'%23linear-gradient\'/%3E%3C/defs%3E%3Cg id=\'Group_306\' data-name=\'Group 306\'%3E%3Cg id=\'Group_27\' data-name=\'Group 27\'%3E%3Cpath id=\'Path_10-2\' data-name=\'Path 10-2\' class=\'cls-1\' d=\'M76,151C34.69,151,.89,117.3,1,76A75,75,0,1,1,76,151Zm62.68-74.93C139,41.85,110.87,14.26,78,13.28,42,12.22,13.54,41.59,13.23,75.4c-.3,33.62,26.63,63.27,62.52,63.43a62.59,62.59,0,0,0,63-62.21v-.51Z\'/%3E%3Cg id=\'Group_26\' data-name=\'Group 26\'%3E%3Cpath id=\'Rectangle_56\' data-name=\'Rectangle 56\' class=\'cls-2\' d=\'M101.05,53.89h0a5.49,5.49,0,0,1-.41,7.76L59,99.13a5.49,5.49,0,0,1-7.76-.41h0a5.5,5.5,0,0,1,.4-7.77L93.28,53.48A5.5,5.5,0,0,1,101.05,53.89Z\'/%3E%3Cpath id=\'Rectangle_58\' data-name=\'Rectangle 58\' class=\'cls-3\' d=\'M52.46,52.61h0a5.52,5.52,0,0,1,7.78,0l39.6,39.6a5.5,5.5,0,0,1,0,7.78h0a5.5,5.5,0,0,1-7.78,0l-39.6-39.6A5.5,5.5,0,0,1,52.46,52.61Z\'/%3E%3C/g%3E%3C/g%3E%3C/g%3E%3C/svg%3E" /> </div> <div class="sp-response-message failed">  <p>Order no : <span id="sp-failed-payment-transaction-id"></span></p><h6 id="sp-txn-failed-text">Transaction failed</h6> <p class="_sp-mt-4 sp-tip">*Tip : Try using a different payment option.</p></div> <div class="_sp-row _sp-pt-2"> <div class="_sp-col-12" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <button class="_sp-btn sp-btn" id="sp-footer-btn-retry" onclick="TryAgainPaynow()">Try again</button> </div> </div> </div> </div> </div> </div> <!-- Screen 11- Payment Processing --> <div id="sp-payment-porccessing" style="display: none;"> <div class="sp-contact-payment-modes"> <div class="sp-response-content"> <div class="sp-payment-proccess-container _sp-text-center"> <h3>Your payment is being processed.<br /> Please wait for a moment.</h3> <div class="_sp-lds-dual-ring"></div><div id="sp-form-submit"></div> </div> </div> </div> </div> <!-- Screen 12- Payment Response Option selection --> <div id="sp-payment-response-type" style="display: none;"> <div class="sp-contact-payment-modes"> <div class="sp-response-content"> <div class="sp-payment-proccess-container _sp-text-center"> <h3>Choose transaction response type</h3> <button class="_sp-btn _sp-btn-success" onclick="SetResponseType(\'success\')">Success</button> <button class="_sp-btn _sp-btn-danger" onclick="SetResponseType(\'fail\')">Fail</button> </div> </div> </div> </div> </div> </div> <div class="_sp-modal-footer"> <span class="sp-footer-poweredby-text">Powered by First Abu Dhabi Bank</span> <div class="sp-footer-logo-div"><img src="' + assetsUrl + '/assets/img/fab-logo.svg" class="sp-poweredby-img" /></div><div><img src="' + assetsUrl + '/assets/img/payserve.png" class="sp-poweredby-img-2" /></div> </div> </div> </div> </div><div class="_sp-modal-backdrop _sp-fade _sp-show"></div>';

        let node = document.createElement("DIV");
        node.innerHTML = str;
        document.getElementsByTagName("body")[0].appendChild(node);
        document.getElementsByTagName('body')[0].classList.add('_sp-modal-open');

        // document.getElementById('sp-amount').innerText = CustomerOrder.currencySymbol + CustomerOrder.amount;
        document.getElementById('sp-order-id-value').innerText = CustomerOrder.orderNo;
        if (AvailablePaymentOptions.length > 0) {
            SelectPaymentMode(AvailablePaymentOptions[0].payModeId);
            // SelectPaymentMode('TB');
            // SelectPaymentMode('card');
            setTimeout(function () {
                document.getElementById('paymentModal').classList.add('_sp-show');
            }, 500);

        }


    }

    /******* Customer Details Proceed button ******* */
    SpTabbyProceed = function () {
        let custDetails = {
            name: document.getElementById('sp-txt-name').value,
            phone: document.getElementById('sp-txt-phone').value,
            email: document.getElementById('sp-txt-email').value,
        }
        // ToDo: check validations and open available payment options.
        CustomerDetails = custDetails;
        // console.log("SpProceed-Cutomer Details::", CustomerDetails);
        const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

        document.getElementById('sp-txt-name-error-text').innerText = "";
        document.getElementById('sp-txt-name').parentNode.classList.remove("sp-has-error");

        document.getElementById('sp-txt-email-error-text').innerText = "";
        document.getElementById('sp-txt-email').parentNode.classList.remove("sp-has-error");

        document.getElementById('sp-txt-phone-error-text').innerText = "";
        document.getElementById('sp-txt-phone').parentNode.classList.remove("sp-has-error");

        if (custDetails.name.trim().length == 0) {
            document.getElementById('sp-txt-name-error-text').innerText = "* Enter name";
            document.getElementById('sp-txt-name').parentNode.classList.add("sp-has-error");
            return;
        }
        if (!custDetails.name.match(/^[a-zA-Z\s]*$/)) {
            document.getElementById('sp-txt-name-error-text').innerText = "* Name must be characters only";
            document.getElementById('sp-txt-name').parentNode.classList.add("sp-has-error");
            return;
        }
        else if (custDetails.name.length > 50) {
            document.getElementById('sp-txt-name-error-text').innerText = "* Name can be max 50 character only.";
            document.getElementById('sp-txt-name').parentNode.classList.add("sp-has-error");
            return;
        } else if (!validEmailRegex.test(custDetails.email)) {
            document.getElementById('sp-txt-email-error-text').innerText = "* Invalid email";
            document.getElementById('sp-txt-email').parentNode.classList.add("sp-has-error");
            return;
        }
        else if (!custDetails.phone.match(/[0-9]/)) {
            document.getElementById('sp-txt-phone-error-text').innerText = "* Invalid phone";
            document.getElementById('sp-txt-phone').parentNode.classList.add("sp-has-error");
            return;
        }
        else if (!custDetails.phone.match(/^[0-9]{9}$/)) {
            document.getElementById('sp-txt-phone-error-text').innerText = "* Accept min 9 digits";
            document.getElementById('sp-txt-phone').parentNode.classList.add("sp-has-error");
            return;
        }
        else {
            SelectedPaymentOption = SelectedPaymentMode.paymentModeDetailsList[0];
            /*
            SetPaymentData();
            let encryptedPostData = GetEncryptedPostData(PaymentData);
            encryptedPostData.me_id = MeId;
            console.log("EncryptedPostDataForTabby", JSON.stringify(encryptedPostData, null, 4));

            let url = `${BaseUrl}/tabbyJSRequest`;
            // console.log("CardNO ::::::::::::::", url);


            let response = CallAPI('POST', url, encryptedPostData, function (response) {
                // console.log("CardTypeDetails::", response);
                try {
                    if (response != '') {
                        let resp = JSON.parse(response);
                        if (resp.message == 'Success') {
                            let data = Decrypt(resp.ag_ref);
                            console.log("Ag_ref:::", data);
                            InitTabby(data);
                            // RenderTabbyPayementDetails()
                            GoToPayLaterByTabby();
                        }
                    }
                } catch (e) {
                    console.log("Error:", e);
                    // HandleEMIBreakDowns([])
                }
                //SavedCards = JSON.parse(cardData); // Decrypted scheme data 
            });
*/
            GoToPayLaterByTabby();

        }

    }

    /******* Render Available Payment modes depending upon Merchant Id ******* */
    /*** Not in Use */
    RenderPaymentModes = function () {
        console.log("RenderPaymentModes is called .............................");
        let payModeStr = '<div class="sp-contact-payment-modes">';
        if (SavedCards.length > 0) {
            payModeStr += '<div> <p class="sp-content-label">Saved Payment methods</p> <div id="sp-card-slider" class="_sp-carousel _sp-slide sp-card-slider"> <div class="_sp-carousel-inner">';
            for (let i = 0; i < SavedCards.length; i++) {
                payModeStr += '<div class="_sp-carousel-item ';
                if (i == 0) payModeStr += "active"; // for active card
                let cardItemStyle = '';
                payModeStr += '"> <div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div>';
                let cardImg = '';
                let cardType = GetCardType(SavedCards[i].first6Digits);

                payModeStr += '<img src="' + assetsUrl + '/assets/img/visa.gif" class="sp-card-img" />';

                payModeStr += '</div> <div class="sp-card-details"> <div> <div> <div><span class="sp-card-type-name">' + cardType + '</span></div> <div><span>**** **** **** ' + SavedCards[i].last4Digits + '</span></div> </div> <div> <div class="_sp-form-row _sp-align-items-center"> <div class="_sp-col-5"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">Exp.Date</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YY" value="' + SavedCards[i].expiryMonth + '/' + SavedCards[i].expiryYear + '" autocomplete="off" /> </div> </div> </div> <div class="_sp-col-5"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-3 sp-card-control-label">CVV</label> <div class="_sp-col-7 _sp-pl-1"> <input type="password" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder=""> </div> </div> </div> </div> </div> </div> </div> </div> <div class="sp-right-arrow"> </div> </div> </div>';
            }

            payModeStr += '</div>'; //Carousel inner.

            payModeStr += '<ol class="_sp-carousel-indicators">';
            for (let i = 0; i < SavedCards.length; i++) {
                payModeStr += '<li data-target="#sp-card-slider" data-slide-to="' + i + '" class="active"></li>';
            }

            payModeStr += '</ol> </div></div>';

        }


        payModeStr += '<div> <p class="sp-content-label">Payment methods</p><div> <div class="sp-payment-methods-list">'; // payment mode list start.

        /******* Looping for the Payment modes available ***** */
        if (AvailablePaymentOptions.length > 0) {
            for (let i = 0; i < AvailablePaymentOptions.length; i++) {
                payModeStr += '<div class="sp-payment-methods-item" onclick="SelectPaymentMode(\'' + AvailablePaymentOptions[i].paymentMode + '\')"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div">';

                let paymentIcon = '';
                if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'debit card' || AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'credit card') {
                    paymentIcon = '<img src="' + assetsUrl + '/assets/img/icons/card.svg" class="sp-card-img" />';
                }
                else if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'net banking') {
                    paymentIcon = '<img src="' + assetsUrl + '/assets/img/icons/bank.png" class="sp-card-img" />';
                }
                else if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'wallet') {
                    paymentIcon = '<img src="' + assetsUrl + '/assets/img/icons/wallet.png" class="sp-card-img" />';
                }
                else if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'upi') {
                    paymentIcon = '<img src="' + assetsUrl + '/assets/img/icons/upi.png" class="sp-card-img" />';
                }
                else if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'paylater' || AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'epaylater') {
                    paymentIcon = '<img src="' + assetsUrl + '/assets/img/icons/emi.svg" class="sp-card-img" />';
                }
                else if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'gpay' || AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'google pay') {
                    paymentIcon = '<img src="' + assetsUrl + '/assets/img/icons/gpay.png" class="sp-card-img" />';
                }
                else {
                    paymentIcon = '';
                }
                payModeStr += paymentIcon;
                payModeStr += '</div> <div class="sp-payment-mode-name"> <span>' + AvailablePaymentOptions[i].paymentMode + '</span> </div> </div> <div class="sp-right-arrow"> <span class="fa fa-chevron-right sp-vertical-center" style="top:40%;"></span> </div> </div>';
            }

        }
        else {
            payModeStr += '<div class="_sp-container _sp-text-center" style="padding-top:100px; padding-bottom:100px"> Currently, there is no enabled payment options.</div>';
        }
        payModeStr += '</div></div>'; // Payment Mode list End

        payModeStr += '</div> </div> </div>'; // Payment Mode cards 

        // ToDo: Bind dynamic options from scheme api.

        document.getElementById('sp-available-payment-modes').innerHTML = payModeStr;
        document.getElementById('sp-available-payment-modes').style.display = 'block';
        document.getElementById('sp-footer').style.display = 'block';
        document.getElementById('sp-contact-details').style.display = 'none';
    }

    /******* Render Tabby Payment Details ******* */
    RenderTabbyPayementDetails = function () {
        let payModeStr = '<div class="_sp-row"> <div class="_sp-col-10 _sp-offset-1"> <div class="sp-tabby-heading"> <p class="sp-tabby-heading-text">Please enter the below fields to pay through Tabby</p> </div> <form class="sp-form"> <div class="_sp-form-group"> <!-- <i class="fa fa-user form-control-icon" aria-hidden="true"></i> --> <i class="_sp-form-control-icon" aria-hidden="true"> <img src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'11\' height=\'11\' viewBox=\'0 0 11 11\'%3E%3Cdefs%3E%3Cstyle%3E.a%7Bfill:%239a9a9a;%7D%3C/style%3E%3C/defs%3E%3Cpath class=\'a\' d=\'M7.5,2A5.5,5.5,0,1,0,13,7.5,5.5,5.5,0,0,0,7.5,2Zm0,1.65A1.65,1.65,0,1,1,5.85,5.3,1.648,1.648,0,0,1,7.5,3.65Zm0,7.81A3.96,3.96,0,0,1,4.2,9.689C4.217,8.595,6.4,8,7.5,8s3.283.6,3.3,1.694A3.96,3.96,0,0,1,7.5,11.46Z\' transform=\'translate(-2 -2)\'/%3E%3C/svg%3E" style="margin-bottom: 5px;" /> </i> <input type="text" class="_sp-form-control _sp-pl-4" placeholder="Enter Name" name="sp-txt-name" id="sp-txt-name" autocomplete="off" /><p class="sp-error-text" id="sp-txt-name-error-text"></p> </div> <div class="_sp-form-group"> <!-- <i class="fa fa-envelope _sp-form-control-icon" aria-hidden="true"></i> --> <i class="_sp-form-control-icon" aria-hidden="true"> <img src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'10.845\' height=\'8.676\' viewBox=\'0 0 10.845 8.676\'%3E%3Cdefs%3E%3Cstyle%3E.a%7Bfill:%239a9a9a;%7D%3C/style%3E%3C/defs%3E%3Cpath class=\'a\' d=\'M11.76,4H3.084A1.083,1.083,0,0,0,2.005,5.084L2,11.591a1.088,1.088,0,0,0,1.084,1.084H11.76a1.088,1.088,0,0,0,1.084-1.084V5.084A1.088,1.088,0,0,0,11.76,4Zm0,7.591H3.084V6.169L7.422,8.88,11.76,6.169ZM7.422,7.8,3.084,5.084H11.76Z\' transform=\'translate(-2 -4)\'/%3E%3C/svg%3E" style="margin-bottom: 5px;" /> </i> <input type="email" class="_sp-form-control pl-4" placeholder="Enter Email ID" name="sp-txt-email" id="sp-txt-email" /><p class="sp-error-text" id="sp-txt-email-error-text"></p> </div> <div class="_sp-form-group"> <!-- <i class="fa fa-mobile-alt _sp-form-control-icon" aria-hidden="true"></i> --> <i class="_sp-form-control-icon" aria-hidden="true"> <img src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' width=\'11\' height=\'11\' viewBox=\'0 0 11 11\'%3E%3Cdefs%3E%3Cstyle%3E.a%7Bfill:%239a9a9a;%7D.b%7Bfill:%23fff;%7D%3C/style%3E%3C/defs%3E%3Cg transform=\'translate(-517 -436)\'%3E%3Ccircle class=\'a\' cx=\'5.5\' cy=\'5.5\' r=\'5.5\' transform=\'translate(517 436)\'/%3E%3Cpath class=\'b\' d=\'M4.209,5.6a5.059,5.059,0,0,0,2.2,2.2l.735-.735a.332.332,0,0,1,.341-.08,3.81,3.81,0,0,0,1.192.19.335.335,0,0,1,.334.334V8.678a.335.335,0,0,1-.334.334A5.677,5.677,0,0,1,3,3.334.335.335,0,0,1,3.334,3H4.5a.335.335,0,0,1,.334.334,3.794,3.794,0,0,0,.19,1.192.335.335,0,0,1-.083.341Z\' transform=\'translate(516 436)\'/%3E%3C/g%3E%3C/svg%3E" style="margin-bottom: 5px;" /> </i> <input type="text" class="_sp-form-control _sp-pl-4" placeholder="Enter Mobile Number" name="sp-txt-phone" id="sp-txt-phone" autocomplete="off" /><p class="sp-error-text" id="sp-txt-phone-error-text"></p> </div> </form> </div> </div> <!-- Tabby Continue Button --> <div class="_sp-row _sp-pt-2"> <div class="_sp-col-6 _sp-col-sm-6" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <span class="sp-amount-heading">Amount payable</span> <h1 class="_sp-mt-0 sp-amount-text" id="sp-tabby-amount-text">0 AED </p> </div> </div> <div class="_sp-col-6 _sp-col-sm-6" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <button class="_sp-btn sp-btn" id="sp-footer-btn-tabby-continue" onclick="SpTabbyProceed()">Pay now</button> </div> </div> </div>';

        document.getElementById('sp-tabby-details').innerHTML = payModeStr;

        document.getElementById('sp-pay-now-content').style.display = 'none';
        document.getElementById('sp-tabby-details').style.display = 'block';
        document.getElementById('sp-selected-mode-text').innerText = 'Buy now pay later';
        document.getElementById('sp-payment-mode-tabby').classList.add('active');
        document.getElementById('sp-tabby-amount-text').innerText = CustomerOrder.currencySymbol + ' ' + CustomerOrder.amount;
    }

    GoToPayLaterByTabby = function () {
        console.log("GoToPayLaterByTabby is called .......................");
        let str = '<div class="sp-contact-payment-modes _sp-pt-3"> <div class="_sp-row"> <div class="_sp-col-10 _sp-offset-1"> <div class="sp-tabby-heading"> <p class="sp-tabby-heading-text _sp-mb-0">Please select your desired term</p> </div> <hr class="sp-hr" /> <div class="sp-tabby-terms _sp-pb-2"> <div class="_sp-form-check _sp-mb-4"> <input class="_sp-form-check-input" type="radio" name="tabby-term" id="tabbyTermRadios1" value="term1" checked> <label class="_sp-form-check-label" for="tabbyTermRadios1"> Pay in 14 Days with <span><img src="' + assetsUrl + '/assets/img/tabby.png" /></span> </label> <div class="sp-tabby-pay-option-details"><p>- No additional fees.</p><p>- Get your order first and pay in 14 days.</p></div></div> <div class="_sp-form-check"> <input class="_sp-form-check-input" type="radio" name="tabby-term" id="tabbyTermRadios2" value="term2"> <label class="_sp-form-check-label" for="tabbyTermRadios2"> Pay in instalments with <span><img src="' + assetsUrl + '/assets/img/tabby.png" /></span> </label> <div class="sp-tabby-pay-option-details"><p>- No interest or additional fees.</p><p>- Any debit or credit card accepted.</p><p>- Pay 25% now and the rest over 3 months.</p></div></div> </div> </div> </div> <div class="_sp-row"> <div class="_sp-col-10 _sp-offset-1"> <hr class="sp-hr" /> </div> </div> <!-- Tabby Continue Button --> <div class="_sp-row _sp-pt-2"> <div class="_sp-col-6 _sp-col-sm-6" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <span class="sp-amount-heading">Amount payable</span> <h1 class="_sp-mt-0 sp-amount-text" id="sp-tabby-opt-amount-text">0 AED </p> </div> </div> <div class="_sp-col-6 _sp-col-sm-6" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <button class="_sp-btn sp-btn" id="sp-footer-btn-tabby" onclick="SPPayNow()">Pay now</button> </div> </div> </div> </div>';

        document.getElementById('sp-tabby-payment-option-selection').innerHTML = str;
        document.getElementById('sp-pay-now-content').style.display = 'none';
        // document.getElementById('sp-tabby-details').style.display = 'none'; //aleready commit
        document.getElementById('sp-tabby-payment-option-selection').style.display = 'block';
        document.getElementById('sp-selected-mode-text').innerText = 'Buy now pay later';
        document.getElementById('sp-payment-mode-tabby').classList.add('active');
        document.getElementById('sp-tabby-opt-amount-text').innerText = CustomerOrder.currencySymbol + ' ' + CustomerOrder.amount;
    }

    /******* Render wallet payment Option ******* */
    RenderPayWithWallet = function (){
        console.log("RenderPayWithWallet is called .......................");
        //let str = '<div class="sp-contact-payment-modes _sp-pt-3"> <div class="_sp-row"><div class="_sp-col-10 _sp-offset-1"><div class="sp-tabby-heading"></div></div></div></div>';
        let str = '<h4 class="mb-3 tab-text"></h4><form class="tab-form wallet-form" novalidate=""><div class="payWallet"><div class="row"><div class="col-12"><ul class="walletDiv mi-hi"><li><div class="wallet_bg"><div class="img-text-content"><div class="imgDiv"><img src="assets/img/blue_pay.png" alt="logo"></div><h6>Pay Blue</h6></div></div></li><li><div class="wallet_bg"><div class="img-text-content"><div class="imgDiv"><img src="assets/img/applepay.png" alt="logo" class="applepay-img"></div><h6>Apple Pay</h6></div></div></li><li><div class="wallet_bg"><div class="img-text-content"><div class="imgDiv"><img src="assets/img/gpay.png" alt="logo" class="gpay-img"></div><h6>G Pay</h6></div></div></li><li><div class="wallet_bg"><div class="img-text-content"><div class="imgDiv"><img src="assets/img/edw.png" alt="logo"></div><h6>EDW</h6></div></div></li><li><div class="wallet_bg"><div class="img-text-content"><div class="imgDiv"><img src="assets/img/kfh_wallet.png" alt="logo"></div><h6>KFH Wallets</h6></div></div></li><li><div class="wallet_bg"><div class="img-text-content"><div class="imgDiv"> <img src="assets/img/samsung.png" alt="logo"></div><h6>Samsung Pay</h6></div></div></li><li><div class="wallet_bg"><div class="img-text-content"><div class="imgDiv"><img src="assets/img/nbd_pay.png" alt="logo"></div><h6>NBD Pay</h6></div></div></li><li><div class="wallet_bg"><div class="img-text-content"><div class="imgDiv"><img src="assets/img/beam.png" alt="logo" class="beam-img"></div><h6>BEAM</h6></div></div></li></ul><div class="walletDetails hide mi-hi"><h6 class="para-text">Enter below details.</h6><div class="row form-group"><div class="col-12 col-sm-3"> <label class="d-block cd-txt">Email / Mobile <span class="red">*</span></label></div><div class="col-12 col-sm-8"> <input type="text" name="em-mb" id="em-mb" placeholder="Email ID or Mobile No." class="input-box em-mb" minlength="" maxlength=""></div></div><div class="row form-group"><div class="col-12 col-sm-3"> <label class="d-block cd-txt">Wallet PIN <span class="red">*</span></label></div><div class="col-3 wal-min-wid"> <input type="password" name="walletpin" placeholder="Wallet Pin" class="input-box wal-inp" minlength="" maxlength=""></div></div><div class="row form-group"><div class="col-12 col-sm-3"></div><div class="col-12 col-sm-8"><p class="sp-error-text" id="">Please enter valid credentials.</p></div></div><div class="row form-group"><div class="col-12 col-sm-3"></div><div class="col-3 wal-min-wid"> <button class="btn btn-primary btn-block no-martop conf-btn" type="button"><a href="#">Confirm</a></button></div></div></div><div class="wallet-card-Bal"><hr class="mb-4"><div class="row form-group"><div class="col-12 col-sm-3"> <label class="d-block cd-txt">Wallet balance </label></div><div class="col-4 wal-min-wid"> <input type="text" name="walletpin" placeholder="Wallet Pin" class="input-box" minlength="" maxlength="" value="AED 4000" disabled></div></div><div class="remain"><div class="row form-group"><div class="col-12 col-sm-3"> <label class="d-block cd-txt">Remaining amount </label></div><div class="col-4 wal-min-wid"> <input type="text" name="reamt" placeholder="" class="input-box" minlength="" maxlength="" value="AED 1000"></div></div><div class="row form-group"><div class="col-8"><p><span class="red">Low balance !</span> Choose additional payment option for the remaining amount.</p></div><div class="col-3"> <label class="switch"> <input type="checkbox" id="togBtn"><div class="slider round"> <span class="on">ON</span> <span class="off">OFF</span></div> </label></div></div></div></div></div></div><div class="row"><div class="col-6"><div class="amt-div"><p>Amount payable</p><h5>AED 5000</h5></div></div><div class="col-5 no-pd"> <button class="btn btn-primary btn-block py-n" type="button"><a href="#">Pay Now</a></button> <button class="btn btn-primary btn-block hide ct-bt" type="button" disabled><a href="pay_by_card.html">Continue</a></button></div></div></div></form>';


        document.getElementById('sp-tabby-payment-option-selection').innerHTML = str;
        document.getElementById('sp-pay-now-content').style.display = 'none';
        // document.getElementById('sp-tabby-details').style.display = 'none'; //aleready commit
        document.getElementById('sp-tabby-payment-option-selection').style.display = 'block';
        document.getElementById('sp-selected-mode-text').innerText = 'Pay using Wallet';
        document.getElementById('sp-payment-mode-wallet').classList.add('active');
    }


    /******* Render gift card Option ******* */
    RenderPayWithGiftCard = function (){
        console.log("RenderPayWithGiftCard is called .......................");
        //let str = '<div class="sp-contact-payment-modes _sp-pt-3"> <div class="_sp-row"> <div class="_sp-col-10 _sp-offset-1"> <div class="sp-tabby-heading"> <p class="sp-tabby-heading-text _sp-mb-0">Please select your desired term</p> </div> <hr class="sp-hr" /> <div class="sp-tabby-terms _sp-pb-2"> <div class="_sp-form-check _sp-mb-4"> <input class="_sp-form-check-input" type="radio" name="tabby-term" id="tabbyTermRadios1" value="term1" checked> <label class="_sp-form-check-label" for="tabbyTermRadios1"> Pay in 14 Days with <span><img src="' + assetsUrl + '/assets/img/tabby.png" /></span> </label> <div class="sp-tabby-pay-option-details"><p>- No additional fees.</p><p>- Get your order first and pay in 14 days.</p></div></div> <div class="_sp-form-check"> <input class="_sp-form-check-input" type="radio" name="tabby-term" id="tabbyTermRadios2" value="term2"> <label class="_sp-form-check-label" for="tabbyTermRadios2"> Pay in instalments with <span><img src="' + assetsUrl + '/assets/img/tabby.png" /></span> </label> <div class="sp-tabby-pay-option-details"><p>- No interest or additional fees.</p><p>- Any debit or credit card accepted.</p><p>- Pay 25% now and the rest over 3 months.</p></div></div> </div> </div> </div> <div class="_sp-row"> <div class="_sp-col-10 _sp-offset-1"> <hr class="sp-hr" /> </div> </div> <!-- Tabby Continue Button --> <div class="_sp-row _sp-pt-2"> <div class="_sp-col-6 _sp-col-sm-6" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <span class="sp-amount-heading">Amount payable</span> <h1 class="_sp-mt-0 sp-amount-text" id="sp-tabby-opt-amount-text">0 AED </p> </div> </div> <div class="_sp-col-6 _sp-col-sm-6" style="font-size: 0.6rem;"> <div class="_sp-px-1 _sp-pt-2"> <button class="_sp-btn sp-btn" id="sp-footer-btn-tabby" onclick="SPPayNow()">Pay now</button> </div> </div> </div> </div>';
        let str = '<h4 class="mb-3 tab-text"></h4><form class="tab-form wallet-form" novalidate=""><div class="payWallet"><div class="row"><div class="col-12"><div class="walletDetails mi-hi"><h6 class="para-text">Enter your card details</h6><div class="row form-group"><div class="col-12 col-sm-3"> <label class="d-block cd-txt">Card number <span class="red">*</span></label></div><div class="col-12 col-sm-8"> <input type="text" name="card-nb" placeholder="0000 0000 0000 0000" class="input-box card-nb" minlength="" maxlength=""></div></div><div class="row form-group"><div class="col-12 col-sm-3"> <label class="d-block cd-txt">Card PIN <span class="red">*</span></label></div><div class="col-12 col-sm-8 wal-min-wid"> <input type="password" name="cardpin" placeholder="Card Pin" class="input-box cd-pin" minlength="" maxlength=""></div></div><div class="row form-group"><div class="col-12 col-sm-3"></div><div class="col-12 col-sm-8"><p class="sp-error-text" id="">Please enter valid credentials.</p></div></div><div class="row form-group"><div class="col-12 col-sm-3"></div><div class="col-3 wal-min-wid"> <button class="btn btn-primary btn-block no-martop cont-btn1" type="button">Confirm</button></div></div></div><div class="wallet-card-Bal"><hr class="mb-4"><div class="row form-group"><div class="col-12 col-sm-3"> <label class="d-block cd-txt">Card balance </label></div><div class="col-4 wal-min-wid"> <input type="text" name="walletpin" placeholder="Wallet Pin" class="input-box" minlength="" maxlength="" value="AED 4000" disabled></div></div><div class="remain"><div class="row form-group"><div class="col-12 col-sm-3"> <label class="d-block cd-txt">Remaining amount </label></div><div class="col-4 wal-min-wid"> <input type="text" name="reamt" placeholder="" class="input-box" minlength="" maxlength="" value="AED 1000"></div></div><div class="row form-group"><div class="col-8"><p><span class="red">Low balance !</span> Choose additional payment option for the remaining amount.</p></div><div class="col-3"> <label class="switch"> <input type="checkbox" id="togBtn"><div class="slider round"> <span class="on">ON</span> <span class="off">OFF</span></div> </label></div></div></div></div></div></div><div class="row"><div class="col-6"><div class="amt-div"><p>Amount payable</p><h5>AED 5000</h5></div></div><div class="col-5 no-pd"> <button class="btn btn-primary btn-block py-n" type="button"><a href="#">Pay Now</a></button> <button class="btn btn-primary btn-block hide ct-bt" type="button" disabled><a href="pay_by_card.html">Continue</a></button></div></div></div></form>';

        document.getElementById('sp-tabby-payment-option-selection').innerHTML = str;
        document.getElementById('sp-pay-now-content').style.display = 'none';
        // document.getElementById('sp-tabby-details').style.display = 'none'; //aleready commit
        document.getElementById('sp-tabby-payment-option-selection').style.display = 'block';
        document.getElementById('sp-selected-mode-text').innerText = 'Pay using Gift card';
        document.getElementById('sp-payment-mode-gift').classList.add('active');

    }



    /******* Render Card Payment Option ******* */
    RenderCardPayement = function () {

        let savedCardList = [];
        if (SavedCards.length > 0) {
            for (let i = 0; i < SavedCards.length; i++) {
                if (SavedCards[i].payModeId == SelectedPaymentMode.payModeId) {
                    savedCardList.push(SavedCards[i]);
                }
            }
        }

        //console.log("Saved Cards", savedCardList);

        let payModeStr = '<div class="sp-payment-list">';
        // for (let i = 0; i < 2; i++) {
        //     // for (let i = 0; i < savedCardList.length; i++) {
        //     payModeStr += '<div class="_sp-carousel-item ';
        //     if (i == 0) payModeStr += "active"; // for active card
        //     let cardItemStyle = '';
        //     payModeStr += '"> <div class="sp-payment-list-item"> <div class="sp-payment-list-item-card">';
        //     let cardImg = '';
        //     let cardType = GetCardType(savedCardList[0].first6Digits);

        //     payModeStr += '<div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div> <img src="./assets/img/visa.gif" class="sp-card-img" /> </div> <div class="sp-card-details"> <div class="sp-card-details"> <div> <!-- <div><span>VISA</span></div> --> <div class="sp-card-number"><span>**** **** **** ' + savedCardList[0].last4Digits + '</span></div> </div> <div> <div class="_sp-form-row _sp-align-items-center"> <div class="_sp-col-12"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">Exp.Date</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YYYY" value="' + savedCardList[0].expiryMonth + '/' + savedCardList[0].expiryYear + '" /> </div> </div> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-3 sp-card-control-label">CVV</label> <div class="_sp-col-7 _sp-pl-1"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder=""/> </div> </div> </div> </div> </div> </div> </div> </div></div> </div> </div>';

        // }
        // payModeStr += '</div> </div></div>';
        for (let i = 0; i < savedCardList.length; i++) {

            payModeStr += '<div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div id="sp-saved-card-icon-' + i + '"> <!--<img src="' + assetsUrl + '/assets/img/visa.gif" class="sp-card-img" />--> </div> <div class="sp-card-details"> <div class="sp-card-details"> <div> <!-- <div><span>VISA</span></div> --> <div class="sp-card-number"><span>**** **** **** ' + savedCardList[i].last4Digits + '</span></div> </div> <div> <div class="form-row align-items-center"> <div class="_sp-col-12"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">Exp.Date</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YY" value="' + savedCardList[i].expiryMonth + '/' + savedCardList[i].expiryYear + '" autocomplete="off" /> </div> </div> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">CVV</label> <div class="_sp-col-8"> <input type="password" class="_sp-form-control _sp-form-control-sm sp-card-control" id="sp-saved-card-cvv-' + SelectedPaymentMode.payModeId + '-' + i + '" placeholder="" onkeyup="HandleCvvForSavedCard(\'sp-saved-card-cvv-' + SelectedPaymentMode.payModeId + '-' + i + '\', ' + i + ')"> </div> </div> </div> </div> </div> </div> </div> </div> <div class="sp-right-arrow"> </div> </div>';
            let maskedCardNo = savedCardList[i].first6Digits.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            GetCardTypeByCardNo(maskedCardNo, 'sp-saved-card-icon-' + i);
        }
        // payModeStr += '<div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div> <img src="./assets/img/visa.gif" class="sp-card-img" /> </div> <div class="sp-card-details"> <div class="sp-card-details"> <div> <!-- <div><span>VISA</span></div> --> <div class="sp-card-number"><span>**** **** **** 1234</span></div> </div> <div> <div class="form-row align-items-center"> <div class="col-12"> <div class="form-group row mb-0"> <label for="inlineFormInput" class="col-4 sp-card-control-label">Exp.Date</label> <div class="col-8"> <input type="text" class="form-control form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YY"> </div> </div> <div class="form-group row mb-0"> <label for="inlineFormInput" class="col-3 sp-card-control-label">CVV</label> <div class="col-7 pl-1"> <input type="text" class="form-control form-control-sm sp-card-control" id="inlineFormInput" placeholder=""> </div> </div> </div> </div> </div> </div> </div> </div> <div class="sp-right-arrow"> <!-- <span class="fa fa-chevron-right sp-vertical-center" style="top:40%;"></span> --> </div> </div>';

        payModeStr += '</div>';
        payModeStr += '<div class="_sp-acc-kontainer">'; //Accordian Start
        // payModeStr += '<div> <p class="sp-content-label mb-3 mt-0">Enter card details</p> '; 
        payModeStr += '<div> <div class="_sp-row"><div class="_sp-col-md-12"><input type="radio" name="acc" id="acc1" class="_sp-acc-input" checked><label for="acc1" class="_sp-acc-label"><p class="sp-content-label sp-acc-label _sp-mb-3 _sp-mt-0"><span>Enter card details</span><span class="sp-acc-icon" id="sp-accordian-card-right-icon" style="display:none;"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ1MS44NDcgNDUxLjg0NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBkPSJNMjI1LjkyMywzNTQuNzA2Yy04LjA5OCwwLTE2LjE5NS0zLjA5Mi0yMi4zNjktOS4yNjNMOS4yNywxNTEuMTU3Yy0xMi4zNTktMTIuMzU5LTEyLjM1OS0zMi4zOTcsMC00NC43NTEgICBjMTIuMzU0LTEyLjM1NCwzMi4zODgtMTIuMzU0LDQ0Ljc0OCwwbDE3MS45MDUsMTcxLjkxNWwxNzEuOTA2LTE3MS45MDljMTIuMzU5LTEyLjM1NCwzMi4zOTEtMTIuMzU0LDQ0Ljc0NCwwICAgYzEyLjM2NSwxMi4zNTQsMTIuMzY1LDMyLjM5MiwwLDQ0Ljc1MUwyNDguMjkyLDM0NS40NDlDMjQyLjExNSwzNTEuNjIxLDIzNC4wMTgsMzU0LjcwNiwyMjUuOTIzLDM1NC43MDZ6IiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /></span></p></label> ';
        payModeStr += '<div class="_sp-acc-body">'; // acordian boddy start for card details

        payModeStr += '<form class="sp-form">';

        payModeStr += '<div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-md-4 _sp-col-sm-3 _sp-col-form-label sp-form-label">Card Number<span class="sp-mandatory">*</span></label> <div class="_sp-col-md-8 _sp-col-sm-9"><input type="text" class="_sp-form-control" placeholder="Enter card number" name="sp-txt-card-number" id="sp-txt-card-number" onkeyup="HandleCardInputChange(\'sp-txt-card-number\', \'' + SelectedPaymentMode.payModeId + '\')" onblur="GetEmiData()" autocomplete="off"/> <i class="sp-card-type-icon" aria-hidden="true" id="sp-card-type-icon"></i><p class="sp-error-text" id="sp-txt-card-number-error-text"></p> </div> </div> ';

        payModeStr += '<div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-md-4 _sp-col-sm-3 _sp-col-form-label sp-form-label">Name on card<span class="sp-mandatory">*</span></label> <div class="_sp-col-md-8 _sp-col-sm-9"><input type="text" class="_sp-form-control" placeholder="Name on card" name="sp-txt-card-name" id="sp-txt-card-name" onkeyup="HandleCardInputChange(\'sp-txt-card-name\', \'' + SelectedPaymentMode.payModeId + '\')" autocomplete="off" /> <p class="sp-error-text" id="sp-txt-card-name-error-text"></p></div> </div>';

        payModeStr += '<div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-md-4 _sp-col-sm-3 _sp-col-form-label sp-form-label">Expiry date<span class="sp-mandatory">*</span></label> ';

        payModeStr += '<div class="_sp-col-3 sp-col-sm-6 _sp-pr-2"> <select class="_sp-form-control _sp-form-control-sm" name="sp-txt-card-expiry-month" id="sp-txt-card-expiry-month" onchange="HandleCardInputChange(\'sp-txt-card-expiry-month\', \'' + SelectedPaymentMode.payModeId + '\')"> <option>Month</option> <option value="01">1</option> <option value="02">2</option> <option value="03">3</option> <option value="04">4</option> <option value="05">5</option> <option value="06">6</option> <option value="07">7</option> <option value="08">8</option> <option value="09">9</option> <option value="10">10</option> <option value="11">11</option> <option value="12">12</option> </select> <p class="sp-error-text" id="sp-txt-card-expiry-month-error-text"></p></div>';

        payModeStr += ' <div class="_sp-col-3 sp-col-sm-6 _sp-pr-2"> <select class="_sp-form-control _sp-form-control-sm" name="sp-txt-card-expiry-year" id="sp-txt-card-expiry-year" onchange="HandleCardInputChange(\'sp-txt-card-expiry-year\', \'' + SelectedPaymentMode.payModeId + '\')"> <option>Year</option> <option value="2020">2020</option> <option value="2021">2021</option> <option value="2022">2022</option> <option value="2023">2023</option> <option value="2024">2024</option> <option value="2025">2025</option> <option value="2026">2026</option> <option value="2027">2027</option> <option value="2028">2028</option> <option value="2029">2029</option> <option value="2030">2030</option> <option value="2031">2031</option> <option value="2032">2032</option> </select><p class="sp-error-text" id="sp-txt-card-expiry-year-error-text"></p> </div>';

        payModeStr += ' </div> ';

        payModeStr += '<div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-md-4 _sp-col-sm-3 _sp-col-form-label sp-form-label">CVV<span class="sp-mandatory">*</span></label> <div class="_sp-col-3 sp-col-sm-6"> <input type="password" class="_sp-form-control" placeholder="CVV"  name="sp-txt-card-cvv" id="sp-txt-card-cvv" onkeyup="HandleCardInputChange(\'sp-txt-card-cvv\', \'' + SelectedPaymentMode.payModeId + '\')" /> <p class="sp-error-text" id="sp-txt-card-cvv-error-text"></p> </div> <div class="_sp-col-1 _sp-px-1"><span class="sp-cvv-info-icon" onclick="ShowCvvInfo()"><img src="' + SvGIconSet.infoIcon + '" /></span></div>';

        payModeStr += '<div class="_sp-col-4"> <!--<div class="_sp-form-group"> <div class="_sp-form-check sp-check-control"> <input class="_sp-form-check-input" type="checkbox" name="sp-txt-save-card" id="sp-txt-save-card" onkeyup="HandleCardInputChange(\'sp-txt-save-card\', \'' + SelectedPaymentMode.payModeId + '\')"/> <label class="_sp-form-check-label sp-form-check-label" for="sp-txt-save-card">Save this Card</label> </div> </div> --></div> </div> ';

        payModeStr += '</form> ';
        payModeStr += '</div> </div></div>'; // Accordian body end for card details
        payModeStr += '</div>';

        if (SelectedPaymentMode.payModeId == 'CC' || SelectedPaymentMode.payModeId == 'CE') {
            payModeStr += '<hr class="sp-hr _sp-m-0" id="sp-emi-hr" style="display:none"/><div id="sp-emi-break-details"></div>';
        }

        payModeStr += '</div>'; //Accordian End

        document.getElementById('sp-card-payment').innerHTML = payModeStr;
        // document.getElementById('sp-card-payment').style.display = 'block';
        document.getElementById('sp-card-payment').style.display = 'block';

        if (SelectedPaymentMode.payModeId == 'DC' || SelectedPaymentMode.payModeId == 'CC') {
            document.getElementById('sp-selected-mode-text').innerText = 'Pay using card';
            document.getElementById('sp-payment-mode-card').classList.add('active');
        }
        else {
            document.getElementById('sp-selected-mode-text').innerText = 'Pay using instalments';
            document.getElementById('sp-payment-mode-epp').classList.add('active');
        }
        // if (SelectedPaymentMode.payModeId == 'CC') {
        //     document.getElementById('sp-payment-mode-credit-card').classList.add('active');
        // }
        document.getElementById('sp-pay-now-content').style.display = 'flex';
    }
    RenderCardPayementWithoutAccordian = function () {

        let savedCardList = [];
        if (SavedCards.length > 0) {
            for (let i = 0; i < SavedCards.length; i++) {
                if (SavedCards[i].payModeId == SelectedPaymentMode.payModeId) {
                    savedCardList.push(SavedCards[i]);
                }
            }
        }

        //console.log("Saved Cards", savedCardList);

        let payModeStr = '<div class="sp-payment-list">';
        // for (let i = 0; i < 2; i++) {
        //     // for (let i = 0; i < savedCardList.length; i++) {
        //     payModeStr += '<div class="_sp-carousel-item ';
        //     if (i == 0) payModeStr += "active"; // for active card
        //     let cardItemStyle = '';
        //     payModeStr += '"> <div class="sp-payment-list-item"> <div class="sp-payment-list-item-card">';
        //     let cardImg = '';
        //     let cardType = GetCardType(savedCardList[0].first6Digits);

        //     payModeStr += '<div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div> <img src="./assets/img/visa.gif" class="sp-card-img" /> </div> <div class="sp-card-details"> <div class="sp-card-details"> <div> <!-- <div><span>VISA</span></div> --> <div class="sp-card-number"><span>**** **** **** ' + savedCardList[0].last4Digits + '</span></div> </div> <div> <div class="_sp-form-row _sp-align-items-center"> <div class="_sp-col-12"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">Exp.Date</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YYYY" value="' + savedCardList[0].expiryMonth + '/' + savedCardList[0].expiryYear + '" /> </div> </div> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-3 sp-card-control-label">CVV</label> <div class="_sp-col-7 _sp-pl-1"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder=""/> </div> </div> </div> </div> </div> </div> </div> </div></div> </div> </div>';

        // }
        // payModeStr += '</div> </div></div>';
        for (let i = 0; i < savedCardList.length; i++) {

            payModeStr += '<div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div id="sp-saved-card-icon-' + i + '"> <!--<img src="' + assetsUrl + '/assets/img/visa.gif" class="sp-card-img" />--> </div> <div class="sp-card-details"> <div class="sp-card-details"> <div> <!-- <div><span>VISA</span></div> --> <div class="sp-card-number"><span>**** **** **** ' + savedCardList[i].last4Digits + '</span></div> </div> <div> <div class="_sp-form-row _sp-align-items-center"> <div class="_sp-col-12"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">Exp.Date</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YY" value="' + savedCardList[i].expiryMonth + '/' + savedCardList[i].expiryYear + '" autocomplete="off" /> </div> </div> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">CVV</label> <div class="_sp-col-8"> <input type="password" class="_sp-form-control _sp-form-control-sm sp-card-control" id="sp-saved-card-cvv-' + SelectedPaymentMode.payModeId + '-' + i + '" placeholder="" onkeyup="HandleCvvForSavedCard(\'sp-saved-card-cvv-' + SelectedPaymentMode.payModeId + '-' + i + '\', ' + i + ')"> </div> </div> </div> </div> </div> </div> </div> </div> <div class="sp-right-arrow"> </div> </div>';
            let maskedCardNo = savedCardList[i].first6Digits.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            GetCardTypeByCardNo(maskedCardNo, 'sp-saved-card-icon-' + i);
        }
        // payModeStr += '<div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div> <img src="./assets/img/visa.gif" class="sp-card-img" /> </div> <div class="sp-card-details"> <div class="sp-card-details"> <div> <!-- <div><span>VISA</span></div> --> <div class="sp-card-number"><span>**** **** **** 1234</span></div> </div> <div> <div class="_sp-form-row _sp-align-items-center"> <div class="_sp-col-12"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">Exp.Date</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YY"> </div> </div> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-3 sp-card-control-label">CVV</label> <div class="_sp-col-7 _sp-pl-1"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder=""> </div> </div> </div> </div> </div> </div> </div> </div> <div class="sp-right-arrow"> <!-- <span class="fa fa-chevron-right sp-vertical-center" style="top:40%;"></span> --> </div> </div>';

        payModeStr += '</div><div> <p class="sp-content-label _sp-mb-3 _sp-mt-0">Enter card details</p> <form class="sp-form">';

        payModeStr += '<div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-md-3 _sp-col-sm-3 _sp-col-form-label sp-form-label">Card Number<span class="sp-mandatory">*</span></label> <div class="_sp-col-md-8 _sp-col-sm-9"><input type="text" class="_sp-form-control" placeholder="Enter card number" name="sp-txt-card-number" id="sp-txt-card-number" onkeyup="HandleCardInputChange(\'sp-txt-card-number\', \'' + SelectedPaymentMode.payModeId + '\')" onblur="GetEmiData()" autocomplete="off"/> <i class="sp-card-type-icon" aria-hidden="true" id="sp-card-type-icon"></i><p class="sp-error-text" id="sp-txt-card-number-error-text"></p> </div> </div> ';

        payModeStr += '<div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-md-3 _sp-col-sm-3 _sp-col-form-label sp-form-label">Name on card<span class="sp-mandatory">*</span></label> <div class="_sp-col-md-8 _sp-col-sm-9"><input type="text" class="_sp-form-control" placeholder="Name on card" name="sp-txt-card-name" id="sp-txt-card-name" onkeyup="HandleCardInputChange(\'sp-txt-card-name\', \'' + SelectedPaymentMode.payModeId + '\')" autocomplete="off"/> <p class="sp-error-text" id="sp-txt-card-name-error-text"></p></div> </div>';

        payModeStr += '<div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-md-3 _sp-col-sm-3 _sp-col-form-label sp-form-label">Expiry Date<span class="sp-mandatory">*</span></label> ';

        payModeStr += '<div class="_sp-col-3 sp-col-sm-6"> <select class="_sp-form-control _sp-form-control-sm" name="sp-txt-card-expiry-month" id="sp-txt-card-expiry-month" onchange="HandleCardInputChange(\'sp-txt-card-expiry-month\', \'' + SelectedPaymentMode.payModeId + '\')"> <option>Month</option> <option value="01">1</option> <option value="02">2</option> <option value="03">3</option> <option value="04">4</option> <option value="05">5</option> <option value="06">6</option> <option value="07">7</option> <option value="08">8</option> <option value="09">9</option> <option value="10">10</option> <option value="11">11</option> <option value="12">12</option> </select> <p class="sp-error-text" id="sp-txt-card-expiry-month-error-text"></p></div>';

        payModeStr += ' <div class="_sp-col-3 sp-col-sm-6"> <select class="_sp-form-control _sp-form-control-sm" name="sp-txt-card-expiry-year" id="sp-txt-card-expiry-year" onchange="HandleCardInputChange(\'sp-txt-card-expiry-year\', \'' + SelectedPaymentMode.payModeId + '\')"> <option>Year</option> <option value="2020">2020</option> <option value="2021">2021</option> <option value="2022">2022</option> <option value="2023">2023</option> <option value="2024">2024</option> <option value="2025">2025</option> <option value="2026">2026</option> <option value="2027">2027</option> <option value="2028">2028</option> <option value="2029">2029</option> <option value="2030">2030</option> <option value="2031">2031</option> <option value="2032">2032</option> </select><p class="sp-error-text" id="sp-txt-card-expiry-year-error-text"></p> </div>';

        payModeStr += ' </div> ';

        payModeStr += '<div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-md-3 _sp-col-sm-3 _sp-col-form-label sp-form-label">CVV<span class="sp-mandatory">*</span></label> <div class="_sp-col-3 sp-col-sm-6"> <input type="password" class="form-control" placeholder="CVV"  name="sp-txt-card-cvv" id="sp-txt-card-cvv" onkeyup="HandleCardInputChange(\'sp-txt-card-cvv\', \'' + SelectedPaymentMode.payModeId + '\')" /> <p class="sp-error-text" id="sp-txt-card-cvv-error-text"></p> </div> ';

        payModeStr += '<div class="_sp-col-5"> <!--<div class="_sp-form-group"> <div class="_sp-form-check sp-check-control"> <input class="_sp-form-check-input" type="checkbox" name="sp-txt-save-card" id="sp-txt-save-card" onkeyup="HandleCardInputChange(\'sp-txt-save-card\', \'' + SelectedPaymentMode.payModeId + '\')"/> <label class="_sp-form-check-label sp-form-check-label" for="sp-txt-save-card">Save this Card</label> </div> </div> --></div> </div> ';

        payModeStr += '</form> </div>';

        if (SelectedPaymentMode.payModeId == 'CC' || SelectedPaymentMode.payModeId == 'CE') {
            payModeStr += '<hr class="sp-hr" id="sp-emi-hr" style="display:none"/><div id="sp-emi-break-details"></div>';
        }

        document.getElementById('sp-card-payment').innerHTML = payModeStr;
        // document.getElementById('sp-card-payment').style.display = 'block';
        document.getElementById('sp-card-payment').style.display = 'block';

        if (SelectedPaymentMode.payModeId == 'DC' || SelectedPaymentMode.payModeId == 'CC') {
            document.getElementById('sp-selected-mode-text').innerText = 'Pay using card';
            document.getElementById('sp-payment-mode-card').classList.add('active');
        }
        else {
            document.getElementById('sp-selected-mode-text').innerText = 'Pay using instalments';
            document.getElementById('sp-payment-mode-epp').classList.add('active');
        }
        // if (SelectedPaymentMode.payModeId == 'CC') {
        //     document.getElementById('sp-payment-mode-credit-card').classList.add('active');
        // }
        document.getElementById('sp-pay-now-content').style.display = 'flex';
    }
    /******* Render Card Payment Option ******* */
    RenderEPPCardPayement = function () {

        let savedCardList = [];
        if (SavedCards.length > 0) {
            for (let i = 0; i < SavedCards.length; i++) {
                if (SavedCards[i].payModeId == SelectedPaymentMode.payModeId) {
                    savedCardList.push(SavedCards[i]);
                }
            }
        }

        //console.log("Saved Cards", savedCardList);

        let payModeStr = '<div class="sp-payment-list"> <div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div> <img src="' + assetsUrl + '/assets/img/visa.gif" class="sp-card-img" /> </div> <div class="sp-card-details"> <div class="sp-card-details"> <div> <!-- <div><span>VISA</span></div> --> <div class="sp-card-number"><span>**** **** **** 1234</span></div> </div> <div> <div class="_sp-form-row _sp-align-items-center"> <div class="_sp-col-12"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">Exp.Date</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YY" autocomplete="off"/> </div> </div> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-3 sp-card-control-label">CVV</label> <div class="_sp-col-7 _sp-pl-1"> <input type="password" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder=""> </div> </div> </div> </div> </div> </div> </div> </div> <div class="sp-right-arrow"> </div> </div> <div class="sp-payment-list-item"> <div class="sp-payment-list-item-card"> <div> <img src="' + assetsUrl + '/assets/img/visa.gif" class="sp-card-img" /> </div> <div class="sp-card-details"> <div class="sp-card-details"> <div> <!-- <div><span>VISA</span></div> --> <div class="sp-card-number"><span>**** **** **** 1234</span></div> </div> <div> <div class="_sp-form-row _sp-align-items-center"> <div class="_sp-col-12"> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-4 sp-card-control-label">Exp.Date</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder="MM/YY"autocomplete="off" /> </div> </div> <div class="_sp-form-group _sp-row _sp-mb-0"> <label for="inlineFormInput" class="_sp-col-3 sp-card-control-label">CVV</label> <div class="_sp-col-7 _sp-pl-1"> <input type="password" class="_sp-form-control _sp-form-control-sm sp-card-control" id="inlineFormInput" placeholder=""> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> <div class="_sp-row"> <div class="_sp-col-11"> <div class="_sp-acc-kontainer"> <div> <input type="radio" name="acc" id="acc1" class="_sp-acc-input" checked> <label for="acc1" class="_sp-acc-label"> <p class="sp-content-label sp-acc-label"><span>+ Add New Card</span> <!-- <span class="fa fa-chevron-down sp-acc-icon"></span> --> <span class="sp-acc-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ1MS44NDcgNDUxLjg0NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBkPSJNMjI1LjkyMywzNTQuNzA2Yy04LjA5OCwwLTE2LjE5NS0zLjA5Mi0yMi4zNjktOS4yNjNMOS4yNywxNTEuMTU3Yy0xMi4zNTktMTIuMzU5LTEyLjM1OS0zMi4zOTcsMC00NC43NTEgICBjMTIuMzU0LTEyLjM1NCwzMi4zODgtMTIuMzU0LDQ0Ljc0OCwwbDE3MS45MDUsMTcxLjkxNWwxNzEuOTA2LTE3MS45MDljMTIuMzU5LTEyLjM1NCwzMi4zOTEtMTIuMzU0LDQ0Ljc0NCwwICAgYzEyLjM2NSwxMi4zNTQsMTIuMzY1LDMyLjM5MiwwLDQ0Ljc1MUwyNDguMjkyLDM0NS40NDlDMjQyLjExNSwzNTEuNjIxLDIzNC4wMTgsMzU0LjcwNiwyMjUuOTIzLDM1NC43MDZ6IiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /> </span> </p> </label> <div class="_sp-acc-body"> <form class="sp-form"> <div class="_sp-form-group _sp-row"> <!-- <i class="fa fa-credit-card _sp-form-control-icon" aria-hidden="true"></i> --> <label for="staticEmail" class="_sp-col-4 _sp-col-form-label sp-form-label">Card Number</label> <div class="_sp-col-8"> <input type="text" class="_sp-form-control" placeholder="Enter card number" name="sp-txt-emi-card-number" id="sp-txt-emi-card-number" onkeyup="HandleCardInputChange(\'sp-txt-emi-card-number\', \'' + SelectedPaymentMode.payModeId + '\', \'emi\')" autocomplete="off" /> </div> </div> <div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-4 _sp-col-form-label sp-form-label" name="sp-txt-emi-card-name" id="sp-txt-emi-card-name" onkeyup="HandleCardInputChange(\'sp-txt-emi-card-name\', \'' + SelectedPaymentMode.payModeId + '\', \'emi\')">Name on card</label> <!-- <i class="fa fa-user _sp-form-control-icon" aria-hidden="true"></i> --> <div class="_sp-col-8"> <input type="text" class="_sp-form-control" placeholder="Name on card" name="sp-txt-card-name" id="sp-txt-card-name" autocomplete="off" /> </div> </div> <div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-4 _sp-col-form-label sp-form-label">Expiry Date</label> <!-- <i class="fa fa-calendar-alt _sp-form-control-icon" aria-hidden="true"></i> --> <div class="_sp-col-3"> <!-- <input type="text" class="_sp-form-control" placeholder="Name on card" name="sp-txt-card-name" id="sp-txt-card-name" autocomplete="off" /> --> <select class="_sp-form-control _sp-form-control-sm" name="sp-txt-card-expiry-month" id="sp-txt-card-expiry"> <option>Month</option> <option value="01">1</option> <option value="02">2</option> <option value="03">3</option> <option value="04">4</option> <option value="05">5</option> <option value="06">6</option> <option value="07">7</option> <option value="08">8</option> <option value="09">9</option> <option value="10">10</option> <option value="11">11</option> <option value="12">12</option> </select> </div> <div class="_sp-col-3"> <select class="_sp-form-control _sp-form-control-sm" name="sp-txt-card-expiry-year" id="sp-txt-card-expiry"> <option>Year</option> <option value="20">2020</option> <option value="21">2021</option> <option value="22">2022</option> <option value="23">2023</option> <option value="24">2024</option> <option value="25">2025</option> <option value="26">2026</option> <option value="27">2027</option> <option value="28">2028</option> <option value="29">2029</option> <option value="30">2030</option> <option value="31">2031</option> <option value="32">2032</option> </select> </div> </div> <div class="_sp-form-group _sp-row"> <label for="staticEmail" class="_sp-col-4 _sp-col-form-label sp-form-label">CVV</label> <!-- <i class="fa fa-user _sp-form-control-icon" aria-hidden="true"></i> --> <div class="_sp-col-3"> <input type="password" class="_sp-form-control" placeholder="CVV" name="sp-txt-card-cvv" id="sp-txt-card-cvv" /> </div> <div class="_sp-col-4"> <div class="_sp-form-group"> <div class="_sp-form-check sp-check-control"> <input class="_sp-form-check-input" type="checkbox" id="sp-txt-save-card" id="sp-txt-save-card"> <label class="_sp-form-check-label sp-form-check-label" for="sp-txt-save-card">Save this Card</label> </div> </div> </div> </div> </form> </div> </div> <hr class="sp-hr" /> <div> <input type="radio" name="acc" id="acc2" class="_sp-acc-input"> <label for="acc2" class="_sp-acc-label"> <p class="sp-content-label sp-acc-label"><span class="sp-epp-acc-title active"><span>EPP</span> <span class="sp-epp-option-available" id="sp-epp-option-available">Option available on this card</span> <span class="_sp-px-1"> <label class="_sp-switch"> <input type="checkbox"> <span class="_sp-slider _sp-round"></span> </label> </span></span> <!-- <span class="fa fa-chevron-down sp-acc-icon"></span> --> <span class="sp-acc-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ1MS44NDcgNDUxLjg0NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBkPSJNMjI1LjkyMywzNTQuNzA2Yy04LjA5OCwwLTE2LjE5NS0zLjA5Mi0yMi4zNjktOS4yNjNMOS4yNywxNTEuMTU3Yy0xMi4zNTktMTIuMzU5LTEyLjM1OS0zMi4zOTcsMC00NC43NTEgICBjMTIuMzU0LTEyLjM1NCwzMi4zODgtMTIuMzU0LDQ0Ljc0OCwwbDE3MS45MDUsMTcxLjkxNWwxNzEuOTA2LTE3MS45MDljMTIuMzU5LTEyLjM1NCwzMi4zOTEtMTIuMzU0LDQ0Ljc0NCwwICAgYzEyLjM2NSwxMi4zNTQsMTIuMzY1LDMyLjM5MiwwLDQ0Ljc1MUwyNDguMjkyLDM0NS40NDlDMjQyLjExNSwzNTEuNjIxLDIzNC4wMTgsMzU0LjcwNiwyMjUuOTIzLDM1NC43MDZ6IiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /> </span> </p> </label> <div class="_sp-acc-body"> <div class="sp-epp-body"> <p class="_sp-epp-content-text">We does not levy any charges for availing EMI. Charges, If any are levied by the bank. Please check with your bank for charges related to interest, processing fees, refund or pre-closure. </p> <div class="_sp-row"> <div class="_sp-col-6"> <div class="_sp-row _sp-mb-1"> <div class="_sp-col-12"> <div class="_sp-form-check"> <input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-1" value="3month"> <label class="_sp-form-check-label" for="emi-option-1"> <span class="sp-epp-emi-options _sp-row"> <span class="_sp-col-8 sp-epp-emi-months _sp-pr-0">AED 1701.51 for 3 months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ 12.5% p.a.</span> </span> </label> </div> </div> </div> <div class="_sp-row _sp-mb-1"> <div class="_sp-col-12"> <div class="_sp-form-check"> <input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-2" value="6month"> <label class="_sp-form-check-label" for="emi-option-2"> <span class="sp-epp-emi-options _sp-row"> <span class="_sp-col-8 sp-epp-emi-months _sp-pr-0">AED 863.98 for 6 months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ 12.5% p.a.</span> </span> </label> </div> </div> </div> <div class="_sp-row _sp-mb-1"> <div class="_sp-col-12"> <div class="_sp-form-check"> <input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-3" value="9month"> <label class="_sp-form-check-label" for="emi-option-3"> <span class="sp-epp-emi-options _sp-row"> <span class="_sp-col-8 sp-epp-emi-months _sp-pr-0">AED 587.27 for 9 months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ 13.5% p.a.</span> </span> </label> </div> </div> </div> <div class="_sp-row _sp-mb-1"> <div class="_sp-col-12"> <div class="form-check"> <input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-4" value="12month"> <label class="_sp-form-check-label" for="emi-option-4"> <span class="sp-epp-emi-options _sp-row"> <span class="_sp-col-8 sp-epp-emi-months _sp-pr-0">AED 447.76 for 12 months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ 13.5% p.a.</span> </span> </label> </div> </div> </div> <div class="_sp-row _sp-mb-1"> <div class="_sp-col-12"> <div class="_sp-form-check"> <input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-5" value="24month"> <label class="_sp-form-check-label" for="emi-option-5"> <span class="sp-epp-emi-options _sp-row"> <span class="_sp-col-8 sp-epp-emi-months _sp-pr-0">AED 00 for 24 months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ 0.0% p.a.</span> </span> </label> </div> </div> </div> <div class="_sp-row _sp-mb-1"> <div class="_sp-col-12"> <div class="_sp-form-check"> <input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-6" value="36month"> <label class="_sp-form-check-label" for="emi-option-6"> <span class="sp-epp-emi-options _sp-row"> <span class="_sp-col-8 sp-epp-emi-months pr-0">AED 00 for 36 months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ 0.0% p.a.</span> </span> </label> </div> </div> </div> </div> <div class="_sp-col-6"> <div class="_sp-acc-kontainer"> <div class="sp-emi-breckdown"> <input type="radio" name="emi-acc" id="emi-acc1" class="_sp-acc-input" checked> <label for="emi-acc1" class="_sp-acc-label sp-emi-breckdown-label _sp-px-1"> <p class="sp-content-label sp-acc-label sp-emi-breckdown-label-text"> <span>View EMI Breakup Details</span> <span class="sp-emi-breckdown-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ0OCA0NDgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPjxwYXRoIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZD0ibTQwOCAxODRoLTEzNmMtNC40MTc5NjkgMC04LTMuNTgyMDMxLTgtOHYtMTM2YzAtMjIuMDg5ODQ0LTE3LjkxMDE1Ni00MC00MC00MHMtNDAgMTcuOTEwMTU2LTQwIDQwdjEzNmMwIDQuNDE3OTY5LTMuNTgyMDMxIDgtOCA4aC0xMzZjLTIyLjA4OTg0NCAwLTQwIDE3LjkxMDE1Ni00MCA0MHMxNy45MTAxNTYgNDAgNDAgNDBoMTM2YzQuNDE3OTY5IDAgOCAzLjU4MjAzMSA4IDh2MTM2YzAgMjIuMDg5ODQ0IDE3LjkxMDE1NiA0MCA0MCA0MHM0MC0xNy45MTAxNTYgNDAtNDB2LTEzNmMwLTQuNDE3OTY5IDMuNTgyMDMxLTggOC04aDEzNmMyMi4wODk4NDQgMCA0MC0xNy45MTAxNTYgNDAtNDBzLTE3LjkxMDE1Ni00MC00MC00MHptMCAwIiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+PC9nPjwvc3ZnPg==" /> </span> </p> </label> <div class="_sp-acc-body sp-emi-breckdown-body"> <div class="_sp-row"> <div class="_sp-col-8"> <span>Order value</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>5000</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Order value</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>5000</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Interest ( charged by bank)</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>285</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Processing fees</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>00</span> </div> </div> <hr class="sp-hr" /> <div class="_sp-row"> <div class="_sp-col-8"> <span><b>Total EMI for 9 months</b></span> </div> <div class="_sp-col-4 _sp-text-right"> <span><b>5285 AED</b></span> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div> </div>';

        document.getElementById('sp-epp').innerHTML = payModeStr;
        // document.getElementById('sp-card-payment').style.display = 'block';
        document.getElementById('sp-epp').style.display = 'block';
        document.getElementById('sp-selected-mode-text').innerText = 'Pay by instalments';
        document.getElementById('sp-payment-mode-epp').classList.add('active');
        document.getElementById('sp-pay-now-content').style.display = 'flex';
    }

    /******* Render Net Banking Option ******* */
    RenderNetBankingPayement = function () {
        let payModeStr = '<div class="sp-contact-payment-modes"> <div class="_sp-row sp-net-bank-list">';

        let netBank = '';
        // console.log("SelectedOptions::", SelectedPaymentMode);
        let len = SelectedPaymentMode.paymentModeDetailsList.length > 5 ? 6 : SelectedPaymentMode.paymentModeDetailsList.length;
        for (let i = 0; i < len; i++) {
            netBank += '<div class="_sp-col-3 _sp-text-center sp-net-bank-item" onclick="SelectPayOption(\'' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_id + '\')"> <div class="sp-net-bank-item-div" id="sp-net-bank-option-' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_id + '"> ';

            let netBankImg = '<img src="' + assetsUrl + '/assets/img/icons/bank.png" /> ';
            if (SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_icon) {
                netBankImg = '<img src="' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_icon + '" />';
            }
            netBank += netBankImg;

            netBank += '<p>' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_name + '</p> </div> </div>';
        }
        payModeStr += netBank;
        // payModeStr += '<div class="_sp-col-4 _sp-text-center sp-net-bank-item"> <div class="sp-net-bank-item-div"> <img src="./assets/img/sbi.png" /> <p>SBI</p> </div> </div>';

        //payModeStr += '<div class="_sp-col-4 _sp-text-center sp-net-bank-item"> <div class="sp-net-bank-item-div"> <img src="./assets/img/hdfc.png" /> <p>HDFC</p> </div> </div> <div class="_sp-col-4 _sp-text-center sp-net-bank-item"> <div class="sp-net-bank-item-div"> <img src="./assets/img/icici.png" /> <p>ICICI</p> </div> </div> <div class="_sp-col-4 _sp-text-center sp-net-bank-item"> <div class="sp-net-bank-item-div"> <img src="./assets/img/axis.png" /> <p>AXIS</p> </div> </div> <div class="_sp-col-4 _sp-text-center sp-net-bank-item"> <div class="sp-net-bank-item-div"> <img src="./assets/img/kotakbank.png" /> <p>KOTAK</p> </div> </div> <div class="_sp-col-4 _sp-text-center sp-net-bank-item"> <div class="sp-net-bank-item-div"> <img src="./assets/img/yesbank.png" /> <p>YES</p> </div> </div>';

        payModeStr += ' </div> <div> <p class="sp-content-label _sp-mb-2">Select Bank</p> <div class="_sp-form-group">';
        payModeStr += '<select class="_sp-form-control sp-select-control" id="sp-net-bank-select" name="sp-net-bank-select" onchange="ChoosePayOption(\'netbanking\')"> <option>Select different Bank</option> ';
        let bankOptions = '';
        for (let i = 0; i < SelectedPaymentMode.paymentModeDetailsList.length; i++) {
            bankOptions += '<option value="' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_id + '">' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_name + '</option>';
        }
        payModeStr += bankOptions; // Available Net Bank list.

        payModeStr += '</select> </div> </div> </div>';

        // ToDo: Bind dynamic options from scheme api.

        document.getElementById('sp-net-banking').innerHTML = payModeStr;

        document.getElementById('sp-net-banking').style.display = 'block';
        document.getElementById('sp-selected-mode-text').innerText = 'Pay using net banking';
        document.getElementById('sp-payment-mode-net-banking').classList.add('active');
        document.getElementById('sp-pay-now-content').style.display = 'flex';
    }

    /******* Render Wallet Option ******* */
    RenderWalletPayement = function () {
        let payModeStr = '<div class="sp-contact-payment-modes"> <div class="_sp-row sp-net-bank-list"> ';
        let wallet = '';
        // console.log("SelectedOptions::", SelectedPaymentMode);
        let len = SelectedPaymentMode.paymentModeDetailsList.length > 5 ? 6 : SelectedPaymentMode.paymentModeDetailsList.length;
        for (let i = 0; i < len; i++) {
            wallet += '<div class="_sp-col-3 _sp-text-center sp-net-bank-item" onclick="SelectPayOption(\'' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_id + '\')"> <div class="sp-net-bank-item-div" id="sp-wallet-option-' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_id + '">';

            let walletImg = '<img src="' + assetsUrl + '/assets/img/icons/wallet.png" /> ';
            if (SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_icon) {
                walletImg = '<img src="' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_icon + '" />';
            }
            wallet += walletImg;

            wallet += '<p>' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_name + '</p> </div> </div>';
        }
        payModeStr += wallet;

        payModeStr += '</div><div> <p class="sp-content-label _sp-mb-2">Select Wallet</p> <div class="_sp-form-group"> <select class="_sp-form-control sp-select-control" id="sp-wallet-select" name="sp-wallet-select" onchange="ChoosePayOption(\'wallet\')"> <option>Select different Wallet</option> ';
        let walletOptions = '';
        for (let i = 0; i < SelectedPaymentMode.paymentModeDetailsList.length; i++) {
            walletOptions += '<option value="' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_id + '">' + SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_name + '</option>';
        }
        payModeStr += walletOptions;
        // payModeStr +='<option>Jio Money</option> <option>Ola Money</option> <option>Freecharge</option>';
        payModeStr += ' </select> </div> </div> </div>';

        // ToDo: Bind dynamic options from scheme api.

        document.getElementById('sp-wallets').innerHTML = payModeStr;
        document.getElementById('sp-wallets').style.display = 'block';
        document.getElementById('sp-selected-mode-text').innerText = 'Pay using wallet';
        document.getElementById('sp-payment-mode-wallet').classList.add('active');
        document.getElementById('sp-pay-now-content').style.display = 'flex';
    }

    /******* Render UPI Option ******* */
    RenderUPIPayement = function () {
        let payModeStr = '<div class="sp-contact-payment-modes"> <div> <p class="sp-content-back-label" onclick="GoBack()"> <span class="fa fa-arrow-left sp-back-arrow"></span> Back to Payment Methods </p> </div> <div> <div class="sp-payment-methods-list"> <div class="sp-payment-methods-item sp-selected-option"> <div class="sp-payment-mode"> <div class="sp-payment-mode-icon-div"> <img src="' + assetsUrl + '/assets/img/icons/upi.png" class="sp-card-img" /> </div> <div class="sp-payment-mode-name"> <span>UPI</span> </div> </div> <div class="sp-right-arrow"> </div> </div> </div> </div> <div id="sp-upi-details-div" style="min-height:250px;"> <div class="sp-scan-qrcode-div"> <img src="' + assetsUrl + '/assets/img/icons/qrcode.png" class="sp-upi-qrcode-img" /> <div class="sp-qrcode-download-div"> <button class="_sp-btn btn-sm sp-qrcode-download-btn">Download</button> </div> </div> <p class="sp-content-label _sp-mb-2">Pay Using UPI Id</p> <div class="sp-upi-form-div"> <div> <img src="' + assetsUrl + '/assets/img/icons/upi.png" class="sp-upi-icon" /> </div> <div style="width:100%"> <div> <p class="sp-upi-heading">UPI ID</p> <p class="sp-upi-sub-heading">Google Pay, PhonePe, BHIM & more</p> </div> <div> <form class="sp-form _sp-p-0"> <div class="_sp-form-group _sp-mr-3 _sp-mb-2"> <input type="text" class="_sp-form-control _sp-pl-2" placeholder="Enter your UPI ID" name="sp-txt-vpa" id="sp-txt-vpa" autocomplete="off"/> </div> </form> </div> </div> </div> </div> </div>';

        // ToDo: Bind dynamic options from scheme api.

        document.getElementById('sp-upis').innerHTML = payModeStr;
        // document.getElementById('sp-card-payment').style.display = 'block';
    }


    /******* Reinit all selected modes ******* */
    HideAllModes = function () {

        document.getElementById('sp-card-payment').style.display = 'none';
        document.getElementById('sp-card-payment').innerHTML = '';

        document.getElementById('sp-net-banking').style.display = 'none';
        document.getElementById('sp-net-banking').innerHTML = '';

        document.getElementById('sp-wallets').style.display = 'none';
        document.getElementById('sp-wallets').innerHTML = '';

        document.getElementById('sp-upis').style.display = 'none';
        document.getElementById('sp-upis').innerHTML = '';

        document.getElementById('sp-epp').style.display = 'none';
        document.getElementById('sp-epp').innerHTML = '';

        document.getElementById('sp-tabby-details').style.display = 'none';
        document.getElementById('sp-tabby-details').innerHTML = '';

        document.getElementById('sp-applepay').style.display = 'none';
        document.getElementById('sp-applepay').innerHTML = '';

        document.getElementById('sp-tabby-payment-option-selection').style.display = 'none';
        document.getElementById('sp-tabby-payment-option-selection').innerHTML = '';

        /*** Remove Active Class ***/
        // console.log("Going to check Active");
        const el = document.querySelectorAll('.sp-payment-methods-item');
        // console.log("PaymentItem Length:", el.length);
        for (let i = 0; i < el.length; i++) {
            if (el[i].classList.contains("active")) {
                // console.log("Active Contains");
                el[i].classList.remove("active");

            }
        }

    }
    /******* Render Card Payment Option ******* */
    SelectPaymentMode = function (mode) {
        console.log("SelectPaymentMode is called");

        IsEMIEnabled = false;
        // console.log("Mode:::", mode);
        // For loop will find the selected mode object form Available options and set the value to SelectedPymentMode.
        for (let i = 0; i < AvailablePaymentOptions.length; i++) {
            //console.log("Scheme:::", AvailablePaymentOptions[i]);
            // if (mode == 'NB') {
            //     if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'netbanking' || AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'net banking') {
            //         SelectedPaymentMode = AvailablePaymentOptions[i];
            //         break;
            //     }
            // }
            // else if (mode.toLowerCase() == 'card' || mode.toLowerCase() == 'debit card' || mode.toLowerCase() == 'credit card') {

            //     if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == 'debit card') { // it should be manged whether  default card type, i.e. Debit or Credit. For now it will be Debit Card.
            //         SelectedPaymentMode = AvailablePaymentOptions[i];
            //         break;
            //     }
            // }
            // else {
            //     if (AvailablePaymentOptions[i].paymentMode.toLowerCase() == mode) {
            //         SelectedPaymentMode = AvailablePaymentOptions[i];
            //         break;
            //     }
            // }
            if (AvailablePaymentOptions[i].payModeId == mode) {
                SelectedPaymentMode = AvailablePaymentOptions[i];
                break;
            }

        }
        // console.log("SelectedPaymentMode:::", SelectedPaymentMode);
        if (mode == 'DC' || mode == 'CC' || mode == 'CE') {
            HideAllModes();
            RenderCardPayement();
        }

        // if (mode == 'CE') {
        //     HideAllModes();
        //     RenderEPPCardPayement()

        // }

        if (mode == 'NB') {
            HideAllModes();
            RenderNetBankingPayement();
            // document.getElementById('sp-net-banking').style.display = 'block';
            // document.getElementById('sp-selected-mode-text').innerText = 'Pay using net banking';
            // document.getElementById('sp-payment-mode-netbanking').classList.add('active');
            // document.getElementById('sp-pay-now-content').style.display = 'flex';
        }
        if (mode == 'WA') {
            HideAllModes();
            RenderWalletPayement();
        }

        if (mode == 'TB' || mode == 'PL') {
            // alert("This option is under development");
            // return;
            HideAllModes();
            console.log("SelectPaymentMode will call now GoToPayLaterByTabby...");
            // RenderTabbyPayementDetails()
            GoToPayLaterByTabby();
        }
        if (mode == 'UP') {
            HideAllModes();
            document.getElementById('sp-upis').style.display = 'block';
            document.getElementById('sp-selected-mode-text').innerText = 'Pay using UPI';
            document.getElementById('sp-payment-mode-upi').classList.add('active');
            document.getElementById('sp-pay-now-content').style.display = 'flex';
        }
        if (mode == 'EP') {
            HideAllModes();
            document.getElementById('sp-epp').style.display = 'block';
            document.getElementById('sp-selected-mode-text').innerText = 'Pay by instalments';
            document.getElementById('sp-payment-mode-epp').classList.add('active');
            document.getElementById('sp-pay-now-content').style.display = 'flex';
        }
        if (mode == 'applepay') {
            // alert("This option is under development");
            // return;
            HideAllModes();
            document.getElementById('sp-applepay').style.display = 'block';
            document.getElementById('sp-selected-mode-text').innerText = 'Pay using Apple Pay';
            document.getElementById('sp-payment-mode-applepay').classList.add('active');
            document.getElementById('sp-pay-now-content').style.display = 'flex';
        }
        if (mode == 'gpay') {
            alert("This option is under development");
            return;
            HideAllModes();
            document.getElementById('sp-upis').style.display = 'block';
            document.getElementById('sp-selected-mode-text').innerText = 'Pay using GPay';
            document.getElementById('sp-payment-mode-gpay').classList.add('active');
            document.getElementById('sp-pay-now-content').style.display = 'flex';
        }

        if (mode == 'emi') {
            alert("This option is under development");
            return;
            HideAllModes();
            // // document.getElementById('sp-cotomer-details-div').style.display = 'none';
            // document.getElementById('sp-upi-details-div').style.display = 'block';
            // document.getElementById('payment-mode-header').style.display = 'flex';
            // document.getElementById('sp-mode').innerText = 'UPI';
        }
        if (mode == 'paylater') {
            alert("This option is under development");
            return;
            HideAllModes();
            // // document.getElementById('sp-cotomer-details-div').style.display = 'none';
            // document.getElementById('sp-upi-details-div').style.display = 'block';
            // document.getElementById('payment-mode-header').style.display = 'flex';
            // document.getElementById('sp-mode').innerText = 'UPI';
        }

        if (mode == 'PW') {  // wallet payment  // Atul
            HideAllModes();
            console.log("SelectPaymentMode will call now RenderPayWithWallet...");
            RenderPayWithWallet();
        }

        if (mode == 'PG') {  // pay with gift card  // Atul
            HideAllModes();
            console.log("SelectPaymentMode will call now RenderPayWithGiftCard...");
            RenderPayWithGiftCard();
        }
        // document.getElementById('sp-available-payment-modes').style.display = 'none';

        //alert("Mode:;" + mode);
        // if (mode.toLowerCase() == 'card' || mode.toLowerCase() == 'debit card' || mode.toLowerCase() == 'credit card') {
        //     RenderCardPayement();
        //     document.getElementById('sp-card-payment').style.display = 'block';
        // }
        // else if (mode.toLowerCase() == 'netbanking' || mode.toLowerCase() == 'net banking') {
        //     RenderNetBankingPayement();
        //     document.getElementById('sp-net-banking').style.display = 'block';
        // }
        // else if (mode.toLowerCase() == 'wallet') {
        //     RenderWalletPayement();
        //     document.getElementById('sp-wallets').style.display = 'block';
        // }
        // else if (mode.toLowerCase() == 'upi') {
        //     RenderUPIPayement();
        //     document.getElementById('sp-upis').style.display = 'block';
        // }
        // else if (mode == 'emi') {
        //     alert("This option is under development");
        //     return;
        //     // // document.getElementById('sp-cotomer-details-div').style.display = 'none';
        //     // document.getElementById('sp-upi-details-div').style.display = 'block';
        //     // document.getElementById('payment-mode-header').style.display = 'flex';
        //     // document.getElementById('sp-mode').innerText = 'UPI';
        // }
        // else if (mode == 'paylater') {
        //     alert("This option is under development");
        //     return;
        //     // // document.getElementById('sp-cotomer-details-div').style.display = 'none';
        //     // document.getElementById('sp-upi-details-div').style.display = 'block';
        //     // document.getElementById('payment-mode-header').style.display = 'flex';
        //     // document.getElementById('sp-mode').innerText = 'UPI';
        // }
        // else {
        //     alert("This option is under development");
        //     return;
        // }
        // document.getElementById('sp-available-payment-modes').style.display = 'none';

    }

    /******* Back Payment Option ******* */
    GoBack = function () {
        let currentMode = SelectedPaymentMode.payModeId;
        // console.log("CurrentMode::" + currentMode);
        if (currentMode == 'DC' || currentMode == 'CC') {
            document.getElementById('sp-card-payment').style.display = 'none';
            document.getElementById('sp-available-payment-modes').style.display = 'block';

        }
        if (currentMode == 'NB') {
            document.getElementById('sp-net-banking').style.display = 'none';
            document.getElementById('sp-available-payment-modes').style.display = 'block';
        }
        if (currentMode == 'WA') {
            document.getElementById('sp-wallets').style.display = 'none';
            document.getElementById('sp-available-payment-modes').style.display = 'block';
        }
        if (currentMode == 'UP') {
            document.getElementById('sp-upis').style.display = 'none';
            document.getElementById('sp-available-payment-modes').style.display = 'block';
        }
        // document.getElementById('sp-footer-btn').disabled = true;
    }

    /******* Close Modal by clicking on top rigth close button ******* */
    CloseSpModal = function () {
        let modalDiv = document.getElementById('paymentModal');
        modalDiv.parentElement.remove();
        //alert("Hello in close");
    }

    SelectPayOption = function (option) {
        // console.log("InsidePayOptions::", option);
        if (option != null || option != undefined || option != '') {
            // For loop will find the selected mode object form Available options and set the value to SelectedPymentMode.
            for (let i = 0; i < SelectedPaymentMode.paymentModeDetailsList.length; i++) {
                // console.log("OptionMatched::", option)
                if (SelectedPaymentMode.paymentModeDetailsList[i].pgDetailsResponse.pg_id === option) {
                    // console.log("OptionMatched::", option, SelectedPaymentMode.paymentModeDetailsList[i]);
                    SelectedPaymentOption = SelectedPaymentMode.paymentModeDetailsList[i];
                    break;
                }
            }

            /***Add Active Style on the selected Option***/
            const el = document.querySelectorAll('.sp-net-bank-item-div');
            for (let i = 0; i < el.length; i++) {
                if (el[i].classList.contains("active")) {
                    el[i].classList.remove("active");

                }
            }

            if (SelectedPaymentMode.paymentMode.toLowerCase() == 'netbanking' || SelectedPaymentMode.paymentMode.toLowerCase() == 'net banking') {
                document.getElementById('sp-net-bank-select').value = option;
                document.getElementById('sp-net-bank-option-' + option).classList.add('active');
            }
            if (SelectedPaymentMode.paymentMode.toLowerCase() == 'wallet') {
                document.getElementById('sp-wallet-select').value = option;
                document.getElementById('sp-wallet-option-' + option).classList.add('active');
            }


            document.getElementById('sp-footer-btn').disabled = false;
        }
        else {
            console.log("Selected payment option not found");
        }

    }

    ChoosePayOption = function (type) {
        // console.log("InsidePayOptions::", type);
        let opt = null;
        if (type === 'netbanking') {
            opt = document.getElementById('sp-net-bank-select').value;
        }
        if (type === 'wallet') {
            opt = document.getElementById('sp-wallet-select').value;
        }

        SelectPayOption(opt);

    }

    /**** Get all the data from payment modal and crate an object for the API. */
    SetPaymentData = function (data) {
        txnDetails = {
            ag_id: AgId, // paygate for js-checkout and fab for fab
            me_id: MeId,
            order_no: CustomerOrder.orderNo,
            amount: CustomerOrder.amount,
            country: CountryCode,
            currency: CustomerOrder.currencyCode,
            txn_type: "SALE",
            success_url: SuccessUrl,
            failure_url: FailUrl,
            channel: "WEB"
        }
        let pgDetails = {
            pg_id: "",
            Paymode: "",
            Scheme: "",
            emi_months: ""
        }

        // let pgDetails = {
        //     pg_id: "63",
        //     Paymode: "NB",
        //     Scheme: "7",
        //     emi_months: "7"
        // }
        // pgDetails = {
        //     pg_id: "75",
        //     Paymode: "TB",
        //     Scheme: "10",
        //     emi_months: ""
        // }


        // ToDo: Add condition to check the merchant type if it is Merchat hosted then Payment Gateway required.
        // for dev pupouse I'm directly passing it.
        /* let payMode = '';
         if (SelectedPaymentMode.paymentMode.toLowerCase() == 'card' || SelectedPaymentMode.paymentMode.toLowerCase() == 'debit card') {
             payMode = 'DC';
         }
         if (SelectedPaymentMode.paymentMode.toLowerCase() == 'credit card') {
             payMode = 'DC';
         }
 
         if (SelectedPaymentMode.paymentMode.toLowerCase() == 'netbanking' || SelectedPaymentMode.paymentMode.toLowerCase() == 'net banking') {
             payMode = 'NB';
         }
 
         if (SelectedPaymentMode.paymentMode.toLowerCase() == 'wallet') {
             payMode = 'WA';
         }
 
         if (SelectedPaymentMode.paymentMode.toLowerCase() == 'upi') {
             payMode = 'WA';
         }
 
         if (SelectedPaymentMode.paymentMode == 'emi') {
             payMode = 'CE';
         }
 
         if (SelectedPaymentMode.paymentMode == 'paylater') {
             payMode = 'WA';
         }*/
        let billDetails = {
            bill_address: "",
            bill_city: "",
            bill_state: "",
            bill_country: "",
            bill_zip: ""
        }
        let emi = '';
        if (SelectedPaymentOption != null) { // It will work for Net Banking and Wallets;
            // console.log("SelectedPaymentOption is available::", SelectedPaymentOption)

            // if (SelectedPaymentMode.payModeId === 'CE') {
            //     // emi = '7' // only for testing 
            //     emi = RemoveZeros(SelectedEmiPlan?.months);
            // }
            if (SelectedPaymentMode.payModeId === 'TB' || SelectedPaymentMode.payModeId === 'PL') {
                emi = '' // only for testing 
            }
            if ((SelectedPaymentMode.payModeId === 'CE' || SelectedPaymentMode.payModeId === 'CC') && IsEMIEnabled) {
                if (SelectedEmiPlan.months != undefined) {
                    emi = RemoveZeros(SelectedEmiPlan?.months);
                }

                billDetails = CustomerBillingAddress;

            }

            pgDetails = {
                pg_id: SelectedPaymentOption.pgDetailsResponse.pg_id,
                Paymode: SelectedPaymentMode.payModeId,
                Scheme: SelectedPaymentOption.schemeDetailsResponse.schemeId,
                emi_months: emi
            }
        }
        else {
            // console.log("SelectedPaymentOption is available::", SelectedPaymentOption)
            // if (SelectedPaymentMode && (SelectedPaymentMode.payModeId == 'CC')) {
            //     pgDetails = {
            //         pg_id: '107',
            //         Paymode: SelectedPaymentMode.payModeId,
            //         Scheme: '2',
            //         emi_months: emi
            //     }

            // }
        }

        let cardDetails = {
            card_no: "",
            exp_month: "",
            exp_year: "",
            cvv2: "",
            card_name: ""
        }
        if (SelectedPaymentMode && (SelectedPaymentMode.payModeId == 'CC' || SelectedPaymentMode.payModeId == 'CE' || SelectedPaymentMode.payModeId == 'DC')) {
            cardDetails = {
                card_no: CustomerCardDetails.cardNumber,
                exp_month: CustomerCardDetails.cardExpiryMonth,
                exp_year: CustomerCardDetails.cardExpiryYear,
                cvv2: CustomerCardDetails.cardCVV2,
                card_name: CustomerCardDetails.nameOnCard
            }
        }

        let custDetails = {
            cust_name: '',
            email_id: '',
            mobile_no: '',
            unique_id: '',
            is_logged_in: 'Y'
        }

        //// Only For Direct Tabby
        // custDetails = {
        //     cust_name: CustomerDetails.name,
        //     email_id: CustomerDetails.email,
        //     mobile_no: CustomerDetails.phone,
        //     unique_id: CustomerOrder.userId,
        //     is_logged_in: 'Y'
        // }
        if (SelectedPaymentMode && (SelectedPaymentMode.payModeId == 'CC' || SelectedPaymentMode.payModeId == 'CE' || SelectedPaymentMode.payModeId == 'DC' || SelectedPaymentMode.payModeId == 'WA' || SelectedPaymentMode.payModeId == 'TB' || SelectedPaymentMode.payModeId == 'PL')) {
            custDetails = {
                cust_name: CustomerDetails.name,
                email_id: CustomerDetails.email,
                mobile_no: CustomerDetails.phone,
                unique_id: CustomerOrder.userId,
                is_logged_in: 'Y'
            }

            if (CustomerBillingAddress != null) {
                billDetails = CustomerBillingAddress;
            }

        }
        if (IsSavedCardPayment) {
            custDetails = {
                cust_name: '',
                email_id: '',
                mobile_no: '',
                unique_id: CustomerOrder.userId,
                is_logged_in: 'Y'
            }
            cardDetails = {
                card_no: "",
                exp_month: "",
                exp_year: "",
                cvv2: "",
                card_name: ""
            }
        }


        // let billDetails = {
        //     bill_address: "",
        //     bill_city: "",
        //     bill_state: "",
        //     bill_country: "",
        //     bill_zip: ""
        // }

        let shipDetails = {
            ship_address: "",
            ship_city: "",
            ship_state: "",
            ship_country: "",
            ship_zip: "",
            ship_days: "",
            address_count: ""

        }
        let itemDetails = {
            item_count: '',
            item_value: "",
            item_category: ""
        }

        let otherDetails = {
            udf_1: "",
            udf_2: "",
            udf_3: "",
            udf_4: "",
            udf_5: "",
        }

        let postData = {
            txn_details: txnDetails,
            pg_details: pgDetails,
            card_details: cardDetails,
            cust_details: custDetails,
            bill_details: billDetails,
            ship_details: shipDetails,
            item_details: itemDetails,
            other_details: otherDetails
        };
        /*let postData = {
            txn_details: {
                ag_id: "Paygate",
                me_id: "201710270001",
                order_no: "96692adf",
                amount: 1,
                country: "IND",
                currency: "INR",
                txn_type: "SALE",
                success_url: "http://stage.antino.io:4000/paytest/success",
                failure_url: "http://stage.antino.io:4000/paytest/failure",
                channel: "WEB"
            },
            pg_details: {  // this will be only required when integration type will Murchant Hosted.
                pg_id: "",
                Paymode: "",
                Scheme: "",
                emi_months: ""
            },
            card_details: {
                card_no: "5457210001000019",
                exp_month: "12",
                exp_year: "2025",
                cvv2: 123,
                card_name: "Sani Yadav"
            },
            cust_details: {
                cust_name: "Sani Yadav",
                email_id: "sani@antino.io",
                mobile_no: '9999555666',
                unique_id: '1234',
                is_logged_in: 'Y'
            },
            Bill_details: {
                bill_address: "",
                bill_city: "",
                bill_state: "",
                bill_country: "",
                bill_zip: ""
            },
            Ship_details: {
                ship_address: "",
                ship_city: "",
                ship_state: "",
                ship_country: "",
                ship_zip: "",
                ship_days: "",
                address_count: ""
    
            },
            Item_details: {
                item_count: '',
                item_value: "",
                item_category: ""
    
            },
            Other_details: {
                udf_1: "",
                udf_2: "",
                udf_3: "",
                udf_4: "",
                udf_5: "",
            },
            me_id: "201710270001"
    
        }; 
        */

        PaymentData = postData;
    }

    /**** Apply Encryption on data. It will be done in two steps. 
    * Step 1- In this step every data will be encrypted using | (Pipe symbol) and create new object.
    * Step 2- Encrypt the object which is generated by step 1 and return the encrypted object.
    */
    GetEncryptedPostData = function (postData) {
        let postJsonData = {
            txn_details: null,
            pg_details: null,
            card_details: null,
            cust_details: null,
            bill_details: null,
            ship_details: null,
            item_details: null,
            other_details: null

        };
        let encryptedPostData = {
            txn_details: null,
            pg_details: null,
            card_details: null,
            cust_details: null,
            bill_details: null,
            ship_details: null,
            item_details: null,
            other_details: null

        };

        let objKeys = Object.keys(postData);
        for (let i = 0; i < objKeys.length; i++) {
            // console.log("ObjKeys::", objKeys[i]);
            let level1 = postData[objKeys[i]];
            // console.log("level1::", objKeys[i]);
            let keys = Object.keys(level1);
            let str = '';
            for (let j = 0; j < keys.length; j++) {
                let level2 = level1[keys[j]];
                //console.log("Keys::===", keys[j], level2);
                if (keys[j] != 'me_id') {
                    //console.log("Level2::"+level2+"-------------");
                    if (level2 != '') {
                        // str += Encrypt(level2);
                        str += level2;
                        if (j != keys.length - 1) {
                            str += '|';
                        }

                    }
                    else {
                        if (j != keys.length - 1) {
                            str += '|';
                        }
                    }

                }
                else {
                    str += level2;
                    if (j != keys.length - 1) {
                        str += '|';
                    }
                }
            }
            //console.log(objKeys[i], "::", str);
            encryptedPostData[objKeys[i]] = Encrypt(str);
            postJsonData[objKeys[i]] = str;
            str = '';
        }
        //console.log('Merchant Encrypted::', postData.me_id, Encrypt(postData.me_id));
        console.log("PostJsonData::", JSON.stringify(postJsonData, null, 4));
        return encryptedPostData;
    }

    GetCardType = function (number) {
        // visa
        var re = new RegExp("^4");
        if (number.match(re) != null)
            return "VISA";

        // Mastercard 
        // Updated for Mastercard 2017 BINs expansion
        if (/^(5[1-5][0-9]{14}|2(22[1-9][0-9]{12}|2[3-9][0-9]{13}|[3-6][0-9]{14}|7[0-1][0-9]{13}|720[0-9]{12}))$/.test(number))
            return "Mastercard";

        // AMEX
        re = new RegExp("^3[47]");
        if (number.match(re) != null)
            return "AMEX";

        // Discover
        re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
        if (number.match(re) != null)
            return "Discover";

        // Diners
        re = new RegExp("^36");
        if (number.match(re) != null)
            return "Diners";

        // Diners - Carte Blanche
        re = new RegExp("^30[0-5]");
        if (number.match(re) != null)
            return "Diners - Carte Blanche";

        // JCB
        re = new RegExp("^35(2[89]|[3-8][0-9])");
        if (number.match(re) != null)
            return "JCB";

        // Visa Electron
        re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
        if (number.match(re) != null)
            return "Visa Electron";

        // RUPAY
        re = new RegExp("^(508227|508[5-9]|603741|60698[5-9]|60699|607[0-8]|6079[0-7]|60798[0-4]|60800[1-9]|6080[1-9]|608[1-4]|608500|6521[5-9]|652[2-9]|6530|6531[0-4])");
        if (number.match(re) != null)
            return "Rupay";

        // MAESTRO
        re = new RegExp("^(5018|5081|5044|504681|504993|5020|502260|5038|603845|603123|6304|6759|676[1-3]|6220|504834|504817|504645|504775)");
        if (number.match(re) != null)
            return "Maestro";

        return "Unknown";

        //     VISA("Visa", R.drawable.ic_visa_card, 16, "^4.*",3),
        // MASTER_CARD("Master", R.drawable.ic_master_card, 16, "^5[1-5].*",3),
        // DISCOVER("Discover", R.drawable.ic_discover_card, 16, "^(6011|65|64[4-9]|622[1-9]).*",3),
        // MAESTRO("Maestro", R.drawable.ic_maestro_card, 19, "^(5018|5081|5044|504681|504993|5020|502260|5038|603845|603123|6304|6759|676[1-3]|6220|504834|504817|504645|504775).*",3),
        // DINERS_CLUB("Dinners club Int", R.drawable.ic_dinners_club_int_card, 14, "^(3[689]|30[0-5]|309).*",3),
        // AMEX("Amex", R.drawable.ic_amex_card, 15, "^3[47].*",4),
        // RUPAY("Rupay", R.drawable.ic_rupay_card, 16, "^(508227|508[5-9]|603741|60698[5-9]|60699|607[0-8]|6079[0-7]|60798[0-4]|60800[1-9]|6080[1-9]|608[1-4]|608500|6521[5-9]|652[2-9]|6530|6531[0-4]).*",3),
        // UNKNOWN("Unknown", R.drawable.ic_unknown_card, 19, "[1-9]*",3);
    }

    HandleCardInputChange = function (target, type) {
        // console.log("OnchangeType::", target, type);
        document.getElementById('sp-footer-btn').disabled = true; /// to disable the pay now button for payment.

        document.getElementById(target + '-error-text').innerText = "";
        document.getElementById(target).parentNode.classList.remove("sp-has-error");

        let value = document.getElementById(target).value;
        if (target == 'sp-txt-card-name') {
            CustomerCardDetails.nameOnCard = value;
            if (CustomerCardDetails.nameOnCard.trim().length == 0) {
                document.getElementById('sp-txt-card-name-error-text').innerText = "* Enter name on card";
                document.getElementById('sp-txt-card-name').parentNode.classList.add("sp-has-error");
                return;
            }
            if (!CustomerCardDetails.nameOnCard.match(/^[a-zA-Z\s]*$/)) {
                document.getElementById('sp-txt-card-name-error-text').innerText = "* Name must be characters only";
                document.getElementById('sp-txt-card-name').parentNode.classList.add("sp-has-error");
                return;
            }
            if (CustomerCardDetails.nameOnCard.length > 50) {
                document.getElementById('sp-txt-name-error-text').innerText = "* Name can be max 50 character only.";
                document.getElementById('sp-txt-name').parentNode.classList.add("sp-has-error");
                return;
            }

        }
        if (target == 'sp-txt-card-number') {
            document.getElementById('sp-txt-card-number').classList.remove("sp-has-error");
            CustomerCardDetails.cardNumber = value.replace(/\W/gi, '');
            let maskedCardNo = value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            document.getElementById(target).value = maskedCardNo.trim();
            if (maskedCardNo.trim().length > 7) {
                document.getElementById('sp-card-type-icon').innerHTML = ''; // To remove the card image.
                GetCardTypeByCardNo(value, 'sp-card-type-icon');
            }
            if (!CustomerCardDetails.cardNumber.match(/[0-9]/)) {
                document.getElementById('sp-txt-card-number-error-text').innerText = "* Invalid card number";
                document.getElementById('sp-txt-card-number').parentNode.classList.add("sp-has-error");
                document.getElementById('sp-txt-card-number').classList.add("sp-has-error");
                return;
            }

            // console.log("InsideCardHandleInput::", maskedCardNo.trim(), maskedCardNo.trim().length);
            // if (type == 'CC' && maskedCardNo.trim().length > 8) {
            //     // if (maskedCardNo.trim().length == 8) { /// to show EMI options
            //         GetEMIDetailsByCard(maskedCardNo.trim());
            //     // }
            //     // if (maskedCardNo.trim().length == 6) { /// to hide emi option
            //     //     HandleEMIBreakDowns('');
            //     // }
            // }

            if (CustomerCardDetails.cardNumber.length < 9 || CustomerCardDetails.cardNumber.length > 19) {
                document.getElementById('sp-txt-card-number-error-text').innerText = "* Invalid card number";
                document.getElementById('sp-txt-card-number').parentNode.classList.add("sp-has-error");
                document.getElementById('sp-txt-card-number').classList.add("sp-has-error");
                return;
            }


        }
        if (target == 'sp-txt-card-expiry') {
            document.getElementById('sp-txt-card-expiry').classList.remove("sp-has-error");
            let maskedEpiry = value.replace(
                /[^0-9]/g, '' // To allow only numbers
            ).replace(
                /^([2-9])$/g, '0$1' // To handle 3 > 03
            ).replace(
                /^(1{1})([3-9]{1})$/g, '0$1/$2' // 13 > 01/3
            ).replace(
                /^0{1,}/g, '0' // To handle 00 > 0
            ).replace(
                /^([0-1]{1}[0-9]{1})([0-9]{1,4}).*/g, '$1/$2' // To handle 113 > 11/3
            );
            document.getElementById(target).value = maskedEpiry;
            if (maskedEpiry.length == 7) {
                let exp = maskedEpiry.split('/');
                CustomerCardDetails.cardExpiryMonth = exp[0];
                CustomerCardDetails.cardExpiryYear = exp[1];

            }
            else {
                document.getElementById('sp-txt-card-expiry-error-text').innerText = "* Invalid date";
                document.getElementById('sp-txt-card-expiry').parentNode.classList.add("sp-has-error");
                document.getElementById('sp-txt-card-expiry').classList.add("sp-has-error");
                return;
            }


        }

        if (target == 'sp-txt-card-cvv') {
            document.getElementById('sp-txt-card-cvv').classList.remove("sp-has-error");
            CustomerCardDetails.cardCVV2 = value;
            if (CustomerCardDetails.cardCVV2.trim().length == 0) {
                document.getElementById('sp-txt-card-cvv-error-text').innerText = "* Enter CVV";
                document.getElementById('sp-txt-card-cvv').parentNode.classList.add("sp-has-error");
                return;
            }
            if (!CustomerCardDetails.cardCVV2.match(/^[0-9]{3,4}$/)) {
                document.getElementById('sp-txt-card-cvv-error-text').innerText = "* Invalid CVV";
                document.getElementById('sp-txt-card-cvv').parentNode.classList.add("sp-has-error");
                document.getElementById('sp-txt-card-cvv').classList.add("sp-has-error");
                return;
            }
        }

        if (target == 'sp-txt-card-expiry-month') {
            document.getElementById('sp-txt-card-expiry-month').classList.remove("sp-has-error");
            CustomerCardDetails.cardExpiryMonth = document.getElementById(target).value;
        }
        if (target == 'sp-txt-card-expiry-year') {
            document.getElementById('sp-txt-card-expiry-year').classList.remove("sp-has-error");
            CustomerCardDetails.cardExpiryYear = document.getElementById(target).value;
        }
        if (target == 'sp-txt-save-card') {
            CustomerCardDetails.saveCardFlag = value;
        }

        if (ValidateCardDetails()) {

            if (CustomerCardDetails.cardNumber != undefined && CustomerCardDetails.cardNumber.length > 4) {
                let cardType = GetCardType(CustomerCardDetails.cardNumber);
                let list = SelectedPaymentMode.paymentModeDetailsList;
                let found = false;
                for (let i = 0; i < list.length; i++) {
                    if (list[i].schemeDetailsResponse.schemeName.toLowerCase() == cardType.toLowerCase()) {
                        SelectPayOption(list[i].pgDetailsResponse.pg_id);
                        found = true;
                        break;
                    }
                }
                // console.log("CardType::", cardType);
                if (!found) {
                    document.getElementById('sp-txt-card-number-error-text').innerText = "* This card is not accepted by merchant.";
                    document.getElementById('sp-txt-card-number').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-number').classList.add("sp-has-error");
                    return;
                }

            }

            document.getElementById('sp-footer-btn').disabled = false; /// to enable the pay now button for payment.
        }

    }

    /**********Card Validation **********/
    ValidateCardDetails = function () {
        if (CustomerCardDetails.nameOnCard != undefined && CustomerCardDetails.cardNumber != undefined && CustomerCardDetails.cardExpiryMonth != undefined && CustomerCardDetails.cardExpiryYear != undefined && CustomerCardDetails.cardCVV2 != undefined) {
            // console.log("Inside Card Validtaion:: in complete details");
            if (CustomerCardDetails.nameOnCard != undefined) {
                if (CustomerCardDetails.nameOnCard.trim().length == 0) {
                    document.getElementById('sp-txt-card-name-error-text').innerText = "* Enter name on card";
                    document.getElementById('sp-txt-card-name').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-name').classList.add("sp-has-error");
                    return false;
                }
                if (!CustomerCardDetails.nameOnCard.match(/^[a-zA-Z\s]*$/)) {
                    document.getElementById('sp-txt-card-name-error-text').innerText = "* Name must be characters only";
                    document.getElementById('sp-txt-card-name').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-name').classList.add("sp-has-error");
                    return false;
                }
                if (CustomerCardDetails.nameOnCard.length > 50) {
                    document.getElementById('sp-txt-name-error-text').innerText = "* Name can be max 50 character only.";
                    document.getElementById('sp-txt-card-name').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-name').classList.add("sp-has-error");
                    return false;
                }
            }
            if (CustomerCardDetails.cardNumber != undefined) {
                if (!CustomerCardDetails.cardNumber.match(/[0-9]/)) {
                    document.getElementById('sp-txt-card-number-error-text').innerText = "* Invalid card number";
                    document.getElementById('sp-txt-card-number').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-number').classList.add("sp-has-error");
                    return false;
                }
                if (CustomerCardDetails.cardNumber.length < 9 || CustomerCardDetails.cardNumber.length > 19) {
                    document.getElementById('sp-txt-card-number-error-text').innerText = "* Invalid card number";
                    document.getElementById('sp-txt-card-number').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-number').classList.add("sp-has-error");
                    return false;
                }
            }

            // if (CustomerCardDetails.cardExpiryMonth != undefined) {
            //     if (CustomerCardDetails.cardExpiryMonth.trim().length == 0) {
            //         document.getElementById('sp-txt-card-expiry-error-text').innerText = "* Enter Date";
            //         document.getElementById('sp-txt-card-expiry-month').parentNode.classList.add("sp-has-error");
            //         return false;
            //     }
            //     if (document.getElementById('sp-txt-card-expiry').value.trim().length != 7) {
            //         document.getElementById('sp-txt-card-expiry-error-text').innerText = "* Invalid Date";
            //         document.getElementById('sp-txt-card-expiry-month').parentNode.classList.add("sp-has-error");
            //         document.getElementById('sp-txt-card-expiry-month').parentNode.classList.add("sp-has-error");
            //         return false;
            //     }

            // }
            if (CustomerCardDetails.cardExpiryYear != undefined) {
                if (CustomerCardDetails.cardExpiryYear.trim().length == 0) {
                    document.getElementById('sp-txt-card-expiry-year-error-text').innerText = "* Enter year";
                    document.getElementById('sp-txt-card-expiry-year').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-expiry-year').classList.add("sp-has-error");
                    return false;
                }
                // if (document.getElementById('sp-txt-card-expiry').value.trim().length != 7) {
                //     document.getElementById('sp-txt-card-expiry-error-text').innerText = "* Invalid Date";
                //     document.getElementById('sp-txt-card-expiry').parentNode.classList.add("sp-has-error");
                //     return false;
                // }

            }
            if (CustomerCardDetails.cardExpiryMonth != undefined) {
                if (CustomerCardDetails.cardExpiryMonth.trim().length == 0) {
                    document.getElementById('sp-txt-card-expiry-month-error-text').innerText = "* Enter year";
                    document.getElementById('sp-txt-card-expiry-month').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-expiry-month').classList.add("sp-has-error");
                    return false;
                }
                // if (document.getElementById('sp-txt-card-expiry').value.trim().length != 7) {
                //     document.getElementById('sp-txt-card-expiry-error-text').innerText = "* Invalid Date";
                //     document.getElementById('sp-txt-card-expiry').parentNode.classList.add("sp-has-error");
                //     return false;
                // }

            }

            if (CustomerCardDetails.cardCVV2 != undefined) {
                if (CustomerCardDetails.cardCVV2.trim().length == 0) {
                    document.getElementById('sp-txt-card-cvv-error-text').innerText = "* Enter CVV";
                    document.getElementById('sp-txt-card-cvv').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-cvv').classList.add("sp-has-error");
                    return false;
                }
                if (!CustomerCardDetails.cardCVV2.match(/^[0-9]{3}$/)) {
                    document.getElementById('sp-txt-card-cvv-error-text').innerText = "* Invalid CVV";
                    document.getElementById('sp-txt-card-cvv').parentNode.classList.add("sp-has-error");
                    document.getElementById('sp-txt-card-cvv').classList.add("sp-has-error");
                    return false;
                }

            }

            return true;
        }
        else {
            // console.log("Inside Card Validtaion:: incomplete details", CustomerCardDetails);
            return false;
        }


    }

    /*** Manage the Payment logic *** */
    SPPayNow = function () {
        // console.log("InsidePayNow");
        SetPaymentData();
        let encryptedPostData = GetEncryptedPostData(PaymentData);
        encryptedPostData.me_id = MeId;
        console.log("EncryptedPostData", JSON.stringify(encryptedPostData, null, 4));

        /*var formBody = [];
        for (var property in encryptedPostData) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(encryptedPostData[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");*/

        let temp = encryptedPostData;
        let requestPayamentStr = '';


        // let resp = CallAPI('POST', 'https://test.avantgardepayments.com/agcore/payment', formBody, function (response) {
        //     console.log("PaymentResponse:", response);
        //     // requestPayamentStr = response;
        //     OpenResponseContent(response)
        //     // document.getElementById('sp-request-payment').style.display = 'block';
        //     // var iframe = document.getElementById('sp-resp-iframe');
        //     // iframe.contentWindow.document.write(response);


        // }, true);

        let formTarget = '';
        let respDiv = '';
        if (SelectedPaymentMode.payModeId != 'TB') {
            // formTarget = 'target="_blank"';
            formTarget = 'target="safexPayWindow"';

        }
        else {
            // formTarget = 'target="sp-payment-iframe"';
            // respDiv = '<iframe id="sp-payment-iframe" style="height:100%; width:100%"></iframe>';
        }
        requestPayamentStr = '<form class="form-horizontal" id = "avanthgarde_payment_form" action = "' + PaymentUrl + '" method = "POST" ' + formTarget + '> <input type="hidden" class="form-control" name="me_id"  value="' + temp.me_id + '" /> <input type="hidden" class="form-control" name="txn_details" value="' + temp.txn_details + '" /> <input type="hidden" class="form-control" name="pg_details"  value="' + temp.pg_details + '" /> <input type="hidden" class="form-control" name="card_details" value="' + temp.card_details + '" /> <input type="hidden" class="form-control" name="cust_details" value="' + temp.cust_details + '" /> <input type="hidden" class="form-control" name="bill_details"  value="' + temp.bill_details + '" /> <input type="hidden" class="form-control" name="ship_details"  value="' + temp.ship_details + '" /> <input type="hidden" class="form-control" name="item_details"  value="' + temp.item_details + '" /> <input type="hidden" class="form-control" name="other_details"  value="' + temp.other_details + '" /> <button type="submit"  id="avanthgarde_payment_form_btn" style="display:none;">Submit</button></form > ' + respDiv;

        // requestPayamentStr = '<form class="form-horizontal" id = "avanthgarde_payment_form" action = "' + PaymentUrl + '" method = "POST" target="_blank"> <input type="hidden" class="form-control" name="me_id"  value="' + temp.me_id + '" /> <input type="hidden" class="form-control" name="txn_details"  value="' + temp.txn_details + '" /> <input type="hidden" class="form-control" name="pg_details"  value="' + temp.pg_details + '" /> <input type="hidden" class="form-control" name="card_details"  value="' + temp.card_details + '" /> <input type="hidden" class="form-control" name="cust_details"  value="' + temp.cust_details + '" /> <input type="hidden" class="form-control" name="bill_details"  value="' + temp.bill_details + '" /> <input type="hidden" class="form-control" name="ship_details"  value="' + temp.ship_details + '" /> <input type="hidden" class="form-control" name="item_details"  value="' + temp.item_details + '" /> <input type="hidden" class="form-control" name="other_details"  value="' + temp.other_details + '" /></form > ';


        // console.log("Merchant Type:;", MerchantBrandData.integration_type);
        if (SelectedPaymentMode.payModeId == "TB" || SelectedPaymentMode.payModeId == "PL") {
            //     // let str = '<div id="sp-payment-iframe" style="height:100%; width:100%"></div> <div ></div>';
            //     let str = '<iframe  id="sp-payment-iframe" style="height:100%; width:100%"></iframe>';
            //     document.getElementById('sp-payment-div').style.display = 'none';
            //     document.getElementById('sp-payment-porccessing').style.display = 'block';
            //     // document.getElementById('sp-form-submit').innerHTML = requestPayamentStr;
            //     document.getElementById('sp-form-submit').innerHTML = str;
            //     // document.getElementById('sp-payment-iframe').src = "data:text/html;charset=utf-8," + escape(requestPayamentStr);
            //     document.getElementById('sp-payment-iframe').contentWindow.document.write(requestPayamentStr)
            //     // setTimeout(() => {
            //         let iframe = document.getElementById('sp-payment-iframe');
            //         let innerDoc = (iframe.contentDocument)
            //             ? iframe.contentDocument
            //             : iframe.contentWindow.document;

            //     //     // let innerDoc = iframe.contentDocument;
            //         console.log("DDDDDDDDDDDDDD: ", innerDoc);
            //         innerDoc.getElementById('dummy').style.color = 'blue';
            //         innerDoc.getElementById('avanthgarde_payment_form').submit( function(){
            //             // document.getElementById('avanthgarde_payment_form').target = 'sp-payment-iframe'; 
            //         });
            //     //     innerDoc.getElementById("dummy").style.display = '';
            //     // }, 1000);

            //     // innerDoc.getElementById("avanthgarde_payment_form").submit(function () {
            //     //     var child = window.open(" ", "_blank", "height=200,width=200");
            //     //     console.log("ChildWindow::::", child);
            //     //     let timer = setInterval(function () {
            //     //         // console.log("InsideInterval:::::", child.closed);
            //     //         if (child.closed) {

            //     //             alert("'Secure Payment' window closed !");
            //     //         }
            //     //     }, 500);
            //     // });

            //     // let node = document.createElement("DIV");
            //     // node.innerHTML = requestPayamentStr;
            //     // document.getElementsByTagName("body")[0].appendChild(node);
            //     // document.getElementById('avanthgarde_payment_form').submit( function(){
            //     //     // document.getElementById('avanthgarde_payment_form').target = 'sp-payment-iframe'; 
            //     // });

            // InitTabby();
            if (TabbyAgRef == null) {
                SpPayByTabby(encryptedPostData);
            }
            else {
                InitTabby(TabbyAgRef);
            }

        }
        else {

            // document.getElementById('sp-payment-response-type').style.display = 'none';
            document.getElementById('sp-payment-div').style.display = 'none';
            document.getElementById('sp-payment-porccessing').style.display = 'block';
            document.getElementById('sp-form-submit').innerHTML = requestPayamentStr;
            // document.getElementById('sp-request-payment').style.minHeight = '350px';
            // let childWindow = null;
            // document.getElementById('avanthgarde_payment_form').submit(function () {
            //     // var child = window.open(" ", "_blank", "height=200,width=200");
            //     // console.log('Inside Form Submit-----------');
            //     // childWindow = window.open('', 'safexPayWindow');
            //     // console.log('Inside Form Submit:::', childWindow);
            //     // console.log("ChildWindow::::", child);
            //     // let timer = setInterval(function () {
            //     //     // console.log("InsideInterval:::::", child.closed);
            //     //     if (child.closed) {

            //     //         // alert("'Secure Payment' window closed !");
            //     //     }
            //     // }, 500);
            // });
            document.getElementById('avanthgarde_payment_form_btn').click()
            /* setTimeout(() => {
                 console.log("InsideSetTimeout:::::::::");
                 childWindow = window.open('', 'safexPayWindow');
                 console.log('Inside Form Submit:::', childWindow);
             }, 1000);
 */
            /*
            $('#avanthgarde_payment_form').onsubmit = function () {
                // I think why window.open() here would not be blocked by browser because the function is attached to the HTML DOM and the event is triggered by user (mouse click)
                console.log('Inside Form Submit-----------');
                childWindow = window.open('', 'safexPayWindow');
                console.log('Inside Form Submit:::', childWindow);
            };

            var checkWindowClose = setInterval(function () {
                console.log('Inside Interval::', childWindow);
                if (childWindow == null) {
                    console.log('ChildWindowNotFound::', childWindow);
                }
                else if (childWindow.closed != undefined) {
                    console.log('Window is closed');
                    clearInterval(checkWindowClose);
                } else {
                    // check the URL if childWindow has been redirected to somewhere
                    try {
                        console.log('Something went wrong');
                        // cross-origin exception will occur if the URL is in different domain
                        if (childWindow.document.URL == 'something') {
                            // ...
                            clearInterval(checkWindowClose);
                        }
                    } catch (err) {
                        // just ignore the error and do nothing
                    }
                }
            }, 1000);
            */
            /*  var childWindow;
  
              $('#avanthgarde_payment_form').onsubmit = function () {
                  // I think why window.open() here would not be blocked by browser because the function is attached to the HTML DOM and the event is triggered by user (mouse click)
                  console.log("Inside On Submit Call Back")
                  childWindow = window.open('', 'windowName');
                  console.log("Inside On Submit Call Back After Init new window")
              };
  
              var checkWindowClose = setInterval(function () {
                  console.log("Inside Set Interval:::", childWindow);
                  if (childWindow != undefined && childWindow.closed) {
                      alert('Window is closed');
                      clearInterval(checkWindowClose);
                  } else {
                      // check the URL if childWindow has been redirected to somewhere
                      try {
                          // cross-origin exception will occur if the URL is in different domain
                          if (childWindow.document.URL == 'something') {
                              // ...
                              clearInterval(checkWindowClose);
                          }
                      } catch (err) {
                          // just ignore the error and do nothing
                      }
                  }
              }, 1000);
  */

            window.addEventListener("storage", function (event) {
                let resObj = null;
                let data = localStorage.getItem("result");
                let d = Decrypt(data.replace('\'', ''));
                // console.log("Response::::", d);
                ResponseValue = d; // Set the response value to transfer at merchent side.
                if (d !== '') {

                    let res = d.split('|');
                    // console.log("Response Array:", res);
                    if (res[10] == 'Successful') {
                        // Show Success Screen
                        // console.log("Inside the Success Response");
                        RenderSuccessContent(res);

                        resObj = {
                            status: true,
                            data: d,
                            message: 'success'
                        }

                    }
                    if (res[10] == 'Failed') {
                        // Show Failed Screen
                        // console.log("Inside the Failed Response");
                        document.getElementById('sp-txn-failed-text').innerText = 'Transaction failed.';
                        RenderFailedContent(res);
                        resObj = {
                            status: false,
                            data: d,
                            message: 'failed'
                        }

                    }
                    // console.log("After Decrpt Response");
                }
                else {
                    //Something went wrong while getting the response.
                    console.log("Something went wrong while getting the response.");
                    resObj = {
                        status: false,
                        data: null,
                        message: 'Something went wrong while getting the response.'
                    }
                }
                //alert(data);
                ResponseCallback(resObj); // send success object in callback
            }, true);

            SpHandleTimeout()/// Throw error component on timeout.
        }




        let currentMode = SelectedPaymentMode.paymentMode;
        //console.log("CurrentMode::" + currentMode);
        // if (currentMode.toLowerCase() == 'card' || currentMode.toLowerCase() == 'debit card' || currentMode.toLowerCase() == 'credit card') {
        //     document.getElementById('sp-card-payment').style.display = 'none';
        //     //document.getElementById('sp-available-payment-modes').style.display = 'block';

        // }
        // if (currentMode.toLowerCase() == 'netbanking' || currentMode.toLowerCase() == 'net banking') {
        //     document.getElementById('sp-net-banking').style.display = 'none';
        //     // document.getElementById('sp-available-payment-modes').style.display = 'block';
        // }
        // if (currentMode.toLowerCase() == 'wallet') {
        //     document.getElementById('sp-wallets').style.display = 'none';
        //     // document.getElementById('sp-available-payment-modes').style.display = 'block';
        // }
        // if (currentMode.toLowerCase() == 'upi') {
        //     document.getElementById('sp-upis').style.display = 'none';
        //     // document.getElementById('sp-available-payment-modes').style.display = 'block';
        // }
        // document.getElementById('sp-footer').style.display = 'none';
        // document.getElementById('sp-footer-btn').disabled = true;
        // // }




    }

    SpPayByTabby = function (encryptedPostData) {
        let url = `${BaseUrl}/tabbyJSRequest`;
        // console.log("CardNO ::::::::::::::", url);


        let response = CallAPI('POST', url, encryptedPostData, function (response) {
            // console.log("CardTypeDetails::", response);
            try {
                if (response != '') {
                    let resp = JSON.parse(response);
                    if (resp.message == 'Success') {
                        let data = Decrypt(resp.ag_ref);
                        console.log("Ag_ref:::", data, Decrypt(resp.api_key) + '-----' + Decrypt(resp.merchant_code));
                        TabbyAccessData = { api_key: Decrypt(resp.api_key), merchant_code: Decrypt(resp.merchant_code) };
                        // console.log("TabbyAccessData:::", TabbyAccessData);
                        InitTabby(data);
                        TabbyAgRef = data;
                        // RenderTabbyPayementDetails()
                        // GoToPayLaterByTabby();
                    }
                }
            } catch (e) {
                console.log("Error:", e);
                // HandleEMIBreakDowns([])
            }
            //SavedCards = JSON.parse(cardData); // Decrypted scheme data 
        });
        // InitTabby();

        /*
        let selectedTabbyProduct = Tabby.PAYLATER;
        let payOption = document.getElementById('tabbyTermRadios1').value;
        if (payOption == 'term2') {
            selectedTabbyProduct = Tabby.INSTALLMENTS;
        }
        console.log("PayOptions:::", payOption);
 
        if (RelaunchTabby) {
            console.log("In RelaunchTabby");
            Tabby.create();
            document.getElementById('tabby-checkout').style.display = '';
        } else {
            console.log("In LaunchTabby");
            Tabby.launch({ product: selectedTabbyProduct });
            console.log("Tabby,", Tabby);
            document.getElementById('tabby-checkout').style.display = '';
        }
        document.getElementById('sp-payment-div').style.display = 'none';
        document.getElementById('sp-payment-porccessing').style.display = 'block'; 
        */
    }

    RenderSuccessContent = function (response) {
        // console.log("InSuccessContent::", response);
        // let str = '<div class="sp-contact-payment-modes"><div class="sp-response-content"><div class="sp-transaction-details"><h6>Payment of Amount</h6><p class="sp-total-amount-value">' + CustomerOrder.currencySymbol + response[3] + '</p></div><div class="sp-response-icon"><!--<span class="far fa-check-circle"></span>--><img src="data:image/svg+xml,%0A%3Csvg xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' xmlns:svgjs=\'http://svgjs.com/svgjs\' version=\'1.1\' width=\'512\' height=\'512\' x=\'0\' y=\'0\' viewBox=\'0 0 330 330\' style=\'enable-background:new 0 0 512 512\' xml:space=\'preserve\' class=\'\'%3E%3Cg%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3Cpath d=\'M165,0C74.019,0,0,74.019,0,165s74.019,165,165,165s165-74.019,165-165S255.981,0,165,0z M165,300 c-74.44,0-135-60.561-135-135S90.56,30,165,30s135,60.561,135,135S239.439,300,165,300z\' fill=\'%23009cdc\' data-original=\'%23000000\' style=\'\' class=\'\'/%3E%3Cpath d=\'M226.872,106.664l-84.854,84.853l-38.89-38.891c-5.857-5.857-15.355-5.858-21.213-0.001 c-5.858,5.858-5.858,15.355,0,21.213l49.496,49.498c2.813,2.813,6.628,4.394,10.606,4.394c0.001,0,0,0,0.001,0 c3.978,0,7.793-1.581,10.606-4.393l95.461-95.459c5.858-5.858,5.858-15.355,0-21.213 C242.227,100.807,232.73,100.806,226.872,106.664z\' fill=\'%23009cdc\' data-original=\'%23000000\' style=\'\' class=\'\'/%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3Cg xmlns=\'http://www.w3.org/2000/svg\'%3E%3C/g%3E%3C/g%3E%3C/svg%3E" /></div><div class="sp-response-message"><h6>Transaction Successful</h6></div></div></div>';
        // document.getElementById('sp-success-payment').innerHTML = str;
        // document.getElementById('sp-request-payment').style.display = 'none';
        // document.getElementById('sp-total-amount').style.display = 'none';
        // document.getElementById('sp-success-payment').style.display = 'block';
        if (SessionTimeout != null) {
            clearTimeout(SessionTimeout);
        }

        document.getElementById('sp-payment-porccessing').style.display = 'none';
        document.getElementById('sp-success-payment').style.display = 'block';
        document.getElementById('sp-success-payment-amount').innerText = CustomerOrder.currencyCode + ' ' + CustomerOrder.amount;
        document.getElementById('sp-success-payment-transaction-id').innerText = CustomerOrder.orderNo;
    }

    RenderFailedContent = function (response) {
        // console.log("InSuccessContent::", response);
        // let str = '<div class="sp-contact-payment-modes"><div class="sp-response-content"><div class="sp-transaction-details failed"><h6>Payment of Amount</h6><p class="sp-total-amount-value">' + CustomerOrder.currencySymbol + response[3] + '</p></div><div class="sp-response-icon failed"><span class="far fa-times-circle"></span></div><div class="sp-response-message failed"><h6>Transaction Failed</h6><p>Order ID : ' + CustomerOrder.orderNo + '</p></div></div></div>';

        // document.getElementById('sp-failed-payment').innerHTML = str;
        // document.getElementById('sp-request-payment').style.display = 'none';
        // document.getElementById('sp-total-amount').style.display = 'none';
        // document.getElementById('sp-failed-payment').style.display = 'block';
        if (SelectedPaymentMode.payModeId == 'TB' || SelectedPaymentMode.payModeId == 'PL') {
            document.querySelector('#tabby-checkout').style.display = 'none';
            document.getElementById('sp-footer-btn-tabby').innerText = 'Pay now';
            document.getElementById('sp-footer-btn-tabby').disabled = false;
            document.getElementById('sp-payment-div').style.display = 'none';
            document.getElementById('sp-payment-porccessing').style.display = 'none';
        }
        else {
            document.getElementById('sp-payment-porccessing').style.display = 'none';
        }

        if (SessionTimeout != null) {
            clearTimeout(SessionTimeout);
        }

        document.getElementById('sp-failed-payment').style.display = 'block';
        document.getElementById('sp-failed-payment-amount').innerText = CustomerOrder.currencyCode + ' ' + CustomerOrder.amount;
        document.getElementById('sp-failed-payment-transaction-id').innerText = CustomerOrder.orderNo;
    }

    TryAgainPaynow = function () {
        // console.log("SelectedMode::", SelectedMode);
        document.getElementById('sp-failed-payment').style.display = 'none';
        // if (SelectedMode == 'card' || SelectedMode == 'wallet' || SelectedMode == 'epp' || SelectedMode == 'applepay') {
        //     ShowResponseTypeScreen();
        // }
        // else {
        document.getElementById('sp-payment-div').style.display = 'flex';
        // document.getElementById('sp-payment-porccessing').style.display = 'block';
        // SPPayNow();
        HideAllModes();
        SelectPaymentMode(AvailablePaymentOptions[0].payModeId);
        // }
    }

    /*** Open Response Content *** */
    OpenResponseContent = function (response) {

        let currentMode = SelectedPaymentMode.paymentMode;
        // console.log("CurrentMode::" + currentMode);
        if (currentMode.toLowerCase() == 'card' || currentMode.toLowerCase() == 'debit card' || currentMode.toLowerCase() == 'credit card') {
            document.getElementById('sp-card-payment').style.display = 'none';
            //document.getElementById('sp-available-payment-modes').style.display = 'block';

        }
        if (currentMode.toLowerCase() == 'netbanking' || currentMode.toLowerCase() == 'net banking') {
            document.getElementById('sp-net-banking').style.display = 'none';
            // document.getElementById('sp-available-payment-modes').style.display = 'block';
        }
        if (currentMode.toLowerCase() == 'wallet') {
            document.getElementById('sp-wallets').style.display = 'none';
            // document.getElementById('sp-available-payment-modes').style.display = 'block';
        }
        if (currentMode.toLowerCase() == 'upi') {
            document.getElementById('sp-upis').style.display = 'none';
            // document.getElementById('sp-available-payment-modes').style.display = 'block';
        }
        document.getElementById('sp-footer').style.display = 'none';
        document.getElementById('sp-footer-btn').disabled = true;




        document.getElementById('sp-request-payment').style.display = 'block';
        var iframe = document.getElementById('sp-resp-iframe');
        //iframe.src = 'https://test.avantgardepayments.com/agcore/payment';
        iframe.contentWindow.document.write(response);

    }

    GetEmiData = function () {
        let cardNo = document.getElementById('sp-txt-card-number').value;
        // console.log("InsideCardHandleInput::", cardNo.trim(), cardNo.trim().length);
        if (SelectedPaymentMode.payModeId == 'CC') {
            GetEMIDetailsByCard(cardNo.trim());
        }

    }

    /*********** Handle EMI BreackDown****************/
    HandleEMIBreakDowns = function (emiData) {

        let emiStr = '';
        if (emiData.length > 0) {
            let breakdownStr = '';
            EmiPlans = []

            breakdownStr += '<div id="collapseTwo" class="_sp-collapse _sp-show" aria-labelledby="headingTwo" data-parent="#accordion"> <div class="sp-card-body pr-4"> <div class="_sp-row form-group"> <div class="_sp-col-12"> <ul class="_sp-radio-emi">';
            for (let i = 0; i < emiData.length; i++) {
                let items = emiData[i].split('|');
                let breakdwonInfo = {
                    principalAmt: items[0],
                    months: items[1],
                    monthlyEmi: items[2],
                    totalInt: items[3],
                    totalAmt: items[4],
                    rateOfInt: items[5],
                    bankId: items[6],
                    processingFee: items[7],
                    percentageProcessingFee: items[8],
                }

                if (breakdwonInfo.percentageProcessingFee != 'null') {
                    // console.log("Apply Calculation");
                    let processingFee = parseFloat(breakdwonInfo.percentageProcessingFee * (breakdwonInfo.principalAmt / 100))
                    let total = (parseFloat(breakdwonInfo.principalAmt) + parseFloat(breakdwonInfo.totalInt) + parseFloat(processingFee)).toFixed(1);

                    let proStr = 'processingFee = parseFloat(' + breakdwonInfo.percentageProcessingFee + ' * (' + breakdwonInfo.principalAmt + ' / 100)) , Result:' + processingFee;
                    let totStr = 'total = (parseFloat(' + breakdwonInfo.principalAmt + ') + parseFloat(' + breakdwonInfo.rateOfInt + ') + parseFloat(' + processingFee + ')).toFixed(1), Result:' + total;
                    // console.log("ProcessingFee:", processingFee, ", Total::", total);
                    // console.log(proStr);
                    // console.log(totStr);

                    breakdwonInfo.processingFee = processingFee;
                    breakdwonInfo.totalAmt = total;
                }

                let isChecked = '';
                if (i == 0) {
                    // isChecked = 'checked';
                    // SelectedEmiPlan = breakdwonInfo;

                }
                EmiPlans.push(breakdwonInfo);
                //breakdownStr += '<div class="_sp-row _sp-mb-1"><div class="_sp-col-12"><div class="_sp-form-check"><input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-' + i + '" value="' + breakdwonInfo.months + '" onchange="ChangeEmiPlan(' + i + ')" ' + isChecked + '/> <label class="_sp-form-check-label" for="emi-option-' + i + '"> <span class="sp-epp-emi-options"><span class="_sp-row"> <span class="_sp-col-8 sp-epp-emi-months _sp-pr-0">' + CustomerOrder.currencyCode + ' ' + RemoveZeros(breakdwonInfo.monthlyEmi) + ' for ' + RemoveZeros(breakdwonInfo.months) + ' months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ ' + breakdwonInfo.rateOfInt + '% p.a.</span> </span> </span></label> </div> </div> </div>';

                //breakdownStr += '<div class="_sp-row _sp-mb-1"><div class="_sp-col-12">';//old

                // breakdownStr += '<div class="_sp-form-check sp-emi-radio" id="sp-emi-radio-' + i + '"><input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-' + i + '" value="' + breakdwonInfo.months + '" onchange="ChangeEmiPlan(' + i + ')" ' + isChecked + '/> <label class="_sp-form-check-label" for="emi-option-' + i + '"> <span class="sp-epp-emi-options" id="sp-epp-emi-options-' + i + '"><span class="_sp-row"> <span class="_sp-col-8 sp-epp-emi-months _sp-pr-0">' + CustomerOrder.currencyCode + ' ' + RemoveZeros(breakdwonInfo.monthlyEmi) + ' for ' + RemoveZeros(breakdwonInfo.months) + ' months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ ' + breakdwonInfo.rateOfInt + '% p.a.</span> </span> </span></label> </div> ';

                // breakdownStr += '<div id="sp-emi-detail-div-' + i + '" class="_sp-emi-detail-div" style="display: none;"> <div class="sp-accordion" id="accordionExample"> <div class="sp-card"> <div class="sp-card-header" id="heading1"> <h6 class="_sp-mb-0"> <button type="button" class="_sp-btn _sp-btn-link btn-det" data-toggle="collapse" data-target="#collapse1" aria-expanded="true"> View EMI Breakup Details</button> </h6> <span class="sp-emi-breckdown-icon" id="sp-emi-breckdown-icon"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ0OCA0NDgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPjxwYXRoIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZD0ibTQwOCAxODRoLTEzNmMtNC40MTc5NjkgMC04LTMuNTgyMDMxLTgtOHYtMTM2YzAtMjIuMDg5ODQ0LTE3LjkxMDE1Ni00MC00MC00MHMtNDAgMTcuOTEwMTU2LTQwIDQwdjEzNmMwIDQuNDE3OTY5LTMuNTgyMDMxIDgtOCA4aC0xMzZjLTIyLjA4OTg0NCAwLTQwIDE3LjkxMDE1Ni00MCA0MHMxNy45MTAxNTYgNDAgNDAgNDBoMTM2YzQuNDE3OTY5IDAgOCAzLjU4MjAzMSA4IDh2MTM2YzAgMjIuMDg5ODQ0IDE3LjkxMDE1NiA0MCA0MCA0MHM0MC0xNy45MTAxNTYgNDAtNDB2LTEzNmMwLTQuNDE3OTY5IDMuNTgyMDMxLTggOC04aDEzNmMyMi4wODk4NDQgMCA0MC0xNy45MTAxNTYgNDAtNDBzLTE3LjkxMDE1Ni00MC00MC00MHptMCAwIiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+PC9nPjwvc3ZnPg==" /></span> </div> <hr class="sp-hr"> <div id="collapse1" class="_sp-collapse _sp-show" aria-labelledby="heading1" data-parent="#accordionExample"> <div class="sp-card-body"> <ul class="_sp-radio-emi"> <li> <div class="_sp-em-deails"> <p>Order value </p> <span>' + RemoveZeros(breakdwonInfo?.principalAmt) + '</span> <div class="_sp-clearfix"> </div> </div> </li> <li> <div class="_sp-em-deails"> <p>Interest ( charged by bank) </p> <span>' + RemoveZeros(breakdwonInfo?.totalInt) + '</span> <div class="_sp-clearfix"> </div> </div> </li> <li> <div class="_sp-em-deails"> <p>Processing fees </p> <span>' + RemoveZeros(breakdwonInfo?.processingFee) + '</span> <div class="_sp-clearfix"> </div> </div> </li> </ul> <div class="sp-card-footer"> <div class="_sp-em-deails"> <p>Total EMI for ' + RemoveZeros(breakdwonInfo?.months) + ' months </p> <span><b>' + CustomerOrder.currencyCode + ' ' + RemoveZeros(breakdwonInfo?.totalAmt) + '</b></span> <div class="_sp-clearfix"> </div> </div> </div> </div> </div> </div> </div> </div> <div class="_sp-clearfix"></div>'; // Selected Emi Details

                breakdownStr += '<li> <div class="_sp-radio-div"> <div class="_sp-radio"> <input type="radio" class="_sp-tradio _sp-radio-btn"name="emi-option" id="emi-option-' + i + '" value="' + breakdwonInfo.months + '" onchange="ChangeEmiPlan(' + i + ')" ' + isChecked + '/><label class="_sp-tlabel" for="emi-option-' + i + '"></label> </div> <div class="_sp-emi-ct" id="emi-ct-' + i + '"> <div class="_sp-label-emi"> ' + CustomerOrder.currencyCode + ' ' + RemoveZeros(breakdwonInfo.monthlyEmi) + ' for ' + RemoveZeros(breakdwonInfo.months) + ' months</div> <span class="sp-emi-rate">@ ' + RemoveZeros(breakdwonInfo.rateOfInt) + '% p.a.</span> </div> </div> <div id="sp-emi-detail-div-' + i + '" class="_sp-emi-detail-div" style="display: none;"> <div class="sp-accordion" id="accordionExample"> <div class="sp-card"> <div class="sp-card-header" id="heading1"> <h6 class="_sp-mb-0"> <button type="button" class="_sp-btn _sp-btn-link btn-det" data-toggle="collapse" data-target="#sp-collapse' + i + '" aria-expanded="true" onclick="ShowEmiPlanDetails(' + i + ')"> View EMI Breakup Details</button> </h6> <span class="sp-emi-breckdown-icon" id="sp-emi-breckdown-icon' + i + '"><img src="' + SvGIconSet.plusIcon + '" /></span> </div> <hr class="sp-hr"> <div id="sp-collapse-' + i + '" class="sp-collapse" aria-labelledby="heading1" data-parent="#accordionExample"> <div class="sp-card-body"> <ul class="_sp-radio-emi"> <li> <div class="_sp-em-deails"> <p>Order value </p> <span>' + RemoveZeros(breakdwonInfo?.principalAmt) + '</span> <div class="_sp-clearfix"> </div> </div> </li> <li> <div class="_sp-em-deails"> <p>Interest ( charged by bank) </p> <span>' + RemoveZeros(breakdwonInfo?.totalInt) + '</span> <div class="_sp-clearfix"> </div> </div> </li> <li> <div class="_sp-em-deails"> <p>Processing fees </p> <span>' + RemoveZeros(breakdwonInfo?.processingFee) + '</span> <div class="_sp-clearfix"> </div> </div> </li> </ul> <div class="_sp-sp-card-footer"> <div class="_sp-em-deails"> <p>Total EMI for ' + RemoveZeros(breakdwonInfo?.months) + ' months </p> <span><b>' + CustomerOrder.currencyCode + ' ' + RemoveZeros(breakdwonInfo?.totalAmt) + '</b></span> <div class="_sp-clearfix"></div> </div> </div> </div> </div> </div> </div> </div> <div class="_sp-clearfix"></div> </li>';

                // breakdownStr += '</div> </div>'; //old
            }
            breakdownStr += ' </ul> </div> </div> </div></div>';
            let enableEmi = '';
            if (IsEMIEnabled) {
                // enableEmi = 'checked';
            }

            // emiStr += '<div class="_sp-row"><div class="_sp-col-md-11"><div class="_sp-acc-kontainer">'; //Accordion Start removed


            emiStr += '<div class="_sp-row"><div class="_sp-col-md-12"><input type="radio" name="acc" id="acc2" class="_sp-acc-input"><label for="acc2" class="_sp-acc-label"><p class="sp-content-label sp-acc-label"><span class="sp-epp-acc-title"><!--<span>EPP</span>--><span class="sp-epp-option-available" id="sp-epp-option-available" onchange="EnableEmiOptions()">Choose instalment plan</span><span class="_sp-px-1"><label class="_sp-switch"><input type="checkbox" id="sp-enable-emi-option" ' + enableEmi + ' onchange="EnableEmiOptions()"><span class="_sp-slider _sp-round"></span></label></span></span><span class="sp-acc-icon"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ1MS44NDcgNDUxLjg0NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBkPSJNMjI1LjkyMywzNTQuNzA2Yy04LjA5OCwwLTE2LjE5NS0zLjA5Mi0yMi4zNjktOS4yNjNMOS4yNywxNTEuMTU3Yy0xMi4zNTktMTIuMzU5LTEyLjM1OS0zMi4zOTcsMC00NC43NTEgICBjMTIuMzU0LTEyLjM1NCwzMi4zODgtMTIuMzU0LDQ0Ljc0OCwwbDE3MS45MDUsMTcxLjkxNWwxNzEuOTA2LTE3MS45MDljMTIuMzU5LTEyLjM1NCwzMi4zOTEtMTIuMzU0LDQ0Ljc0NCwwICAgYzEyLjM2NSwxMi4zNTQsMTIuMzY1LDMyLjM5MiwwLDQ0Ljc1MUwyNDguMjkyLDM0NS40NDlDMjQyLjExNSwzNTEuNjIxLDIzNC4wMTgsMzU0LjcwNiwyMjUuOTIzLDM1NC43MDZ6IiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /></span></p></label>'; ///This is accordion Heading.

            //emiStr += '<input type="checkbox" name="acc" id="acc2" class="_sp-acc-input" disabled/><label for="acc2" class="_sp-acc-label"><p class="sp-content-label sp-acc-label"><span class="sp-epp-acc-title active"><span>EPP </span> <span class="sp-epp-option-available" id="sp-epp-option-available">Option available on this card</span><span class="_sp-px-1"><label class="_sp-switch"><input type="checkbox" id="sp-enable-emi-option" ' + enableEmi + ' onchange="EnableEmiOptions()"><span class="_sp-slider _sp-round"></span></label></span></span><span class="sp-acc-icon"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ1MS44NDcgNDUxLjg0NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBkPSJNMjI1LjkyMywzNTQuNzA2Yy04LjA5OCwwLTE2LjE5NS0zLjA5Mi0yMi4zNjktOS4yNjNMOS4yNywxNTEuMTU3Yy0xMi4zNTktMTIuMzU5LTEyLjM1OS0zMi4zOTcsMC00NC43NTEgICBjMTIuMzU0LTEyLjM1NCwzMi4zODgtMTIuMzU0LDQ0Ljc0OCwwbDE3MS45MDUsMTcxLjkxNWwxNzEuOTA2LTE3MS45MDljMTIuMzU5LTEyLjM1NCwzMi4zOTEtMTIuMzU0LDQ0Ljc0NCwwICAgYzEyLjM2NSwxMi4zNTQsMTIuMzY1LDMyLjM5MiwwLDQ0Ljc1MUwyNDguMjkyLDM0NS40NDlDMjQyLjExNSwzNTEuNjIxLDIzNC4wMTgsMzU0LjcwNiwyMjUuOTIzLDM1NC43MDZ6IiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /></span></p></label>'; ///This is accordion Heading.

            emiStr += '<div class="_sp-acc-body"><div class="sp-epp-body">'; //Accordion Body start
            emiStr += '<p class="_sp-epp-content-text">We does not levy any charges for availing EMI. Charges, If any are levied by the bank. Please check with your bank for charges related to interest, processing fees, refund or pre-closure. </p>';

            // emiStr += '<div class="_sp-row">'; //Emi Breakdown details Row start

            // emiStr += '<div class="_sp-col-md-12">'; //Emi Rates details start

            emiStr += breakdownStr // EMI options

            // emiStr += '</div>'; //Emi Rates details start

            // emiStr += '<div class="_sp-col-md-6 sp-emi-calculation-content">'; //Emi Payment Details start
            // emiStr += '<div class="_sp-acc-kontainer"> <div class="sp-emi-breckdown"> <input type="radio" name="emi-acc" id="emi-acc1" class="_sp-acc-input" checked> <label for="emi-acc1" class="_sp-acc-label sp-emi-breckdown-label px-1"> <p class="sp-content-label sp-acc-label sp-emi-breckdown-label-text"> <span>View EMI Breakup Details</span> <span class="sp-emi-breckdown-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ0OCA0NDgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPjxwYXRoIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZD0ibTQwOCAxODRoLTEzNmMtNC40MTc5NjkgMC04LTMuNTgyMDMxLTgtOHYtMTM2YzAtMjIuMDg5ODQ0LTE3LjkxMDE1Ni00MC00MC00MHMtNDAgMTcuOTEwMTU2LTQwIDQwdjEzNmMwIDQuNDE3OTY5LTMuNTgyMDMxIDgtOCA4aC0xMzZjLTIyLjA4OTg0NCAwLTQwIDE3LjkxMDE1Ni00MCA0MHMxNy45MTAxNTYgNDAgNDAgNDBoMTM2YzQuNDE3OTY5IDAgOCAzLjU4MjAzMSA4IDh2MTM2YzAgMjIuMDg5ODQ0IDE3LjkxMDE1NiA0MCA0MCA0MHM0MC0xNy45MTAxNTYgNDAtNDB2LTEzNmMwLTQuNDE3OTY5IDMuNTgyMDMxLTggOC04aDEzNmMyMi4wODk4NDQgMCA0MC0xNy45MTAxNTYgNDAtNDBzLTE3LjkxMDE1Ni00MC00MC00MHptMCAwIiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+PC9nPjwvc3ZnPg==" /> </span> </p> </label> <div class="_sp-acc-body sp-emi-breckdown-body" id="sp-selected-emi-plan-details"> <div class="_sp-row"> <div class="_sp-col-8"> <span>Order value</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan?.principalAmt) + '</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Interest ( charged by bank)</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan?.totalInt) + '</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Processing fees</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan?.processingFee) + '</span> </div> </div> <hr class="sp-hr"/> <div class="_sp-row"> <div class="_sp-col-8 _sp-pr-2"> <span><b>Total EMI for ' + RemoveZeros(SelectedEmiPlan?.months) + ' months</b></span> </div> <div class="_sp-col-4 _sp-text-right _sp-pl-1"> <span><b>' + CustomerOrder.currencyCode + ' ' + RemoveZeros(SelectedEmiPlan?.totalAmt) + '</b></span> </div> </div> </div> </div> </div>'//Emi Calculations
            // emiStr += '</div>'; //Emi Payment Details End

            // emiStr += '</div>';//Emi Breakdown details Row End

            emiStr += '</div></div>'; //Accordion Body End

            emiStr += '</div></div>';

            // emiStr+= '</div>'; //Accordion End

            document.getElementById('sp-emi-hr').style.display = 'block'; // Show line
            document.getElementById('sp-accordian-card-right-icon').style.display = 'block'; // Show line

        }
        else {
            let elem = document.getElementById('sp-emi-hr');
            if (typeof (elem) != 'undefined' && elem != null) {
                // alert('Element exists!');
                document.getElementById('sp-emi-hr').style.display = 'none'; // Hide line
            }

        }
        document.getElementById('sp-emi-break-details').innerHTML = emiStr;
        if (emiData.length > 0) {
            // EnableEmiOptions() // To enable the EMI option bydefault
        }

    }

    HandleEMIBreakDownsCopied = function (emiData) {

        let emiStr = '';
        if (emiData.length > 0) {
            let breakdownStr = '';
            EmiPlans = []
            for (let i = 0; i < emiData.length; i++) {
                let items = emiData[i].split('|');
                let breakdwonInfo = {
                    principalAmt: items[0],
                    months: items[1],
                    monthlyEmi: items[2],
                    totalInt: items[3],
                    totalAmt: items[4],
                    rateOfInt: items[5],
                    bankId: items[6],
                    processingFee: items[7],
                    percentageProcessingFee: items[8],
                }
                let isChecked = '';
                if (i == 0) {
                    isChecked = 'checked';
                    SelectedEmiPlan = breakdwonInfo;

                }
                EmiPlans.push(breakdwonInfo);
                breakdownStr += '<div class="_sp-row _sp-mb-1"><div class="_sp-col-12"><div class="_sp-form-check"><input class="_sp-form-check-input sp-epp-emi-radio" type="radio" name="emi-option" id="emi-option-' + i + '" value="' + breakdwonInfo.months + '" onchange="ChangeEmiPlan(' + i + ')" ' + isChecked + '/> <label class="_sp-form-check-label" for="emi-option-' + i + '""> <span class="sp-epp-emi-options _sp-row"> <span class="_sp-col-8 sp-epp-emi-months _sp-pr-0">' + CustomerOrder.currencyCode + ' ' + RemoveZeros(breakdwonInfo.monthlyEmi) + ' for ' + RemoveZeros(breakdwonInfo.months) + ' months</span> <span class="_sp-col-4 sp-epp-emi-rate _sp-px-1">@ ' + breakdwonInfo.rateOfInt + '% p.a.</span> </span> </label> </div> </div> </div>';

            }

            let enableEmi = '';
            if (IsEMIEnabled) {
                enableEmi = 'checked';
            }

            emiStr += '<div class="_sp-row"><div class="_sp-col-md-12"><div class="_sp-acc-kontainer">'; //Accordion Start

            emiStr += '<input type="checkbox" name="acc" id="acc2" class="_sp-acc-input" disabled/><label for="acc2" class="_sp-acc-label"><p class="sp-content-label sp-acc-label"><span class="sp-epp-acc-title"><span>EPP </span> <span class="sp-epp-option-available" id="sp-epp-option-available">Option available on this card</span><span class="_sp-px-1"><label class="_sp-switch"><input type="checkbox" id="sp-enable-emi-option" ' + enableEmi + ' onchange="EnableEmiOptions()"><span class="_sp-slider _sp-round"></span></label></span></span><span class="sp-acc-icon"><img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ1MS44NDcgNDUxLjg0NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNTEyIDUxMiIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgY2xhc3M9IiI+PGc+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+Cgk8cGF0aCBkPSJNMjI1LjkyMywzNTQuNzA2Yy04LjA5OCwwLTE2LjE5NS0zLjA5Mi0yMi4zNjktOS4yNjNMOS4yNywxNTEuMTU3Yy0xMi4zNTktMTIuMzU5LTEyLjM1OS0zMi4zOTcsMC00NC43NTEgICBjMTIuMzU0LTEyLjM1NCwzMi4zODgtMTIuMzU0LDQ0Ljc0OCwwbDE3MS45MDUsMTcxLjkxNWwxNzEuOTA2LTE3MS45MDljMTIuMzU5LTEyLjM1NCwzMi4zOTEtMTIuMzU0LDQ0Ljc0NCwwICAgYzEyLjM2NSwxMi4zNTQsMTIuMzY1LDMyLjM5MiwwLDQ0Ljc1MUwyNDguMjkyLDM0NS40NDlDMjQyLjExNSwzNTEuNjIxLDIzNC4wMTgsMzU0LjcwNiwyMjUuOTIzLDM1NC43MDZ6IiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPGcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPC9nPgo8ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8L2c+CjxnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjwvZz4KPC9nPjwvc3ZnPg==" /></span></p></label>'; ///This is accordion Heading.

            emiStr += '<div class="_sp-acc-body"><div class="sp-epp-body">'; //Accordion Body start
            emiStr += '<p class="_sp-epp-content-text">We does not levy any charges for availing EMI. Charges, If any are levied by the bank. Please check with your bank for charges related to interest, processing fees, refund or pre-closure. </p>';

            emiStr += '<div class="_sp-row">'; //Emi Breakdown details Row start

            emiStr += '<div class="_sp-col-6">'; //Emi Rates details start

            emiStr += breakdownStr // EMI options

            emiStr += '</div>'; //Emi Rates details start

            emiStr += '<div class="_sp-col-6">'; //Emi Payment Details start
            emiStr += '<div class="_sp-acc-kontainer"> <div class="sp-emi-breckdown"> <input type="radio" name="emi-acc" id="emi-acc1" class="_sp-acc-input" checked> <label for="emi-acc1" class="_sp-acc-label sp-emi-breckdown-label _sp-px-1"> <p class="sp-content-label sp-acc-label sp-emi-breckdown-label-text"> <span>View EMI Breakup Details</span> <span class="sp-emi-breckdown-icon"> <img src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMSIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHhtbG5zOnN2Z2pzPSJodHRwOi8vc3ZnanMuY29tL3N2Z2pzIiB3aWR0aD0iNTEyIiBoZWlnaHQ9IjUxMiIgeD0iMCIgeT0iMCIgdmlld0JveD0iMCAwIDQ0OCA0NDgiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTIiIHhtbDpzcGFjZT0icHJlc2VydmUiIGNsYXNzPSIiPjxnPjxwYXRoIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgZD0ibTQwOCAxODRoLTEzNmMtNC40MTc5NjkgMC04LTMuNTgyMDMxLTgtOHYtMTM2YzAtMjIuMDg5ODQ0LTE3LjkxMDE1Ni00MC00MC00MHMtNDAgMTcuOTEwMTU2LTQwIDQwdjEzNmMwIDQuNDE3OTY5LTMuNTgyMDMxIDgtOCA4aC0xMzZjLTIyLjA4OTg0NCAwLTQwIDE3LjkxMDE1Ni00MCA0MHMxNy45MTAxNTYgNDAgNDAgNDBoMTM2YzQuNDE3OTY5IDAgOCAzLjU4MjAzMSA4IDh2MTM2YzAgMjIuMDg5ODQ0IDE3LjkxMDE1NiA0MCA0MCA0MHM0MC0xNy45MTAxNTYgNDAtNDB2LTEzNmMwLTQuNDE3OTY5IDMuNTgyMDMxLTggOC04aDEzNmMyMi4wODk4NDQgMCA0MC0xNy45MTAxNTYgNDAtNDBzLTE3LjkxMDE1Ni00MC00MC00MHptMCAwIiBmaWxsPSIjNjY2NjY2IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBzdHlsZT0iIiBjbGFzcz0iIj48L3BhdGg+PC9nPjwvc3ZnPg==" /> </span> </p> </label> <div class="_sp-acc-body sp-emi-breckdown-body" id="sp-selected-emi-plan-details"> <div class="_sp-row"> <div class="_sp-col-8"> <span>Order value</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan.principalAmt) + '</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Interest ( charged by bank)</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan.totalInt) + '</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Processing fees</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan.processingFee) + '</span> </div> </div> <hr class="sp-hr"/> <div class="_sp-row"> <div class="_sp-col-8 _sp-pr-2"> <span><b>Total EMI for ' + RemoveZeros(SelectedEmiPlan.months) + ' months</b></span> </div> <div class="_sp-col-4 _sp-text-right _sp-pl-1"> <span><b>' + CustomerOrder.currencyCode + ' ' + RemoveZeros(SelectedEmiPlan.totalAmt) + '</b></span> </div> </div> </div> </div> </div>'//Emi Calculations
            emiStr += '</div>'; //Emi Payment Details End

            emiStr += '</div>';//Emi Breakdown details Row End

            emiStr += '</div></div>'; //Accordion Body End

            emiStr += '</div></div></div>'; //Accordion End

            document.getElementById('sp-emi-hr').style.display = 'block'; // Show line

        }
        else {
            let elem = document.getElementById('sp-emi-hr');
            if (typeof (elem) != 'undefined' && elem != null) {
                // alert('Element exists!');
                document.getElementById('sp-emi-hr').style.display = 'none'; // Hide line
            }

        }
        document.getElementById('sp-emi-break-details').innerHTML = emiStr;
        // document.getElementById('sp-emi-break-details').style.display = 'none';
        // if (emiData.length > 0) {
        //     EnableEmiOptions() // To enable the EMI option bydefault
        // }

    }

    /*********** Change Emi Plan************/
    ChangeEmiPlan = function (emiIndex) {
        SelectedEmiPlan = EmiPlans[emiIndex];

        // str = '<div class="_sp-row"> <div class="_sp-col-8"> <span>Order value</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan.principalAmt) + '</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Interest ( charged by bank)</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan.totalInt) + '</span> </div> </div> <div class="_sp-row"> <div class="_sp-col-8"> <span>Processing fees</span> </div> <div class="_sp-col-4 _sp-text-right"> <span>' + RemoveZeros(SelectedEmiPlan.processingFee) + '</span> </div> </div> <hr class="sp-hr" /> <div class="_sp-row"> <div class="_sp-col-8 _sp-pr-2"> <span><b>Total EMI for ' + RemoveZeros(SelectedEmiPlan.months) + ' months</b></span> </div> <div class="_sp-col-4 _sp-text-right _sp-pl-1"> <span><b>' + CustomerOrder.currencyCode + ' ' + RemoveZeros(SelectedEmiPlan.totalAmt) + '</b></span> </div> </div>';

        // document.getElementById('sp-selected-emi-plan-details').innerHTML = str;

        for (let i = 0; i < EmiPlans.length; i++) {
            if (emiIndex == i) {
                document.getElementById('sp-emi-detail-div-' + i).style.display = 'block';
                // document.getElementById('sp-epp-emi-options-' + i).classList.add('active');
                document.getElementById('emi-ct-' + i).classList.add('active');

                IsEMIEnabled = true;
                document.getElementById('sp-enable-emi-option').checked = IsEMIEnabled;
                // document.getElementById('acc2').checked = IsEMIEnabled;
                document.getElementById('sp-amount-heading').innerText = 'Approx EMI per month';
                document.getElementById('sp-amount-text').innerText = CustomerOrder.currencyCode + ' ' + SelectedEmiPlan.monthlyEmi;

                // console.log("Selected Emi Option found.");

            }
            else {
                document.getElementById('sp-emi-detail-div-' + i).style.display = 'none';
                if (document.getElementById('emi-ct-' + i).classList.contains("active")) {
                    // console.log("Active Contains");
                    document.getElementById('emi-ct-' + i).classList.remove("active");

                }
                // if (document.getElementById('sp-epp-emi-options-' + i).classList.contains("active")) {
                //     // console.log("Active Contains");
                //     document.getElementById('sp-epp-emi-options-' + i).classList.remove("active");

                // }
            }
        }
    }

    RemoveZeros = function (num) {
        // console.log("Nummber Type::",typeof num);
        if (num) {

            if (typeof num == 'number') {
                return num.toString().replace('.00', '').replace('.0', '');
            }
            else {
                return num.replace('.00', '').replace('.0', '');
            }
        }
        else {
            return '';
        }
    }

    DisableEMIOption = function () {

        if (IsEMIEnabled) {
            IsEMIEnabled = false;
            document.getElementById('sp-enable-emi-option').checked = false;
        }
    }

    /*********** Enable EMI Option ************/
    EnableEmiOptions = function () {
        IsEMIEnabled = !IsEMIEnabled;
        // document.getElementById('sp-enable-emi-option').checked = IsEMIEnabled;
        document.getElementById('acc2').checked = IsEMIEnabled;
        // console.log("CurrentStateEMIOption::", document.getElementById('sp-enable-emi-option').checked);
        if (!IsEMIEnabled) {
            document.getElementById('sp-amount-heading').innerText = 'Amount payable';
            document.getElementById('sp-amount-text').innerText = CustomerOrder.currencyCode + ' ' + CustomerOrder.amount;
        }
        else {
            if (SelectedEmiPlan != null) {
                document.getElementById('sp-amount-heading').innerText = 'Approx EMI per month';
                document.getElementById('sp-amount-text').innerText = CustomerOrder.currencyCode + ' ' + SelectedEmiPlan.monthlyEmi;
            }

        }
    }

    /*********** Show EMI Details ************/
    ShowEmiPlanDetails = function (emiIndex) {
        for (let i = 0; i < EmiPlans.length; i++) {
            if (emiIndex == i) {
                // document.getElementById('sp-collapse-' + i ).style.display = 'block';
                if (document.getElementById('sp-collapse-' + i).classList.contains("show")) {
                    // console.log("Active Contains");
                    document.getElementById('sp-collapse-' + i).classList.remove("show");
                    document.getElementById('sp-emi-breckdown-icon' + i).innerHTML = '<img src="' + SvGIconSet.plusIcon + '"/>';
                }
                else {
                    document.getElementById('sp-collapse-' + i).classList.add("show");
                    document.getElementById('sp-emi-breckdown-icon' + i).innerHTML = '<img src="' + SvGIconSet.minusIcon + '" />';
                }
            }
            else {
                // document.getElementById('sp-collapse' + i ).style.display = 'none';
                if (document.getElementById('emi-ct-' + i).classList.contains("show")) {
                    // console.log("Active Contains");
                    document.getElementById('emi-ct-' + i).classList.remove("show");
                    document.getElementById('sp-emi-breckdown-icon' + i).innerHTML = '<img src="' + SvGIconSet.minusIcon + '" />';

                }
            }
        }
    }

    /*********** Handle CVV from Saved Cards ************/
    HandleCvvForSavedCard = function (target, ind) {
        //console.log("Saved Card Cvv Target:", target, SavedCards[ind]);
        let value = document.getElementById(target).value;
        //document.getElementById('sp-txt-card-cvv').classList.remove("sp-has-error");
        // CustomerCardDetails.cardCVV2 = value;
        document.getElementById('sp-footer-btn').disabled = true; /// to disable the pay now button for payment.
        if (value.trim().length == 0) {
            return;
        }

        if (!value.match(/^[0-9]{3}$/)) {
            return;
        }

        let savedCardList = [];
        for (let i = 0; i < SavedCards.length; i++) {
            if (SavedCards[i].payModeId == SelectedPaymentMode.payModeId) {
                savedCardList.push(SavedCards[i]);
            }
        }

        //console.log("SelectedSavedCard::", savedCardList[ind]);
        /*
        CustomerCardDetails.nameOnCard = savedCardList[ind].nameOnCard;
        CustomerCardDetails.cardNumber = savedCardList[ind].first6Digits + '******' + savedCardList[ind].last4Digits;
        CustomerCardDetails.cardExpiryMonth = savedCardList[ind].expiryMonth;
        CustomerCardDetails.cardExpiryYear = savedCardList[ind].expiryYear;
        CustomerCardDetails.cardCVV2 = value;
*/
        /*
                let maskedCardNo = savedCardList[ind].first6Digits.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
                let url = `${QueryUrl}/checkCardTpe/${maskedCardNo}`;
                // console.log("CardNO ::::::::::::::", url);
        
        
                let response = CallAPI('POST', url, {}, function (response) {
                    // console.log("CardTypeDetails::", response);
                    try {
                        if (response != '') {
                            let cardTypeData = response.split('|');
                            if (cardTypeData.length > 0) {
                                let data = {
                                    cardNo: cardTypeData[0],
                                    cardType: cardTypeData[1],
                                    cardSchemeName: cardTypeData[2],
                                    cardSchemeLogo: cardTypeData[3],
                                    cardIssuerBank: cardTypeData[4],
                                    cardOperatingMode: cardTypeData[5],
                                    cardProductType: cardTypeData[6],
                                }
        
                                let cardType = data.cardSchemeName;
                                let list = SelectedPaymentMode.paymentModeDetailsList;
                                let found = false;
                                for (let i = 0; i < list.length; i++) {
                                    if (list[i].schemeDetailsResponse.schemeName.toLowerCase() == cardType.toLowerCase()) {
                                        SelectPayOption(list[i].pgDetailsResponse.pg_id);
                                        found = true;
                                        break;
                                    }
                                }
                                // console.log("CardType::", cardType);
                                if (!found) {
                                    console.log("Card is not supported by this merchant");
                                    return;
                                }
                                IsSavedCardPayment = true;
                                document.getElementById('sp-footer-btn').disabled = false; /// to enable the pay now button for payment.
        
                            }
                            else {
                                // No data found.
                            }
                            // HandleEMIBreakDowns(emiData)
                        }
                    } catch (e) {
                        console.log("Error:", e);
                        // HandleEMIBreakDowns([])
                    }
                    //SavedCards = JSON.parse(cardData); // Decrypted scheme data 
                });
                */


        IsSavedCardPayment = true;
        document.getElementById('sp-footer-btn').disabled = false; /// to enable the pay now button for payment.


    }

    /*************** Handle session timeout**************/

    SpHandleTimeout = function () {
        SessionTimeout = setTimeout(function () {
            console.log("Sesson timeout.");
            resObj = {
                status: false,
                data: null,
                message: 'Sesson timeout.'
            }
            document.getElementById('sp-txn-failed-text').innerText = 'Session timeout.';
            RenderFailedContent(null);
            //ResponseCallback(resObj); // send success object in callback
        }, 120000);//2mins timeout.
    }

    ShowCvvInfo = function () {

        let str = '<div class="sp-cvv-info-modal"><div class="sp-cvv-info-modal-content"><div class="sp-cvv-info-title"><button type="button" class="_sp-close sp-cvv-close-btn" data-dismiss="modal" onclick="HideCvvInfo()"> <span aria-hidden="true"><span class="sp-close-btn">&times;</span></span> </button><h3 class="_sp-my-1">CVV/CVC</h3> </div><div class="sp-cvv-info-content _sp-pt-2"><p>The CVV Number ("Card Verification Value") on your credit card or debit card is a 3 digit number on VISA®, MasterCard® and Discover® branded credit and debit cards. On your American Express® branded credit or debit card it is a 4 digit numeric code.</p></div></div></div>'
        let node = document.createElement("DIV");
        node.innerHTML = str;
        node.id = 'sp-cvv-modal';
        // document.getElementsByTagName("body")[0].appendChild(node);
        document.getElementById("paymentModal").appendChild(node);
        // document.getElementsByTagName('body')[0].classList.add('modal-open');
    }
    HideCvvInfo = function () {
        // alert("Hello");
        let modalDiv = document.getElementById('sp-cvv-modal');
        modalDiv.remove();
    }
    /************************************ Public Functions **************************************************** */


    /**** Show Payment Modal */
    this.Init = (spOptions) => {
        // console.log("SpOptions::", spOptions);
        /******** Sp Options Will have following values ******** 
        {
            merchantId:201710270001,
            merchantKey:'oqUl4D0LqA4plZw4reAX/K3UKJoQdet0k/N6X6K4Y5k=',
            successUrl:'',
            failureUrl:'',
            countryCode:'IND',
            Mode:'DEV'
        }
        */
        MeId = spOptions.merchantId;
        SuccessUrl = spOptions.responseUrl;
        FailUrl = spOptions.responseUrl;
        CountryCode = spOptions.countryCode;
        // Key = spOptions.merchantKey;
        if (spOptions.mode) {
            IntegrationMode = spOptions.mode;
            if (spOptions.mode == 'PROD') {
                PaymentUrl = PaymentProdUrl;
            }
        }
        Init(); // call init fuction to set initial data by Merchant Id.


    }

    /**** Hide Payment Modal */
    this.Hide = () => {

    }

    this.Text = () => {
        Init();
    }

    this.Pay = (data, callback) => {

        console.log("Pay is called..........................................");

        CustomerOrder = data;
        if (callback != null) {
            ResponseCallback = callback;
        }

        //console.log("OrderDetails::", data)
        if (CustomerOrder.customerDetails) {
            CustomerDetails = {
                name: CustomerOrder.customerDetails.name,
                email: CustomerOrder.customerDetails.email,
                phone: CustomerOrder.customerDetails.phone,
                userId: CustomerOrder.userId,
            }
        }
        if (CustomerOrder.billingDetails) {
            if (CustomerOrder.billingDetails.bill_address && CustomerOrder.billingDetails.bill_city && CustomerOrder.billingDetails.bill_state && CustomerOrder.billingDetails.bill_country && CustomerOrder.billingDetails.bill_zip) {
                CustomerBillingAddress = CustomerOrder.billingDetails;
            }

        }

        // if (MerchantBrandData.integration_type != "Merchant Hosted") {
        //     SPPayNow();
        // }
        // else {
        RenderPaymentModal();
        // }


    }

}






